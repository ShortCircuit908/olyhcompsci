package com.calebmilligan.compsci;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author ShortCircuit908
 *         Created on 6/9/2016.
 */
public class ProjectRunner {
	private JPanel content_panel;
	private JComboBox<Class<? extends Object>> class_chooser;
	private JTextField args_field;
	private JCheckBox button_run;
	private final DefaultComboBoxModel<Class<? extends Object>> class_model;

	private SecurityManager default_security_manager;
	private ThreadGroup run_thread_group = new ThreadGroup(Thread.currentThread().getThreadGroup(), "RunGroup");
	private final Timer monitor_timer = new Timer("RunMonitorTimer", true);
	private final TimerTask monitor_task = new TimerTask() {
		Thread[] threads = new Thread[0];

		@Override
		public void run() {
			int thread_count_approx = (int) Math.ceil(run_thread_group.activeCount() * 1.5);
			if (thread_count_approx > threads.length) {
				threads = new Thread[thread_count_approx];
			}
			int thread_count = run_thread_group.enumerate(threads, true);
			if (thread_count == 0) {
				finished();
			}
		}
	};
	private final AtomicBoolean running = new AtomicBoolean(false);

	public ProjectRunner(List<Class<? extends Object>> classes) {
		class_model = new DefaultComboBoxModel<>(new Vector<>(classes));
		class_chooser.setModel(class_model);


		button_run.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				if (button_run.isSelected()) {
					stop();
					final Class<? extends Object> main_class = (Class<? extends Object>) class_model.getSelectedItem();
					final String[] args = args_field.getText().trim().isEmpty() ? new String[0] : args_field.getText().split(" (?=\")|(?<=\")\\s");
					Thread t = new Thread(run_thread_group, new Runnable() {
						@Override
						public void run() {
							try {
								Method main_method = main_class.getDeclaredMethod("main", String[].class);
								System.err.println("\nRunning project: " + main_class.getName() + "\n");
								running.set(true);
								//default_security_manager = System.getSecurityManager();
								//System.setSecurityManager(new NoExitSecurityManager());
								main_method.invoke(null, (Object) args);
							}
							catch (Throwable e) {
								e.printStackTrace();
							}
						}
					});
					t.start();
				}
				else {
					stop();
				}
			}
		});

		JFrame frame = new JFrame("Caleb Milligan - ProjectRunner");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setContentPane(content_panel);
		frame.setLocationByPlatform(true);
		frame.pack();
		frame.setVisible(true);
	}

	private void finished() {
		running.set(false);
		button_run.setSelected(false);
		System.err.println("\nProject finished\n");
		//System.setSecurityManager(default_security_manager);
	}

	private void stop() {
		run_thread_group.interrupt();
		running.set(false);
		try {
			run_thread_group.stop();
		}
		catch (Throwable e) {
			// Do nothing
		}
		System.err.println("\nProject halted\n");
		//System.setSecurityManager(default_security_manager);
	}
}
