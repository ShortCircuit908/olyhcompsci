package com.calebmilligan.compsci;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Caleb Milligan
 *         Created on 6/9/2016.
 */
public class CSFinal {
	public static void main(String... args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e) {
			// Do nothing
		}
		SubTypesScanner scan = new SubTypesScanner(false);

		Reflections reflections = new Reflections("com.calebmilligan.compsci.projects", scan);
		LinkedList<Class<?>> classes = new LinkedList<>();
		getSubTypesOf(reflections, classes, Object.class);
		getSubTypesOf(reflections, classes, JFrame.class);
		getSubTypesOf(reflections, classes, JApplet.class);
		getSubTypesOf(reflections, classes, Container.class);
		LinkedList<Class<? extends Object>> main_classes = new LinkedList<>();
		for (Class<? extends Object> clazz : classes) {
			if (clazz.equals(CSFinal.class)) {
				continue;
			}
			try {
				Method main_method = clazz.getDeclaredMethod("main", String[].class);
				int modifiers = main_method.getModifiers();
				if (!Modifier.isStatic(modifiers) || !Modifier.isPublic(modifiers) || !main_method.getReturnType().equals(void.class)) {
					continue;
				}
				main_classes.add(clazz);
			}
			catch (ReflectiveOperationException e) {
				// Do nothing
			}
		}
		if (main_classes.size() == 0) {
			return;
		}
		Collections.sort(main_classes, new Comparator<Class<? extends Object>>() {
			@Override
			public int compare(Class<? extends Object> o1, Class<? extends Object> o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		new ProjectRunner(main_classes);
	}

	public static <T> void getSubTypesOf(Reflections reflections, List<Class<?>> lst, Class<T> cls) {
		LinkedList<Class<? extends T>> nlist = new LinkedList<>();
		nlist.addAll(reflections.getSubTypesOf(cls));
		lst.addAll(nlist);
		for (Class<? extends T> scls : nlist) {
			getSubTypesOf(reflections, lst, scls);
		}
	}
}
