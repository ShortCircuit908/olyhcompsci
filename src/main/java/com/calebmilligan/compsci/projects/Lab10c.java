package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: Lab10b.java
 * Description: Error handling
 *
 * @author Caleb Milligan
 *         Created on 2/23/2016
 */
@ProjectId(37)
public class Lab10c {
	public static void main(String... args) {
		double infinity_double = 5.0 / 0.0;
		System.out.println("infinity_double = " + infinity_double);
		double nan_double = 0.0 / 0.0;
		System.out.println("nan_double = " + nan_double);
		float infinity_float = 5.0f / 0.0f;
		System.out.println("infinity_float = " + infinity_float);
		float nan_float = 0.0f / 0.0f;
		System.out.println("nan_float = " + nan_float);
		try {
			int divide_by_zero = 4 / 0;
			System.out.println("divide_by_zero = " + divide_by_zero);
		}
		catch (ArithmeticException e) {
			e.printStackTrace();
		}
	}
}
