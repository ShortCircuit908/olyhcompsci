package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: StringList.java
 * Description: Array practice
 *
 * @author Caleb Milligan
 *         Created on 12/11/2015
 */
@ProjectId(27)
public class StringList {
	public static void main(String... args) {
		// Define new empty String array
		String[] string_list = new String[10];
		System.out.println("Enter 10 different animals into a list:\n");
		Scanner scanner = new Scanner(System.in);
		// Store user input into each element
		for (int i = 0; i < string_list.length; i++) {
			System.out.print("Enter animal #" + (i + 1) + ": ");
			string_list[i] = scanner.nextLine();
		}
		System.out.println("\nYour list of animals:\n");
		// Display each element with formatting
		for (int i = 0; i < string_list.length; i++) {
			System.out.println("Animal #" + (i + 1) + ": " + string_list[i]);
		}
	}
}
