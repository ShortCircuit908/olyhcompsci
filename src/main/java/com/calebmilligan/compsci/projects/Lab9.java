package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * Name: Lab9.java
 * Description: Multiplying 2-dimensional arrays
 *
 * @author Caleb Milligan
 *         Created on 1/18/2016
 */
@ProjectId(31)
public class Lab9 {
	private static final double[] categories = {0.1, 0.4, 0.5};
	private static final String format = "%1$s - Avg scores: %2$s (daily)\t%3$s (quiz)\t%4$s (test)\tFinal grade: %5$s";
	private static final DecimalFormat percent_format = new DecimalFormat("#0.#%");

	public static void main(String... args) {
		int size = -1;
		Scanner scanner = new Scanner(System.in);
		while (size < 0) {
			try {
				System.out.print("How many students? ");
				size = Integer.parseInt(scanner.nextLine());
				if (size < 0) {
					throw new NumberFormatException();
				}
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid input");
			}
		}
		String[] names = new String[size];
		double[][] avg_Scores = new double[size][3];

		for (int i = 0; i < size; i++) {
			System.out.print("Student's name: ");
			names[i] = scanner.nextLine();
			double[] scores = new double[3];
			avg_Scores[i] = scores;
			scores[0] = promptScore("daily", scanner);
			scores[1] = promptScore("quiz", scanner);
			scores[2] = promptScore("test", scanner);
		}

		double[][] grades = multiplyMatrices(avg_Scores);

		for (int i = 0; i < size; i++) {
			System.out.println(String.format(
					format,
					names[i],
					percent_format.format(avg_Scores[i][0]),
					percent_format.format(avg_Scores[i][1]),
					percent_format.format(avg_Scores[i][2]),
					percent_format.format(grades[i][0])));
		}
	}

	private static double promptScore(String category, Scanner scanner) {
		double score;
		while (true) {
			try {
				System.out.print("Student's average " + category + " score (%): ");
				score = Double.parseDouble(scanner.nextLine()) / 100.0;
				if (score < 0) {
					throw new NumberFormatException();
				}
				return score;
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid input");
			}
		}
	}

	private static double[][] multiplyMatrices(double[][] matrix_a) {
		int a_rows = matrix_a.length;
		int a_columns = matrix_a[0].length;

		double[][] result = new double[a_rows][1];

		for (int x = 0; x < a_rows; x++) {
			for (int y = 0; y < a_columns; y++) {
				result[x][0] += matrix_a[x][y] * categories[y];
			}
		}
		return result;
	}
}
