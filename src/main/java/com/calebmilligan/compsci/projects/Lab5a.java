package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: Lab5a.java
 * Description: Strings (count vowels)
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/26/2015
 */
@ProjectId(19)
public class Lab5a {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter some text: ");
		String phrase = scanner.nextLine().toLowerCase();
		countInstances(phrase, "a");
		countInstances(phrase, "e");
		countInstances(phrase, "i");
		countInstances(phrase, "o");
		countInstances(phrase, "u");
	}

	private static void countInstances(String str, String search) {
		System.out.println(search + " occurs " + ((str.length() - str.replace(search, "").length()) / search.length()) + " time(s)");
	}
}
