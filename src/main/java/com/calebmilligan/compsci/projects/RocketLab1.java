package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: RocketLab1.java
 * Description: Draw a rocket
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/16/2015
 */
@ProjectId(3)
public class RocketLab1 {
	private static final String[] lines = {
			"   /\\   ",
			"  /  \\  ",
			" /    \\ ",
			" \\    / ",
			"  \\  /  ",
			"   \\/   ",
			"        ",
			" \\    / ",
			"  \\  /  ",
			"   \\/   ",
			"   /\\   ",
			"  /  \\  ",
			" /    \\ ",
			"        ",
			"   /\\   ",
			"  /  \\  ",
			" /    \\ ",
			"+------+",
			"|      |",
			"|      |",
			"+------+",
			"|UNITED|",
			"|STATES|",
			"+------+",
			"|      |",
			"|      |",
			"+------+",
			"   /\\   ",
			"  /  \\  ",
			" /    \\ "
	};

	public static void main(String... args) {
		for (String line : lines) {
			System.out.println(line);
		}
	}
}
