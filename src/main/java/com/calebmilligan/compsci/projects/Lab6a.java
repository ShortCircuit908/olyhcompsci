package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * Name: Lab6a.java
 * Description: Methods (calculator)
 *
 * @author ShortCircuit908
 *         Created on 11/12/2015
 */
@ProjectId(24)
public class Lab6a {
	private static final DecimalFormat output_format = new DecimalFormat(",##0.##################");

	public static void main(String... args) {
		System.out.println("Welcome to my not-so-fancy calculator!\n");
		Scanner scanner = new Scanner(System.in);
		while (true) {
			printMenu();
			System.out.print("Your selection: ");
			String line = scanner.nextLine().toLowerCase().replace(",", "");
			if (!compute(line, 0.0, 0.0, false)) {
				continue;
			}
			if (line.equals("x")) {
				break;
			}
			double first = Double.NaN;
			double second = Double.NaN;
			while (first != first) {
				try {
					System.out.print("Enter your first number: ");
					first = Double.parseDouble(scanner.nextLine());
				}
				catch (NumberFormatException e) {
					System.out.println("Invalid input.\n");
				}
			}
			while (second != second) {
				try {
					System.out.print("Enter your second number: ");
					second = Double.parseDouble(scanner.nextLine());
				}
				catch (NumberFormatException e) {
					System.out.println("Invalid input.\n");
				}
			}
			compute(line, first, second, true);
		}
		scanner.close();
		System.out.println("Thank you for calculating with me! Come back soon!");
	}

	private static boolean compute(String line, double first, double second, boolean do_output) {
		double result;
		if (line.matches("p(rod(uct)?)?")) {
			result = multiply(first, second);
		}
		else if (line.matches("q(uot(ient)?)?")) {
			result = divide(first, second);
		}
		else if (line.matches("s(um)?")) {
			result = add(first, second);
		}
		else if (line.matches("d(iff(erence)?)?")) {
			result = subtract(first, second);
		}
		else if (line.matches("c(omp(are)?)?")) {
			if (do_output) {
				compare(first, second);
			}
			return true;
		}
		else {
			System.out.println("Invalid input\n");
			return false;
		}
		if (do_output) {
			System.out.println("The result is " + output_format.format(result) + "\n");
		}
		return true;
	}

	private static double add(double a, double b) {
		return a + b;
	}

	private static double subtract(double a, double b) {
		return a - b;
	}

	private static double multiply(double a, double b) {
		return a * b;
	}

	private static double divide(double a, double b) {
		return a / b;
	}

	private static void compare(double a, double b) {
		String compare = " is equal to ";
		if (a > b) {
			compare = " is greater than ";
		}
		else if (a < b) {
			compare = " is less than ";
		}
		System.out.println(output_format.format(a) + compare + output_format.format(b) + "\n");
	}

	private static void printMenu() {
		System.out.println("Enter P to compute the Product");
		System.out.println("Enter Q to compute the Quotient");
		System.out.println("Enter S to compute the Sum");
		System.out.println("Enter D to compute the Difference");
		System.out.println("Enter C to compare");
		System.out.println("Enter X to exit\n");
	}
}
