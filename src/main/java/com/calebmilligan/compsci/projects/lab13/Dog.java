package com.calebmilligan.compsci.projects.lab13;

/**
 * @author Caleb Milligan
 *         Created on 2/26/2016
 */
public class Dog extends Mammal {
	private String name;

	public Dog() {
		this(null, null, null);
	}

	public Dog(String name) {
		this(name, null, null);
	}

	public Dog(String name, String hair_type) {
		this(name, null, hair_type);
	}

	public Dog(String name, Boolean gender) {
		this(name, gender, null);
	}

	public Dog(String name, Boolean gender, String hair_type) {
		super(gender, hair_type);
		this.name = name;
	}

	public int digBones(Dog dog, int x) {
		System.out.println("I love digging for bones!");
		return x;
	}

	public void fetch() {
		System.out.println("I'd love to play fetch!");
	}

	@Override
	public String speak() {
		return "bark, bark";
	}

	@Override
	public String feed() {
		return "This dog is hungry. Feed me!";
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + getPossessivePronoun() + " name is " + (name == null ? "unknown" : name);
	}
}
