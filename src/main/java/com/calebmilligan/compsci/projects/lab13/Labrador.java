package com.calebmilligan.compsci.projects.lab13;

/**
 * Name: BorderCollie.java
 *
 * @author Caleb Milligan
 *         Created on 3/11/2016
 */
public class Labrador extends Dog {
	private String breed;

	public Labrador(String name, Boolean gender, String hair_type, String breed) {
		super(name, gender, hair_type);
	}

	@Override
	public String toString() {
		return super.toString() + "\nMy breed is a " + breed;
	}

	@Override
	public int digBones(Dog dog, int x) {
		System.out.println("I'd rather be swimming, but I'll dig up the bones.");
		return x;
	}

	@Override
	public void fetch() {
		System.out.println("Fetching and catching is my favorite!");
	}
}
