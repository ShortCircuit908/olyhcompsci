package com.calebmilligan.compsci.projects.lab13;

/**
 * Name: Animal.java
 * Description: The superclass for all animals
 *
 * @author Caleb Milligan
 *         Created on 2/26/2016
 */
public abstract class Animal {
	public static final boolean MALE = true;
	public static final boolean FEMALE = false;
	private Boolean gender;

	public Animal() {
		this(null);
	}

	public Animal(Boolean gender) {
		this.gender = gender;
	}

	public static void main(String... args) {
		Dog dog1 = new Dog("Bud", MALE, "yellow fur"); // Prediction: This dog is hungry. Feed me!
		Mammal dog2 = new Dog("Ruby", FEMALE, "short brown and black fur"); // Prediction: This dog is hungry. Feed me!
		Animal dog3 = new Dog("Lucky", FEMALE, "white and curly fur"); // Prediction: This dog is hungry. Feed me!
		System.out.println(dog1.feed()); // Actual: This dog is hungry. Feed me!
		System.out.println(dog2.feed()); // Actual: This dog is hungry. Feed me!
		System.out.println(dog3.feed()); // Actual: This dog is hungry. Feed me!

		Dog dug = new Dog("Dug", FEMALE, "long fur");
		int x;
		System.out.println(x = dug.digBones(null, 4));
		BorderCollie bc = new BorderCollie("Rover", MALE, "long fur", "Border Collie");
		int y;
		System.out.println(y = bc.digBones(bc, 2));
		Dog d = new BorderCollie("Spike", FEMALE, "long fur", "Border Collie");
		int z;
		System.out.println(z = d.digBones(d, 4));

		Labrador lab = new Labrador("Spike", FEMALE, "long fur", "Labrador");
		System.out.println(lab.digBones(lab, 4));
	}

	private static void printAnimal(Animal animal) {
		System.out.println(animal);
		System.out.println(animal.speak());
	}

	public abstract String speak();

	@Override
	public String toString() {
		return "This " + getClass().getSimpleName() + "'s gender is " + (gender == null ? "unknown" : (gender ? "male" : "female"));
	}

	protected String getPossessivePronoun() {
		return gender == null ? "Their" : (gender ? "His" : "Her");
	}

	protected String getPronoun() {
		return gender == null ? "They" : (gender ? "He" : "She");
	}

	public String feed() {
		return "This animal is hungry. Feed me!";
	}
}
