package com.calebmilligan.compsci.projects.lab13;

/**
 * @author Caleb Milligan
 *         Created on 3/3/2016
 */
public class Crow extends Bird {
	private String name;

	public Crow() {
		this(null, null, true);
	}

	public Crow(String name) {
		this(name, null, true);
	}

	public Crow(String name, Boolean gender) {
		this(name, gender, true);
	}

	public Crow(String name, Boolean gender, boolean can_fly) {
		super(gender, can_fly);
		this.name = name;
	}

	@Override
	public String speak() {
		return "caw, caw";
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + getPossessivePronoun() + " name is " + (name == null ? "unknown" : name);
	}
}
