package com.calebmilligan.compsci.projects.recursion;

/**
 * Name: RecursionLab1.java
 * Description: Recursion practice
 *
 * @author Caleb Milligan
 *         Created on 3/23/2016
 */
public class RecursionLab1 {
	public static void main(String... args) {
		System.out.println("5! = " + factorial(500));
	}

	public static int factorial(int n) {
		return n == 1 || n == 0 ? 1 : n * factorial(n + (n < 0 ? 1 : -1));
	}
}
