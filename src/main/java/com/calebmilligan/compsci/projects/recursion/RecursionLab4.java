package com.calebmilligan.compsci.projects.recursion;

/**
 * Name: RecursionLab4.java
 * Description: Recursion practice
 *
 * @author Caleb Milligan, Matthew Meier
 *         Created on 3/25/2016
 */
public class RecursionLab4 {
	public static void main(String... args) {
		writeBinary(0);
	}

	public static void writeBinary(long n) {
		// writeBinary(13)
		// 13 = 1101
		//----------------
		// writeBinary(1101)              * Parent call (1101)
		// | 1101 >>> 1 = 0110            * | Shift right 1 bit
		// | 0110 != 0                    * | The result (0110) contains non-zero bits, so...
		// | writeBinary(0110)            * | Recurse with shifted value (0110)
		// | | 0110 >>> 1 = 0011          * | | Shift right 1 bit
		// | | 0011 != 0                  * | | The result (0011) contains non-zero bits, so...
		// | | writeBinary(0011)          * | | Recurse with shifted value (0011)
		// | | | 0011 >>> 1 = 0001        * | | | Shift right 1 bit
		// | | | 0001 != 0                * | | | The result (0001) contains non-zero bits, so...
		// | | | writeBinary(0001)        * | | | Recurse with shifted value (0001)
		// | | | | 0001 >>> 1 = 0000      * | | | | Shift right 1 bit
		// | | | | 0000 == 0              * | | | | The result (0000) does not contain non-zero bits, so...
		// | | | | print(0001 & 0001 = 1) * | | | | Set bits 63-1 to 0 and print the result (1)
		// | | | print(0011 & 0001 = 1)   * | | | Set bits 63-1 to 0 and print the result (1)
		// | | print(0110 & 0001 = 0)     * | | Set bits 63-1 to 0 and print the result (0)
		// | print(1101 & 0001 = 1)       * | Set bits 63-1 to 0 and print the result (1)


		// Shift right 1 bit and recurse if there are any non-zero bits
		if ((n >>> 1) != 0) {
			writeBinary(n >>> 1);
		}
		// Set all but the rightmost bit to 0 and print the result
		System.out.print(n & 0x1);
	}

	public static void writeBinary(double n) {
		writeBinary(Double.doubleToRawLongBits(n));
	}

	public static void writeBinary(float n) {
		writeBinary(Float.floatToRawIntBits(n));
	}
}