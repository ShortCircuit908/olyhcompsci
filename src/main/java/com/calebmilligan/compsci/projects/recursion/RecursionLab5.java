package com.calebmilligan.compsci.projects.recursion;

/**
 * Name: RecursionLab5.java
 * Description: Recursion practice
 *
 * @author Caleb Milligan
 *         Created on 3/28/2016
 */
public class RecursionLab5 {
	public static void main(String... args) {
		printBackwards("pAlinDROme");
	}

	public static void printBackwards(String s) {
		if (s.length() > 1) {
			printBackwards(s.substring(1, s.length()));
		}
		System.out.print(s.charAt(0));
	}
}
