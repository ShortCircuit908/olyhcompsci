package com.calebmilligan.compsci.projects.recursion;

/**
 * Name: RecursionLab2.java
 * Description: Recursion practice
 *
 * @author Caleb Milligan
 *         Created on 3/23/2016
 */
public class RecursionLab2 {
	public static void main(String... args) {
		System.out.println("2^5 = " + pow2(500));
	}

	public static int pow2(int n) {
		return n > 0 ? 2 * pow2(n - 1) : 1;
	}
}
