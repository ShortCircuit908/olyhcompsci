package com.calebmilligan.compsci.projects.recursion;

import java.io.IOException;

/**
 * Name: RecursionLab3.java
 * Description: Recursion practice
 *
 * @author Caleb Milligan
 *         Created on 3/23/2016
 */
public class RecursionLab3 {
	public static void main(String... args) throws IOException {
		starString(50);
	}

	public static int starString(int n) throws IllegalArgumentException {
		if (n < 0) {
			throw new IllegalArgumentException("n must not be less than 0");
		}
		if (n > 0) {
			int result = starString(n - 1);
			for (int i = 0; i < result; i++) {
				System.out.print('*');
			}
			return result * 2;
		}
		System.out.print('*');
		return 1;
	}
}
