package com.calebmilligan.compsci.projects.finalproject;

import java.awt.*;
import java.util.LinkedList;

import static com.calebmilligan.compsci.projects.finalproject.FinalProject.level;

/**
 * Name: Ball.java
 * Description: Represents a ball used for play
 *
 * @author Caleb Milligan
 *         Created on 5/11/2016.
 */
public class Ball extends GameObject {
	private static final Dimension ball_size = new Dimension(FinalProject.SIZE.height / 40, FinalProject.SIZE.height / 40);
	private static final double base_speed = FinalProject.SIZE.height / (ball_size.height * 10.0);
	private double dir_x = (Math.random() < 0.5 ? -1 : 1) * (Math.random() * 0.5 + 0.5), dir_y = 1;

	public Ball() {
		Paddle paddle = FinalProject.INSTANCE.getGameObjects(Paddle.class).get(0);
		origin.x = (paddle.origin.x + (paddle.getWidth() - ball_size.width) / 2.0);
		origin.y = (FinalProject.INSTANCE.getGameObjects(Paddle.class).get(0).origin.y - ball_size.height * 1.5);
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(FinalProject.GLOBAL_COLOR != null ? FinalProject.GLOBAL_COLOR : Color.LIGHT_GRAY);
		g.fillRect((int) origin.x, (int) origin.y, ball_size.width, ball_size.height);
	}

	@Override
	public void update() {
		if (FinalProject.state != 1 || FinalProject.state == 2) {
			return;
		}
		// Movement
		origin.x += (dir_x * base_speed * (level * 0.05 + 1));
		origin.y -= (dir_y * base_speed * (level * 0.05 + 1));
		// Handle collision
		GameObject contacted = null;
		if (origin.x >= FinalProject.SIZE.width - ball_size.width || (contacted = contactRight()) != null) {
			if (FinalProject.balls_can_collide || !(contacted instanceof Ball)) {
				handleContact(contacted);
				dir_x = -Math.abs(dir_x);
			}
		}
		if (origin.x <= 0 || (contacted = contactLeft()) != null) {
			if (FinalProject.balls_can_collide || !(contacted instanceof Ball)) {
				handleContact(contacted);
				dir_x = Math.abs(dir_x);
			}
		}
		if (origin.y <= 0 || (contacted = contactTop()) != null) {
			if (FinalProject.balls_can_collide || !(contacted instanceof Ball)) {
				handleContact(contacted);
				dir_y = -Math.abs(dir_y);
			}
		}
		if ((contacted = contactBottom()) != null) {
			if (FinalProject.balls_can_collide || !(contacted instanceof Ball)) {
				handleContact(contacted);
				dir_y = Math.abs(dir_y);
			}
		}
		// Lose a ball
		if (origin.y >= FinalProject.SIZE.height) {
			FinalProject.INSTANCE.removeGameObject(this);
			FinalProject.setScreenShakeDuration(10);
		}
	}

	private void removeBrick(Brick brick) {
		FinalProject.setScreenShakeDuration(10);
		FinalProject.INSTANCE.removeGameObject(brick);
		if (++FinalProject.score > FinalProject.high_score) {
			FinalProject.high_score = FinalProject.score;
		}
	}

	private void handleContact(GameObject contacted) {
		if (contacted == null) {
			AdvancedToneGen.playToneAsync(700, 25);
		}
		else if (contacted instanceof Brick) {
			removeBrick((Brick) contacted);
			AdvancedToneGen.playToneAsync(800, 25);
		}
		else if (contacted instanceof Paddle) {
			double paddle_center = contacted.origin.x + (contacted.getWidth() / 2.0);
			double center = origin.x + (ball_size.width / 2.0);
			double diff = paddle_center - center;
			diff = (Math.signum(diff) * Math.pow(Math.abs(diff), 1.0/16.0)) / 2.0;
			dir_x += diff;

			// Normalize the vector
			double length = Math.sqrt(Math.pow(dir_x, 2) + Math.pow(dir_y, 2));
			if (length != 0) {
				dir_x /= length;
				dir_y /= length;
			}
			AdvancedToneGen.playToneAsync(800, 25);
		}
	}

	@Override
	public int getWidth() {
		return ball_size.width;
	}

	@Override
	public int getHeight() {
		return ball_size.height;
	}

	/**
	 * Detect bounding-box collision
	 *
	 * @return collided object, or {@code null} if none
	 */
	private GameObject contactLeft() {
		LinkedList<GameObject> objects = FinalProject.INSTANCE.getGameObjects(null);
		for (GameObject object : objects) {
			double other_right = object.origin.x + object.getWidth();
			if (object != this
					&& other_right + Math.abs(dir_x * base_speed * (level * 0.05 + 1)) >= origin.x && other_right - Math.abs(dir_x * base_speed * (level * 0.05 + 1)) <= origin.x
					&& object.origin.y <= origin.y + getHeight() && object.origin.y + object.getHeight() >= origin.y) {
				return object;
			}
		}
		return null;
	}

	/**
	 * Detect bounding-box collision
	 *
	 * @return collided object, or {@code null} if none
	 */
	private GameObject contactRight() {
		LinkedList<GameObject> objects = FinalProject.INSTANCE.getGameObjects(null);
		for (GameObject object : objects) {
			double other_left = object.origin.x;
			if (object != this
					&& other_left + Math.abs(dir_x * base_speed * (level * 0.05 + 1)) >= origin.x + getWidth() && other_left - Math.abs(dir_x * base_speed * (level * 0.05 + 1)) <= origin.x + getWidth()
					&& object.origin.y <= origin.y + getHeight() && object.origin.y + object.getHeight() >= origin.y) {
				return object;
			}
		}
		return null;
	}

	/**
	 * Detect bounding-box collision
	 *
	 * @return collided object, or {@code null} if none
	 */
	private GameObject contactTop() {
		LinkedList<GameObject> objects = FinalProject.INSTANCE.getGameObjects(null);
		for (GameObject object : objects) {
			double other_bottom = object.origin.y + object.getHeight();
			if (object != this
					&& other_bottom + Math.abs(dir_y * base_speed * (level * 0.05 + 1)) >= origin.y && other_bottom - Math.abs(dir_y * base_speed * (level * 0.05 + 1)) <= origin.y
					&& object.origin.x <= origin.x + getWidth() && object.origin.x + object.getWidth() >= origin.x) {
				return object;
			}
		}
		return null;
	}

	/**
	 * Detect bounding-box collision
	 *
	 * @return collided object, or {@code null} if none
	 */
	private GameObject contactBottom() {
		LinkedList<GameObject> objects = FinalProject.INSTANCE.getGameObjects(null);
		for (GameObject object : objects) {
			double other_top = object.origin.y;
			if (object != this
					&& other_top + Math.abs(dir_y * base_speed * (level * 0.05 + 1)) >= origin.y + getHeight() && other_top - Math.abs(dir_y * base_speed * (level * 0.05 + 1)) <= origin.y + getHeight()
					&& object.origin.x <= origin.x + getWidth() && object.origin.x + object.getWidth() >= origin.x) {
				return object;
			}
		}
		return null;
	}
}
