package com.calebmilligan.compsci.projects.finalproject;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;

/**
 * Name: Keyboard.java
 * Description: Handle keyboard input
 *
 * @author Caleb Milligan
 *         Created on 5/11/2016.
 */
public class Keyboard extends KeyEvent implements KeyListener {
	public static final int VK_ANY = 0xFFFFFFFF;
	public static final int KEY_LOCATION_ANY = 0xFFFFFFFF;

	private static final LinkedList<KeyState> key_states = new LinkedList<>();

	public Keyboard() {
		// We only extend KeyEvent for easy access to its constants
		super(new JLabel(), 0, 0, 0, 0, (char) 0, 0);
	}

	public static boolean isKeyPressed(int key) {
		return isKeyPressed(key, KEY_LOCATION_ANY);
	}

	public static boolean isKeyPressed(int key, int location) {
		for (KeyState state : key_states) {
			if (((key == VK_ANY || state.key_code == key) && (location == KEY_LOCATION_ANY || state.location == location)) && state.pressed) {
				return true;
			}
		}
		return false;
	}

	public static boolean isAnyKeyPressed() {
		return isKeyPressed(VK_ANY, KEY_LOCATION_ANY);
	}

	public static boolean isAnyKeyPressed(int... ignore_keys) {
		for (KeyState state : key_states) {
			for (int ignore_key : ignore_keys) {
				if (state.key_code == ignore_key) {
					continue;
				}
				if (state.pressed) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isAnyKeyPressed(int[]... ignore_keys) {
		for (KeyState state : key_states) {
			for (int[] ignore_key : ignore_keys) {
				if (ignore_key == null || ignore_key.length == 0 || state.key_code == ignore_key[0] || (ignore_key.length > 1 && state.location == ignore_key[1])) {
					continue;
				}
				if (state.pressed) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isKeyTriggered(int key) {
		return isKeyTriggered(key, KEY_LOCATION_ANY);
	}

	public static boolean isKeyTriggered(int key, int location) {
		for (KeyState state : key_states) {
			if (((key == VK_ANY || state.key_code == key) && (location == KEY_LOCATION_ANY || state.location == location)) && state.triggered) {
				return true;
			}
		}
		return false;
	}

	public static boolean isAnyKeyTriggered() {
		return isKeyTriggered(VK_ANY, KEY_LOCATION_ANY);
	}

	public static boolean isAnyKeyTriggered(int... ignore_keys) {
		for (KeyState state : key_states) {
			for (int ignore_key : ignore_keys) {
				if (state.key_code == ignore_key) {
					continue;
				}
				if (state.triggered) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isAnyKeyTriggered(int[]... ignore_keys) {
		for (KeyState state : key_states) {
			for (int[] ignore_key : ignore_keys) {
				if (ignore_key == null || ignore_key.length == 0 || state.key_code == ignore_key[0] || (ignore_key.length > 1 && state.location == ignore_key[1])) {
					continue;
				}
				if (state.triggered) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Resets the triggered state of all keys
	 * <p>
	 * This method should be called once at the end of the main event loop
	 */
	public static void update() {
		synchronized (key_states) {
			for (KeyState key_state : key_states) {
				key_state.triggered = false;
			}
		}
	}

	public static KeyState getKeyState(int key, int location) {
		for (KeyState state : key_states) {
			if (state.key_code == key && (location == KEY_LOCATION_ANY || state.location == location)) {
				return state;
			}
		}
		KeyState state = new KeyState(key, false, false, location);
		key_states.add(state);
		return state;
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent event) {
		synchronized (key_states) {
			KeyState state = getKeyState(event.getKeyCode(), event.getKeyLocation());
			if (!state.pressed) {
				state.triggered = true;
				state.pressed = true;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		synchronized (key_states) {
			getKeyState(event.getKeyCode(), event.getKeyLocation()).pressed = false;
		}
	}

	public static class KeyState {
		public final int key_code;
		public final int location;
		public boolean pressed;
		public boolean triggered;

		private KeyState(int key_code, boolean pressed, boolean triggered, int location) {
			this.key_code = key_code;
			this.pressed = pressed;
			this.triggered = triggered;
			this.location = location;
		}
	}
}
