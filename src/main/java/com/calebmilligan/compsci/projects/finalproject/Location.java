package com.calebmilligan.compsci.projects.finalproject;

/**
 * Name: Location.java
 * Description: Store a double-precision location
 *
 * @author Caleb Milligan
 *         Created on 5/11/2016.
 */
public class Location {
	public double x;
	public double y;
}
