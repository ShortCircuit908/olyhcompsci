package com.calebmilligan.compsci.projects.finalproject;

import java.awt.*;

/**
 * Name: GameObject.java
 * Description: Base class for game objects
 *
 * @author Caleb Milligan
 *         Created on 5/11/2016.
 */
public abstract class GameObject {
	protected final Location origin = new Location();

	public abstract void draw(Graphics2D g);

	public abstract void update();

	public abstract int getWidth();

	public abstract int getHeight();
}
