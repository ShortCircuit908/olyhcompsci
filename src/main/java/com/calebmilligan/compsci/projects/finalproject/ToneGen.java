package com.calebmilligan.compsci.projects.finalproject;

import javax.sound.sampled.*;

/**
 * Name: ToneGen.java
 * Description: Beep boop
 *
 * @author Caleb Milligan
 *         Created on 5/16/2016.
 */
public class ToneGen {
	private static final float SAMPLE_RATE = 8000;
	private static final AudioFormat format = new AudioFormat(SAMPLE_RATE, 8, 1, true, false);
	private static SourceDataLine sdl = null;

	public static final double[][] TUNE_GAME_OVER = {{1200}, {}, {1000}, {}, {800}, {}, {950}, {}, {900, 1000}};
	public static final double[][] TUNE_LOSE_LIFE = {{800, 250}, {}, {700, 250}};
	public static final double[][] TUNE_GAIN_LIFE = {{700, 250}, {}, {800, 250}};

	public static void openDataLine() {
		if (sdl != null) {
			return;
		}
		try {
			sdl = AudioSystem.getSourceDataLine(format);
			sdl.open(format);
			sdl.start();
		}
		catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	public static void closeDataLine() {
		if (sdl == null) {
			return;
		}
		sdl.close();
		sdl = null;
	}

	public static void playTuneAsync(double[][] tune) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				playTune(tune);
			}
		}).start();
	}

	public static void playTune(double[][] tune) {
		for (double[] frame : tune) {
			int frequency = frame == null ? 0 : frame.length > 0 ? (int) frame[0] : 0;
			int duration = frame == null ? 0 : frame.length > 1 ? (int) frame[1] : 125;
			double volume = frame == null ? 0 : frame.length > 2 ? frame[2] : 1.0;
			playToneSilent(frequency, duration, volume);
		}
	}

	public static void playToneAsync(int frequency, int millis) {
		playToneAsync(frequency, millis, 1.0);
	}

	public static void playToneAsync(int frequency, int millis, double volume) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				playToneSilent(frequency, millis, volume);
			}
		}).start();
	}

	public static void playToneSilent(int frequency, int millis) {
		playToneSilent(frequency, millis, 1.0);
	}

	public static void playToneSilent(int frequency, int millis, double volume) {
		try {
			playTone(frequency, millis, volume);
		}
		catch (LineUnavailableException | InterruptedException e) {
			// Do nothing
		}
	}

	public static void playTone(int frequency, int millis) throws LineUnavailableException, InterruptedException {
		playTone(frequency, millis, 1.0);
	}

	public static void playTone(int frequency, int millis, double volume) throws LineUnavailableException, InterruptedException {
		if (sdl == null) {
			return;
		}
		if (frequency <= 0 || volume <= 0) {
			Thread.sleep(millis);
			return;
		}
		volume = Math.min(127, Math.max(volume, 0));
		int total_written = 0;
		int to_go = millis * 8;
		byte[] buffer;
		if (sdl.isActive()) {
			sdl.stop();
		}
		sdl.start();
		while (to_go > 0) {
			buffer = new byte[Math.min(to_go, 512)];
			for (int i = 0; i < buffer.length; i++) {
				// Smooth sine wave
				//double angle = ++total_written / (SAMPLE_RATE / frequency) * 2.0 * Math.PI;
				//buffer[i] = (byte) (Math.sin(angle) * 127.0 * volume);
				// Square wave
				//buffer[i] = (byte) (Math.signum(Math.sin(frequency * 2.0 * Math.PI * ++total_written / SAMPLE_RATE)) * 127 * volume);
			}
			sdl.write(buffer, 0, buffer.length);
			to_go -= buffer.length;
		}
		sdl.drain();
		sdl.stop();
	}
}