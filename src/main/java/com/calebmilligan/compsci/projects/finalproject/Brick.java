package com.calebmilligan.compsci.projects.finalproject;

import java.awt.*;

/**
 * Name: Brick.java
 * Description: Represents a single brick in the game
 *
 * @author Caleb Milligan
 *         Created on 5/11/2016.
 */
public class Brick extends GameObject {
	private static final Dimension brick_size = new Dimension(FinalProject.SIZE.width / 13, FinalProject.SIZE.height / 20);
	private final Color color;

	public Brick(double x, double y, Color color) {
		this.origin.x = x;
		this.origin.y = y;
		this.color = color;
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(FinalProject.GLOBAL_COLOR != null ? FinalProject.GLOBAL_COLOR : color);
		g.fillRect((int) origin.x, (int) origin.y, brick_size.width, brick_size.height);
	}

	@Override
	public void update() {

	}

	@Override
	public int getWidth() {
		return brick_size.width;
	}

	@Override
	public int getHeight() {
		return brick_size.height;
	}
}
