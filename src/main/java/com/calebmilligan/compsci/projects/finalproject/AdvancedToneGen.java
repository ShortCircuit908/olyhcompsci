package com.calebmilligan.compsci.projects.finalproject;

import javax.sound.sampled.*;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Name: AdvancedToneGen.java
 * Description: Utilize clips to play multiple tones simultaneously
 *
 * @author Caleb Milligan
 *         Created on 5/16/2016.
 */
public class AdvancedToneGen {
	private static final int max_sounds = 3;
	private static AtomicInteger sounds_playing = new AtomicInteger(0);
	private static final float SAMPLE_RATE = 8000;
	private static final AudioFormat format = new AudioFormat(SAMPLE_RATE, 8, 1, true, false);
	private static final LineListener sound_listener = new LineListener() {
		@Override
		public void update(LineEvent event) {
			if (event.getType() == LineEvent.Type.STOP || event.getType() == LineEvent.Type.CLOSE) {
				sounds_playing.decrementAndGet();
				event.getLine().removeLineListener(this);
				event.getLine().close();
			}
		}
	};

	public static final double[][] TUNE_GAME_OVER = {{1200}, {}, {1000}, {}, {800}, {}, {950}, {}, {900, 1000}};
	public static final double[][] TUNE_LOSE_LIFE = {{800, 250}, {}, {700, 250}};
	public static final double[][] TUNE_GAIN_LIFE = {{700, 250}, {}, {800, 250}};

	public static void playTuneAsync(double[][] tune) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				playTune(tune);
			}
		}).start();
	}

	public static void playTune(double[][] tune) {
		for (double[] frame : tune) {
			int frequency = frame == null ? 0 : frame.length > 0 ? (int) frame[0] : 0;
			int duration = frame == null ? 0 : frame.length > 1 ? (int) frame[1] : 125;
			double volume = frame == null ? 0 : frame.length > 2 ? frame[2] : 1.0;
			playToneSilent(frequency, duration, volume);
		}
	}

	public static void playToneAsync(int frequency, int millis) {
		playToneAsync(frequency, millis, 1.0);
	}

	public static void playToneAsync(int frequency, int millis, double volume) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				playToneSilent(frequency, millis, volume);
			}
		}).start();
	}

	public static void playToneSilent(int frequency, int millis) {
		playToneSilent(frequency, millis, 1.0);
	}

	public static void playToneSilent(int frequency, int millis, double volume) {
		try {
			playTone(frequency, millis, volume);
		}
		catch (LineUnavailableException | InterruptedException | IOException e) {
			// Do nothing
		}
	}

	public static void playTone(int frequency, int millis) throws LineUnavailableException, InterruptedException, IOException {
		playTone(frequency, millis, 1.0);
	}

	public static void playTone(int frequency, int millis, double volume) throws LineUnavailableException, IOException, InterruptedException {
		if (frequency <= 0 || volume <= 0 || sounds_playing.get() >= max_sounds) {
			Thread.sleep(millis);
			return;
		}
		sounds_playing.incrementAndGet();
		volume = Math.min(127, Math.max(volume, 0));
		int total_written = 0;
		int to_go = millis * 8;
		byte[] buffer = new byte[512];
		Clip clip = AudioSystem.getClip();
		clip.addLineListener(sound_listener);
		PipedOutputStream pout = new PipedOutputStream();
		PipedInputStream pin = new PipedInputStream(pout, to_go);
		while (to_go > 0) {
			int size = Math.min(to_go, buffer.length);
			for (int i = 0; i < size; i++) {
				if (FinalProject.GLOBAL_COLOR == null) {
					// Smooth sine wave
					double angle = ++total_written / (SAMPLE_RATE / frequency) * 2.0 * Math.PI;
					buffer[i] = (byte) (Math.sin(angle) * 127.0 * volume);
				}
				else {
					// Square wave
					buffer[i] = (byte) (Math.signum(Math.sin(frequency * 2.0 * Math.PI * ++total_written / SAMPLE_RATE)) * 127 * volume);
				}
			}
			pout.write(buffer, 0, size);
			to_go -= size;
		}
		pout.flush();
		AudioInputStream ain = new AudioInputStream(pin, format, millis * 8);
		clip.open(ain);
		clip.start();
		Thread.sleep(millis);
	}
}