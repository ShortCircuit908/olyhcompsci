package com.calebmilligan.compsci.projects.finalproject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Name: CoolTextThing.java
 * Description: Does a cool thing with text
 * <p>
 * Sorry for the poor quality of this code. I wrote it at around midnight
 * after about six hours of programming a library for a personal project,
 * so I was a little out of it. But hey, it works.
 *
 * @author Caleb Milligan
 *         Created on 5/27/2016.
 */
public class CoolTextThing {
	private static final Timer timer = new Timer();
	private CoolTextThing next;
	private final BufferedImage background;
	private final long duration;
	private final long delay;
	private final TimerTask task;
	private final Graphics g;
	private final Graphics buffer_graphics;
	private final Image buffer_image;
	private final double fade_step;
	private double fade;
	private long age;
	private final int offset_x;
	private final int offset_y;
	private final int text_width;
	private final int text_height;
	private final int its_really_late_and_im_too_tired_for_this;
	private final Location shake_location = new Location();
	private Runnable callback;

	public CoolTextThing(Graphics g, Image buffer_image, BufferedImage background, String text, long delay, long duration_centisecond) {
		this.duration = duration_centisecond;
		this.delay = delay;
		this.background = background;
		this.g = g;
		this.buffer_graphics = buffer_image.getGraphics();
		buffer_graphics.setFont(Font.decode("default").deriveFont(Font.BOLD, 80));
		this.buffer_image = buffer_image;
		offset_x = (background.getWidth() - buffer_graphics.getFontMetrics().stringWidth(text)) / 2;
		offset_y = (background.getHeight() - buffer_graphics.getFontMetrics().getHeight()) / 2;
		text_height = buffer_graphics.getFontMetrics().getHeight();
		text_width = buffer_graphics.getFontMetrics().stringWidth(text);
		its_really_late_and_im_too_tired_for_this = offset_y - text_height + buffer_graphics.getFontMetrics().getMaxDescent();
		fade_step = 255.0 / (duration / 4L);
		task = new TimerTask() {
			@Override
			public void run() {
				shake_location.x = (Math.random() - 0.5) * 7;
				shake_location.y = (Math.random() - 0.5) * 7;
				if (age <= duration / 4L) {
					fade = age * fade_step;
				}
				if (age >= duration / 4L * 3L) {
					fade = (duration - age) * fade_step;
				}
				age++;
				if (age >= duration) {
					g.drawImage(background, 0, 0, null);
					cancel();
					timer.purge();
					if (callback != null) {
						callback.run();
					}
					if (next != null) {
						next.start();
					}
					else {
						buffer_graphics.dispose();
						g.dispose();
						timer.cancel();
					}
					return;
				}
				buffer_graphics.drawImage(background, 0, 0, null);
				buffer_graphics.setColor(new Color(0, 0, 0, (int) fade));
				buffer_graphics.fillRect((int) (offset_x + shake_location.x), (int) (its_really_late_and_im_too_tired_for_this + shake_location.y), text_width, text_height);
				buffer_graphics.setColor(new Color(255, 255, 255, (int) fade));
				buffer_graphics.drawString(text, (int) (offset_x + shake_location.x), (int) (offset_y + shake_location.y));
				g.drawImage(buffer_image, 0, 0, null);
			}
		};
	}

	public void start() {
		timer.schedule(task, delay * 10, 10);
	}

	public CoolTextThing chainNext(String text, long delay, long duration_centiseconds) {
		this.next = new CoolTextThing(g, buffer_image, background, text, delay, duration_centiseconds);
		return next;
	}

	public CoolTextThing setCallback(Runnable callback) {
		this.callback = callback;
		return this;
	}
}
