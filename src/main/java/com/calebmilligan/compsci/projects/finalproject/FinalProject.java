package com.calebmilligan.compsci.projects.finalproject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.Timer;

/**
 * @author Caleb Milligan
 *         Created on 5/11/2016.
 */
public class FinalProject {
	public static final Color ACTUAL_ORANGE = new Color(0xFF6A00);
	public static boolean balls_can_collide = true;
	private static long screen_shake_duration = 0;
	private static final Location screen_shake_offset = new Location();
	public static final int RESET_ALL = 0;
	public static final int RESET_LEVEL = 1;
	public static final int RESET_PLAYER = 2;
	public static final Dimension SIZE = new Dimension(800, 600);
	private static final File high_score_file = new File("high_score");
	private static final Color[] color_modes = {null, Color.GREEN, Color.WHITE};
	@SuppressWarnings("unchecked")
	private static final LinkedList<Image>[] icon_images = new LinkedList[color_modes.length];
	public static int high_score = 0;
	public static int score = 0;
	public static int level = 1;
	public static int state = 0;
	public static FinalProject INSTANCE;
	private static int color_mode = 0;
	public static Color GLOBAL_COLOR = color_modes[color_mode];
	private static boolean paused = false;
	private static int lives = 3;
	private final JFrame frame;
	private final Graphics2D graphics;
	private final Image buffer_image;
	private final Graphics2D buffer_graphics;
	private final LinkedList<GameObject> to_remove = new LinkedList<>();
	private final LinkedList<GameObject> game_objects = new LinkedList<>();
	private int reset_type = -1;
	private Font small_font;
	private Font big_font;


	public FinalProject() {
		INSTANCE = this;
		high_score = getHighScore();

		// Create the frame
		frame = new JFrame("Caleb Milligan - Breakout");
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationByPlatform(true);
		frame.getRootPane().setPreferredSize(SIZE);
		frame.pack();

		// Set the initial icons
		frame.setIconImages(generateIconImages());

		// Create the swap image
		buffer_image = frame.getRootPane().createImage(SIZE.width, SIZE.height);

		// Set up Graphics2D contexts
		graphics = (Graphics2D) frame.getRootPane().getGraphics();
		buffer_graphics = (Graphics2D) buffer_image.getGraphics();
		HashMap<RenderingHints.Key, Object> rendering_hints = new HashMap<>();
		rendering_hints.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		graphics.addRenderingHints(rendering_hints);
		buffer_graphics.addRenderingHints(rendering_hints);

		// Set up the fonts to use
		small_font = Font.decode("Courier New").deriveFont(Font.BOLD, SIZE.height / 25.0f);
		big_font = small_font.deriveFont(SIZE.height / 12.5f);
		buffer_graphics.setFont(small_font);

		// Set up the game
		reset(RESET_ALL);

		Timer game_timer = new Timer();
		// Start the redraw timer
		game_timer.schedule(new TimerTask() {
			@Override
			public void run() {
				draw(buffer_graphics);
				graphics.drawImage(buffer_image, 0, 0, null);
			}
		}, 0, 1000 / 60);

		// Start the update timer
		game_timer.schedule(new TimerTask() {
			@Override
			public void run() {
				update();
			}
		}, 0, 1000 / 60);

		// Initialize the Keyboard
		frame.addKeyListener(new Keyboard());

		// Set close listener
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				//ToneGen.closeDataLine();
				setHighScore(high_score);
				try {
					beEvil(frame);
				}
				catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});

		// Set focus listener (pause on focus lost)
		frame.addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowLostFocus(WindowEvent e) {
				paused = true;
			}
		});

		// Show the frame
		frame.setVisible(true);
	}

	private void beEvil(JFrame frame) throws AWTException {
		// For some reason, this only works on the primary monitor, even though it SHOULD work on any.
		GraphicsDevice device = frame.getGraphicsConfiguration().getDevice();
		DisplayMode mode = device.getDisplayMode();
		Robot robot = new Robot(device);
		Rectangle rect = new Rectangle(0, 0, mode.getWidth(), mode.getHeight());
		JFrame evil_frame = new JFrame(frame.getGraphicsConfiguration());
		evil_frame.setUndecorated(true);
		evil_frame.setResizable(false);
		evil_frame.setIconImages(frame.getIconImages());
		evil_frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		evil_frame.getRootPane().setPreferredSize(new Dimension(mode.getWidth(), mode.getHeight()));
		evil_frame.setAlwaysOnTop(true);
		evil_frame.pack();
		evil_frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setVisible(false);
		BufferedImage cap = robot.createScreenCapture(rect);
		evil_frame.setVisible(true);
		if (device.isFullScreenSupported()) {
			device.setFullScreenWindow(evil_frame);
		}
		Image buffer = evil_frame.getRootPane().createImage(cap.getWidth(), cap.getHeight());
		CoolTextThing thing = new CoolTextThing(evil_frame.getRootPane().getGraphics(), buffer, cap, "Did you think this was a game?", 0, 500);
		thing.chainNext("Only a game?", 100, 500)
				.chainNext("A simple little game?", 100, 500)
				.chainNext("Well", 100, 500)
				.chainNext("You were right", 100, 500)
				.chainNext("...", 200, 500)
				.chainNext("Toodles!", 300, 700)
				.setCallback(new Runnable() {
					@Override
					public void run() {
						evil_frame.dispose();
					}
				});
		thing.start();
	}

	public static void main(String... args) {
		//ToneGen.openDataLine();
		new FinalProject();
	}

	private static final List<Image> generateIconImages() {
		if (icon_images[color_mode] != null) {
			return icon_images[color_mode];
		}
		BufferedImage parent_image = new BufferedImage(16, 16, BufferedImage.TYPE_3BYTE_BGR);
		Graphics g = parent_image.getGraphics();
		Color[] layer_colors = {Color.RED, ACTUAL_ORANGE, Color.YELLOW, Color.GREEN};
		g.setColor(GLOBAL_COLOR != null ? GLOBAL_COLOR : layer_colors[0]);
		g.fillRect(0, 0, 1, 3);
		g.fillRect(2, 0, 7, 3);
		g.fillRect(10, 0, 6, 3);
		g.setColor(GLOBAL_COLOR != null ? GLOBAL_COLOR : layer_colors[1]);
		g.fillRect(0, 4, 5, 3);
		g.fillRect(6, 4, 7, 3);
		g.fillRect(14, 4, 2, 3);
		g.setColor(GLOBAL_COLOR != null ? GLOBAL_COLOR : layer_colors[2]);
		g.fillRect(0, 8, 1, 3);
		g.fillRect(2, 8, 7, 3);
		g.fillRect(10, 8, 6, 3);
		g.setColor(GLOBAL_COLOR != null ? GLOBAL_COLOR : layer_colors[3]);
		g.fillRect(0, 12, 5, 3);
		g.fillRect(6, 12, 7, 3);
		g.fillRect(14, 12, 2, 3);

		LinkedList<Image> icons = new LinkedList<>();
		int[] sizes = {24, 32, 48, 256};
		icons.add(parent_image);
		for (int size : sizes) {
			icons.add(parent_image.getScaledInstance(size, size, BufferedImage.SCALE_FAST));
		}
		icon_images[color_mode] = icons;
		return icons;
	}

	public static int getHighScore() {
		if (!high_score_file.exists()) {
			return 0;
		}
		try {
			DataInputStream in = new DataInputStream(new FileInputStream(high_score_file));
			int score = UnsignedConversion.signedToUnsigned(in.readShort());
			in.close();
			return score;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static void setHighScore(int score) {
		try {
			if (high_score_file.getParent() != null) {
				new File(high_score_file.getParent()).mkdirs();
			}
			high_score_file.createNewFile();
			DataOutputStream out = new DataOutputStream(new FileOutputStream(high_score_file));
			out.writeShort(UnsignedConversion.unsignedToSigned(score));
			out.flush();
			out.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void removeGameObject(GameObject object) {
		synchronized (to_remove) {
			to_remove.add(object);
		}
	}

	public <T extends GameObject> LinkedList<T> getGameObjects(Class<T> clazz) {
		LinkedList<T> list = new LinkedList<>();
		synchronized (game_objects) {
			for (GameObject object : game_objects) {
				if (clazz == null || object.getClass().isAssignableFrom(clazz)) {
					list.add((T) object);
				}
			}
		}
		return list;
	}

	private void draw(Graphics2D g) {
		final boolean do_screen_shake = screen_shake_duration > 0;
		final double screen_shake_offset_x = screen_shake_offset.x, screen_shake_offset_y = screen_shake_offset.y;
		g.setBackground(Color.BLACK);
		g.clearRect(0, 0, SIZE.width, SIZE.height);
		if (do_screen_shake && screen_shake_offset_x != 0 && screen_shake_offset_y != 0) {
			g.translate(screen_shake_offset_x, screen_shake_offset_y);
		}
		synchronized (game_objects) {
			for (GameObject object : game_objects) {
				object.draw(g);
			}
		}
		g.setColor(GLOBAL_COLOR != null ? GLOBAL_COLOR : Color.WHITE);
		g.setXORMode(Color.BLACK);
		FontMetrics metrics = g.getFontMetrics();
		g.drawString("Level: " + level, (int) (metrics.getHeight() / 2.0), (int) (SIZE.height - metrics.getHeight() * 1.5));
		g.drawString("Lives: " + lives, metrics.getHeight() / 2, (int) (SIZE.height - metrics.getHeight() / 2.0));
		String score_str_0 = "Score : " + (score * 100);
		int width_0 = (int) (SIZE.width - metrics.stringWidth(score_str_0) - metrics.getHeight() / 2.0);
		String score_str_1 = "Record: " + (high_score * 100);
		int width_1 = (int) (SIZE.width - metrics.stringWidth(score_str_1) - metrics.getHeight() / 2.0);
		g.drawString(score_str_0, Math.min(width_0, width_1), (int) (SIZE.height - metrics.getHeight() * 1.5));
		g.drawString(score_str_1, Math.min(width_0, width_1), (int) (SIZE.height - metrics.getHeight() / 2.0));
		if (paused) {
			g.setFont(big_font);
			centerText(g, "PAUSED", "PRESS \"ESC\" TO RESUME");
			g.setFont(small_font);
		}
		else if (state == 2) {
			g.setFont(big_font);
			centerText(g, "GAME OVER", "PRESS \"SPACE\" TO RESTART");
			g.setFont(small_font);
		}
		else if (state == 0) {
			centerText(g, "A/D/LEFT/RIGHT TO MOVE", "PRESS \"SPACE\" TO START");
		}
		g.setPaintMode();
		if (do_screen_shake && screen_shake_offset_x != 0 && screen_shake_offset_y != 0) {
			g.translate(-screen_shake_offset_x, -screen_shake_offset_y);
		}
	}

	private void centerText(Graphics2D g, String... text) {
		FontMetrics metrics = g.getFontMetrics();
		float offset_y = (SIZE.height - (metrics.getHeight() * text.length + ((SIZE.height / 60.0f) * text.length - 1))) / 2.0f;
		int height = metrics.getHeight();
		for (int i = 0; i < text.length; i++) {
			String line = text[i];
			float offset_x = (SIZE.width - metrics.stringWidth(line)) / 2.0f;
			g.drawString(line, offset_x, offset_y + (height * i) + ((SIZE.height / 60.0f) * i));
		}
	}

	public static void setScreenShakeDuration(long ticks) {
		screen_shake_duration = ticks;
	}

	private void update() {
		// Press F to step one frame
		if (!paused || Keyboard.isKeyTriggered(Keyboard.VK_F)) {
			// Perform screen shake
			if (screen_shake_duration > 0) {
				screen_shake_offset.x = (Math.random() - 0.5) * 7;
				screen_shake_offset.y = (Math.random() - 0.5) * 7;
				screen_shake_duration--;
			}
			// Perform any queued reset
			if (reset_type >= 0) {
				doReset();
			}
			// Update all objects
			synchronized (game_objects) {
				for (GameObject object : game_objects) {
					object.update();
				}
			}
			// Remove all objects if needed
			synchronized (to_remove) {
				game_objects.removeAll(to_remove);
				to_remove.clear();
			}
			// In play
			if (state == 1) {
				// Level won
				if (getGameObjects(Brick.class).isEmpty()) {
					if (++level % 5 == 0) {
						lives++;
						AdvancedToneGen.playTuneAsync(AdvancedToneGen.TUNE_GAIN_LIFE);
					}
					reset(RESET_LEVEL);
				}
				// Level lost
				if (getGameObjects(Ball.class).isEmpty()) {
					// No lives left
					if (lives <= 0) {
						state = 2;
						AdvancedToneGen.playTuneAsync(AdvancedToneGen.TUNE_GAME_OVER);
					}
					// Some lives left
					else {
						lives--;
						AdvancedToneGen.playTuneAsync(AdvancedToneGen.TUNE_LOSE_LIFE);
						INSTANCE.reset(RESET_PLAYER);
					}
				}
			}
			// Reset after game over
			else if (state == 2 && Keyboard.isKeyTriggered(Keyboard.VK_SPACE)) {
				reset(RESET_ALL);
			}
			// Cheat
			if (Keyboard.isKeyPressed(Keyboard.VK_SPACE) && Keyboard.isKeyPressed(Keyboard.VK_SHIFT) && state == 1) {
				game_objects.add(new Ball());
			}
			// Start game
			else if (Keyboard.isKeyTriggered(Keyboard.VK_SPACE) && state != 1) {
				state = 1;
			}
		}
		if (Keyboard.isKeyTriggered(Keyboard.VK_ESCAPE)) {
			// Swap color mode
			if (Keyboard.isKeyPressed(Keyboard.VK_SHIFT)) {
				color_mode++;
				if (color_mode >= color_modes.length) {
					color_mode = 0;
				}
				GLOBAL_COLOR = color_modes[color_mode];
				frame.setIconImages(generateIconImages());
			}
			// Toggle pause
			else {
				paused ^= true;
			}
		}
		// Toggle ball collision
		if (Keyboard.isKeyTriggered(Keyboard.VK_B) && Keyboard.isKeyPressed(Keyboard.VK_SHIFT)) {
			balls_can_collide ^= true;
		}
		Keyboard.update();
	}

	public void reset(int reset_type) {
		if (this.reset_type < 0) {
			this.reset_type = reset_type;
		}
	}

	private void doReset() {
		state = 0;
		synchronized (to_remove) {
			to_remove.clear();
		}
		synchronized (game_objects) {
			LinkedList<GameObject> affected = new LinkedList<>();
			for (GameObject object : game_objects) {
				if (reset_type == RESET_ALL || reset_type == RESET_LEVEL || !(object instanceof Brick)) {
					affected.add(object);
				}
			}
			game_objects.removeAll(affected);
			to_remove.clear();
		}
		game_objects.add(new Paddle());
		int num_balls = level % 5 == 0 ? level / 2 : 1;
		for (int i = 0; i < num_balls; i++) {
			game_objects.add(new Ball());
		}
		if (reset_type == RESET_ALL || reset_type == RESET_LEVEL) {
			if (reset_type == RESET_ALL) {
				level = 1;
				score = 0;
				lives = 3;
			}
			double padding_x = SIZE.width / 12.0 / 12.0;
			Color[] layer_colors = {Color.RED, ACTUAL_ORANGE, Color.YELLOW, Color.GREEN};
			for (int y = 0; y < 4; y++) {
				for (int x = 0; x < 12; x++) {
					Brick brick = new Brick(0, 0, layer_colors[y]);
					brick.origin.x = x * (brick.getWidth() + padding_x) + (padding_x / 2);
					brick.origin.y = y * (brick.getHeight() + padding_x) + (padding_x / 2);
					game_objects.add(brick);
				}
			}
		}
		reset_type = -1;
	}
}
