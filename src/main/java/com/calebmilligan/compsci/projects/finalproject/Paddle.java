package com.calebmilligan.compsci.projects.finalproject;

import java.awt.*;

import static com.calebmilligan.compsci.projects.finalproject.FinalProject.level;

/**
 * Name: Paddle.java
 * Description: Player-controlled paddle
 *
 * @author Caleb Milligan
 *         Created on 5/11/2016.
 */
public class Paddle extends GameObject {
	private static final Dimension paddle_size = new Dimension(FinalProject.SIZE.width / 4, FinalProject.SIZE.height / 20);
	private static final double paddle_speed = FinalProject.SIZE.width / (double) paddle_size.width;

	public Paddle() {
		origin.x = (FinalProject.SIZE.width - paddle_size.width) / 2;
		origin.y = (FinalProject.SIZE.height - (int) (paddle_size.height * 1.25));
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(FinalProject.GLOBAL_COLOR != null ? FinalProject.GLOBAL_COLOR : Color.CYAN);
		g.fillRect((int) origin.x, (int) origin.y, paddle_size.width, paddle_size.height);
	}

	@Override
	public void update() {
		if (FinalProject.state == 2) {
			return;
		}
		// Get keyboard input
		boolean moving_right = Keyboard.isKeyPressed(Keyboard.VK_RIGHT) || Keyboard.isKeyPressed(Keyboard.VK_D);
		boolean moving_left = Keyboard.isKeyPressed(Keyboard.VK_LEFT) || Keyboard.isKeyPressed(Keyboard.VK_A);
		// Move right, if possible
		if (moving_right && !moving_left && origin.x + paddle_size.width + (int) (paddle_size.height * 0.25) < FinalProject.SIZE.width) {
			origin.x += paddle_speed * (level * 0.25 + 1);
			origin.x = Math.min(origin.x, FinalProject.SIZE.width - (paddle_size.width + (int) (paddle_size.height * 0.25)));
			if (FinalProject.state == 0) {
				// Move the ball with the paddle
				for (Ball ball : FinalProject.INSTANCE.getGameObjects(Ball.class)) {
					ball.origin.x = (origin.x + (paddle_size.width - ball.getWidth()) / 2.0);
				}
			}
		}
		// Move left, if possible
		if (moving_left && !moving_right && origin.x - (int) (paddle_size.height * 0.25) > 0) {
			origin.x -= paddle_speed * (level * 0.25 + 1);
			origin.x = Math.max(origin.x, (int) (paddle_size.height * 0.25));
			if (FinalProject.state == 0) {
				// Move the ball with the paddle
				for (Ball ball : FinalProject.INSTANCE.getGameObjects(Ball.class)) {
					ball.origin.x = (origin.x + (paddle_size.width - ball.getWidth()) / 2.0);
				}
			}
		}
	}

	@Override
	public int getWidth() {
		return paddle_size.width;
	}

	@Override
	public int getHeight() {
		return paddle_size.height;
	}
}
