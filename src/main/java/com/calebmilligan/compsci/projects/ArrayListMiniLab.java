package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * Name: ArrayListMiniLab.java
 * Description: Practice with ArrayLists
 *
 * @author Caleb Milligan
 *         Created on 2/3/2016
 */
@ProjectId(33)
public class ArrayListMiniLab {
	public static void main(String... args) {
		ArrayList<String> teachers = new ArrayList<>();
		teachers.add("Mz. Nunn");
		teachers.add("Mz. Hart");
		teachers.add("Mr. Turnbow");
		teachers.add("Mz. Houge");
		teachers.add("Mr. Beattie");
		teachers.add("Mz. Madsen");
		System.out.println(teachers);
		for (int i = 1; i < 10; i += 2) {
			teachers.add(i, "Passing Period");
		}
		System.out.println(teachers);
		teachers.removeIf(new Predicate<String>() {
			@Override
			public boolean test(String s) {
				return s != null && s.equalsIgnoreCase("passing period");
			}
		});
		System.out.println(teachers);
		teachers.set(0, "Mr. Kabat");
		teachers.set(1, "Sra. Chan");
		teachers.set(3, "Mr. Wright");
		teachers.set(2, "mr. Jacobs");
		teachers.set(4, "Mz. Hart");
		teachers.set(5, "Mz. Boyer");
		System.out.println("2nd period: " + teachers.get(1));
		System.out.println("6th period: " + teachers.get(5));
	}
}
