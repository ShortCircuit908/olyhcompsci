package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Name: Lab10b.java
 * Description: Error handling
 *
 * @author Caleb Milligan
 *         Created on 2/23/2016
 */
@ProjectId(36)
public class Lab10b {
	public static void main(String... args) {
		int[] nums = new int[50];
		Random random = new Random();
		for (int i = 0; i < nums.length; i++) {
			nums[i] = random.nextInt(50) + 1;
		}
		System.out.println("nums = " + Arrays.toString(nums));
		Scanner scanner = new Scanner(System.in);
		int index = 0;
		int num = 0;
		System.out.println("Please input an index from 0 to 49");
		while (scanner.hasNextLine()) {
			try {
				num = nums[index = Integer.parseInt(scanner.nextLine())];
				break;
			}
			catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
				System.out.println("Please input an index from 0 to 49");
			}
		}
		scanner.close();
		System.out.println("The value at index " + index + " is " + num);
	}
}
