package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: Lab1b.java
 * Description: Initialis
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/16/2015
 */
@ProjectId(2)
public class Lab1b {
	private static final String[] lines = new String[]{
			"    CCCCCCCCCCCC        MMMM            MMMM",
			"    CCCCCCCCCCCC        MMMM            MMMM",
			"CCCC            CCCC    MMMMMMMM    MMMMMMMM",
			"CCCC            CCCC    MMMMMMMM    MMMMMMMM",
			"CCCC                    MMMM    MMMM    MMMM",
			"CCCC                    MMMM    MMMM    MMMM",
			"CCCC                    MMMM            MMMM",
			"CCCC                    MMMM            MMMM",
			"CCCC                    MMMM            MMMM",
			"CCCC                    MMMM            MMMM",
			"CCCC            CCCC    MMMM            MMMM",
			"CCCC            CCCC    MMMM            MMMM",
			"    CCCCCCCCCCCC        MMMM            MMMM",
			"    CCCCCCCCCCCC        MMMM            MMMM"
	};

	public static void main(String... args) {
		for (String line : lines) {
			System.out.println(line);
		}
	}
}
