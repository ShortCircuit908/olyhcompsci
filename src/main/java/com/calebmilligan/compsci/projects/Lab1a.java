package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: Lab1a.java
 * Description: First in-class assignment, all about me.
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/15/2015
 */
@ProjectId(1)
public class Lab1a {

	public static void main(String... args) {
		System.out.println(
				"Name: Caleb Milligan\n"
						+ "Birthday: 5/15/1998\n"
						+ "Hobbies: Programming\n"
						+ "Favorite book: The Number of the Beast by Robert Heinlein\n"
						+ "Favorite movie(s): The Harry Potter series\n"
		);
	}
}
