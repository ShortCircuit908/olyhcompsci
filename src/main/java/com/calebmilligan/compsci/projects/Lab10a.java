package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: Lab10b.java
 * Description: Error handling
 *
 * @author Caleb Milligan
 *         Created on 2/18/2016
 */
@ProjectId(35)
public class Lab10a {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter numbers to add\nType \"p\" to start again\nType \"q\" to exit");
		outer_loop:
		while (true) {
			inner_loop:
			while (scanner.hasNextLine()) {
				int sum = 0;
				String line = scanner.nextLine();
				String[] parts = line.split("\\s");
				for (String part : parts) {
					if (part.toLowerCase().matches("q(uit)?")) {
						break outer_loop;
					}
					if (part.equalsIgnoreCase("p")) {
						break inner_loop;
					}
					try {
						sum += Integer.parseInt(part.replace(",", ""));
					}
					catch (NumberFormatException e) {
						System.out.println(part + " is not a valid number!");
					}
				}
				System.out.println("The total is " + sum);
			}
		}
		scanner.close();
	}
}
