package com.calebmilligan.compsci.projects.graphics;

import com.calebmilligan.compsci.projects.extra.Sierpinski;

import java.awt.*;

/**
 * Name: DrawPicture.java
 * Description: Practice graphics
 *
 * @author Caleb Milligan
 *         Created on 5/5/2016.
 */
public class DrawPicture extends DoubleBufferedJApplet {
	private int[][][] points = {
			{
					{0x5382A1}, {14, 77}, {19, 78}, {42, 78}, {52, 76}, {60, 72}
			},
			{
					{0x5382A1}, {19, 62}, {21, 65}, {29, 66}, {36, 65}, {40, 64}
			},
			{
					{0x5382A1}, {18, 53}, {16, 55}, {22, 57}, {31, 57}, {41, 56}
			},
			{
					{0x5382A1}, {19, 44}, {12, 47}, {17, 49}, {25, 50}, {36, 49}, {46, 47}
			},
			{
					{0x5382A1}, {48, 58}, {56, 54}, {59, 48}, {56, 44}, {52, 43}
			},
			{
					{0xE76F00}, {30, 41}, {26, 37}, {23, 32}, {23, 27}, {27, 22}, {38, 13}, {40, 7}, {40, 1}
			},
			{
					{0xE76F00}, {35, 45}, {38, 40}, {35, 34}, {34, 28}, {37, 23}, {43, 19}, {47, 17}
			}
	};

	@Override
	public void doPaint(final Graphics2D g) {
		double step = getWidth() / 255.0;
		for (int i = 0; i < 255; i++) {
			g.setColor(new Color(i, i, i));
			g.fillRect((int) (i * step), (int) (i * step), (int) (getWidth() - (i * step * 2)), (int) (getHeight() - (i * step * 2)));
		}
		double w = 64, h = 85, scale = w / getWidth() > h / getHeight() ? getWidth() / w : getHeight() / h;
		g.setColor(new Color(0xC0C0C0));
		int offset_x = (int) ((getWidth() - w * scale) / 2);
		int offset_y = (int) ((getHeight() - h * scale) / 2);
		Sierpinski.drawSierpinski(g, getWidth(), getHeight(), 8);
		int x, y, px, py;
		for (int[][] line : points) {
			px = -1;
			py = -1;
			g.setColor(new Color(line[0][0]));
			for (int i = 1; i < line.length; i++) {
				x = (int) (line[i][0] * scale) + offset_x;
				y = (int) (line[i][1] * scale) + offset_y;
				if (px > -1 && py > -1) {
					g.drawLine(px, py, x, y);
				}
				px = x;
				py = y;
			}
		}
	}
}
