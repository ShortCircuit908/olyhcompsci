package com.calebmilligan.compsci.projects.graphics;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.applet.Applet;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Name: RandomBuildings.java
 * Description: Generated graphics
 *
 * @author Caleb Milligan
 *         Created on 5/13/2016.
 */
public class RandomBuildings extends DoubleBufferedJApplet {
	// Try not to let anyone see this key, as it's linked with my own
	// account. Change this to any other value, or replace it with your
	// own API key (https://home.openweathermap.org/users/sign_up)
	private static final String openweathermap_api_key = "5a6cbeaaf09b5375d2d16213c27074e6";

	// List of city IDs at http://openweathermap.org/help/city_list.txt
	// Set to -1 to cycle through all weather
	// 5805687 = Olympia
	private static final int city_id = 5805687;

	// Increasing this value speeds up time
	private static final double time_multiplier = 1;

	private static final Random random = new Random();
	private static final Color moon_color = new Color(0xFFFFED);
	private static final Color sun_color = new Color(0xFFFFAD);
	private static final Color window_light_color = new Color(0xFFFFBB);
	private static final Color window_dark_color = new Color(0x2B2B2B);
	private static final Color building_color = new Color(0x202020);
	private static final Font font = Font.decode("default").deriveFont(24.0f);
	private static final int window_width = 20;
	private static final double lightning_variance_modifier = 0.40;
	private static final double lightning_decrease_modifier = 0.55;
	private static final int lightning_minimum_length = 50;

	private final Timer update_timer = new Timer();
	private final Timer draw_timer = new Timer();
	private final Timer weather_timer = new Timer();
	private Weather weather = Weather.STORM;
	private String city_name;
	private Color sky_color = getTimeColor();
	private Color rain_color = new Color(0x6F6666FF, true);
	private Color cloud_color = new Color(0xB0B0B0);
	private Color haze_color = new Color(0x0, true);
	private final LinkedList<Paintable> objects = new LinkedList<>();
	private volatile boolean weather_success = true;
	private volatile boolean fetching_weather = false;
	private int fallback_weather_index = 0;

	@Override
	public void init() {
		//RENDERING_HINTS.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//RENDERING_HINTS.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		//RENDERING_HINTS.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		//RENDERING_HINTS.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		objects.add(new SunMoon());

		objects.add(new Haze());

		objects.add(new Lightning());

		int num_objs = random.nextInt(5) + 5;
		for (int i = 0; i < num_objs; i++) {
			objects.add(new Building(random.nextInt(2) + 5, random.nextInt(20) + 5));
		}

		num_objs = random.nextInt(10) + 5;
		for (int i = 0; i < num_objs; i++) {
			objects.add(new Car());
		}

		objects.add(new Rain());

		num_objs = random.nextInt(10) + 10;
		for (int i = 0; i < num_objs; i++) {
			objects.add(new Cloud());
		}

		update_timer.schedule(new TimerTask() {
			@Override
			public void run() {
				for (Paintable object : objects) {
					object.update();
				}
			}
		}, 0, 10);

		draw_timer.schedule(new TimerTask() {
			@Override
			public void run() {
				sky_color = getTimeColor();
				RandomBuildings.this.repaint();
			}
		}, 0, 10);

		weather_timer.schedule(new TimerTask() {
			@Override
			public void run() {
				weather = getWeather();
				switch (weather) {
					case SNOW:
						rain_color = Color.WHITE;
						haze_color = new Color(0x50FFFFFF, true);
						cloud_color = new Color(0x2E2E2E);
						break;
					case STORM:
					case RAIN:
						rain_color = new Color(0x6F6666FF, true);
						haze_color = new Color(0x90000000, true);
						cloud_color = new Color(0x2E2E2E);
						break;
					default:
						haze_color = new Color(0x0, true);
						cloud_color = new Color(0xB0B0B0);
						break;
				}
			}
		}, 0, 10000);
	}

	@Override
	public void doPaint(Graphics2D g) {
		g.setFont(font);
		g.setBackground(sky_color);
		g.clearRect(0, 0, getWidth(), getHeight());

		double scale_x = ((double) getWidth() / window_width) / 100.0;
		double scale_y = ((double) getHeight() / window_width) / 55.0;
		double scale = Math.min(scale_x, scale_y);

		for (Paintable object : objects) {
			object.paint(this, g, scale);
		}

		g.setXORMode(Color.BLACK);
		Color c = g.getColor();
		g.setColor(Color.WHITE);
		if (city_id != -1) {
			if (fetching_weather) {
				g.drawString("Fetching weather...", 0, g.getFontMetrics().getHeight());
			}
			else if (!weather_success) {
				g.drawString("Failed to contact weather service", 0, g.getFontMetrics().getHeight());
			}
			else {
				g.drawString(city_name + " weather: " + weather, 0, g.getFontMetrics().getHeight());
			}
		}
		g.setColor(c);
		g.setPaintMode();
	}

	public interface Paintable {
		void update();

		void paint(Applet applet, Graphics2D g, double scale);
	}

	public class Car implements Paintable {
		private double speed;
		private double x = Double.NaN;
		private final Color color;

		public Car() {
			speed = random.nextDouble() + 0.6;
			color = new Color(
					(random.nextInt(6) + 2) * 32,
					(random.nextInt(6) + 2) * 32,
					(random.nextInt(6) + 2) * 32
			);
		}

		@Override
		public void update() {

		}

		@Override
		public void paint(Applet applet, Graphics2D g, double scale) {
			if (x != x) {
				x = (random.nextDouble() * applet.getWidth()) / scale;
			}
			x += speed;
			if (x * scale > applet.getWidth()) {
				x = -3 * window_width * scale;
				speed = random.nextDouble() + 0.6;
			}
			g.setColor(color);
			double offset_y = Math.sin(x / 10.0);
			g.fillOval(
					(int) (x * scale + (window_width / 2.0 * scale)),
					(int) (applet.getHeight() - (window_width / 1.5 * scale) + offset_y),
					(int) (3 * window_width * scale),
					(int) (2 * window_width * scale)
			);
			g.setColor(Color.BLACK);
			g.fillOval(
					(int) ((x + window_width) * scale),
					(int) (applet.getHeight() - (0.25 * window_width * scale) + offset_y),
					(int) (window_width / 2.0 * scale),
					(int) (window_width / 2.0 * scale)
			);
			g.fillOval(
					(int) (x * scale + (2.25 * window_width * scale)),
					(int) (applet.getHeight() - (0.25 * window_width * scale) + offset_y),
					(int) (window_width / 2.0 * scale),
					(int) (window_width / 2.0 * scale)
			);
		}
	}

	public class SunMoon implements Paintable {
		private double angle;

		@Override
		public void update() {
			Calendar c = Calendar.getInstance();
			// Get total hours
			double hours = (
					(
							c.get(Calendar.HOUR_OF_DAY)
									+ c.get(Calendar.MINUTE) / 60.0
									+ c.get(Calendar.SECOND) / 3600.0
									+ c.get(Calendar.MILLISECOND) / 3600000.0
					) * time_multiplier) % 24.0;
			angle = (((360.0 / 24.0)) * hours);
		}

		@Override
		public void paint(Applet applet, Graphics2D g, double scale) {
			double diameter = 20.0 * window_width * scale;

			double h = ((applet.getWidth() - diameter) / 2.0);

			double orbit = applet.getWidth() / 2.0;
			double k = applet.getHeight() - Math.min(applet.getHeight() - orbit, orbit - applet.getHeight());

			double x = Math.sin(angle * (Math.PI / 180.0));
			double y = Math.cos(angle * (Math.PI / 180.0));

			g.setColor(sun_color);
			g.fillOval((int) (h + orbit * x), (int) (k + orbit * y), (int) diameter, (int) diameter);
			g.setColor(moon_color);
			g.fillOval((int) (h - orbit * x), (int) (k - orbit * y), (int) diameter, (int) diameter);
		}
	}

	public class Haze implements Paintable {

		@Override
		public void update() {

		}

		@Override
		public void paint(Applet applet, Graphics2D g, double scale) {
			g.setColor(haze_color);
			g.fillRect(0, 0, applet.getWidth(), applet.getHeight());
		}
	}

	public class Lightning implements Paintable {
		private int flash_ticks = 0;
		private final LinkedList<List<Point>> bolts = new LinkedList<>();

		@Override
		public void update() {
			if (flash_ticks <= 0) {
				synchronized (bolts) {
					bolts.clear();
				}
				if (random.nextInt(200) == 0) {
					flash_ticks = random.nextInt(10) + 30;
				}
			}
			else {
				flash_ticks--;
			}
		}

		@Override
		public void paint(Applet applet, Graphics2D g, double scale) {
			if (weather != Weather.STORM || 100 * scale < 1.0) {
				return;
			}
			if (flash_ticks > 0) {
				if (bolts.isEmpty()) {
					Point p1 = new Point(random.nextInt(applet.getWidth()), 0);
					Point p2 = new Point(
							p1.x + random.nextInt((int) (200 * scale)) - (int) (100 * scale),
							applet.getHeight()
					);
					drawLightning(p1, p2, scale);
				}
				g.setColor(new Color(0x4FFFFFFF, true));
				g.fillRect(0, 0, applet.getWidth(), applet.getHeight());
				g.setColor(Color.WHITE);
				synchronized (bolts) {
					for (List<Point> bolt : bolts) {
						for (int i = 0; i < bolt.size() - 1; i++) {
							g.drawLine(bolt.get(i).x, bolt.get(i).y, bolt.get(i + 1).x, bolt.get(i + 1).y);
						}
					}
				}
			}
		}

		private void drawLightning(Point start, Point end, double scale) {
			synchronized (bolts) {
				bolts.add(genLightning(start, end, scale));
			}
		}

		private List<Point> genLightning(Point start, Point end, double scale) {
			LinkedList<Point> bolt = new LinkedList<>();
			bolt.add(start);
			double length = Math.sqrt(Math.pow(start.x - end.x, 2) + Math.pow(start.y - end.y, 2));
			genLightning(start, end, bolt, length * lightning_variance_modifier, scale);
			return bolt;
		}

		private void genLightning(Point start, Point end, List<Point> bolt, double variance, double scale) {
			double length = Math.sqrt(Math.pow(start.x - end.x, 2) + Math.pow(start.y - end.y, 2));
			if (length > lightning_minimum_length * scale) {
				int
						variance_x = (int) ((Math.random() * variance * 2) - variance),
						midpoint_x = (start.x + end.x) / 2 + variance_x,
						midpoint_y = (start.y + end.y) / 2;
				Point mid = new Point(midpoint_x, midpoint_y);
				genLightning(start, mid, bolt, variance * lightning_decrease_modifier, scale);
				genLightning(mid, end, bolt, variance * lightning_decrease_modifier, scale);
				if (random.nextBoolean()) {
					int ex = midpoint_x + (int) (random.nextInt((int) (length * 1.5)) - (length * .75));
					int ey = midpoint_y + random.nextInt((int) length / 2);
					drawLightning(mid, new Point(ex, ey), scale);
				}
			}
			else {
				bolt.add(end);
			}
		}
	}

	public class Cloud implements Paintable {
		private double speed;
		private final double max_width = 20;
		private final double width;
		private double x = Double.NaN;
		private LinkedList<Ellipse2D.Double> components = new LinkedList<>();

		public Cloud() {
			speed = random.nextDouble() + 0.6;
			int num_components = random.nextInt(5) + 10;
			double temp_width = 0;
			for (int i = 0; i < num_components; i++) {
				double r = (random.nextDouble() * max_width / 2) + max_width / 2;
				Ellipse2D.Double ellipse = new Ellipse2D.Double(
						random.nextInt((int) (max_width * 4)),
						random.nextDouble() * max_width,
						r,
						r
				);
				components.add(ellipse);
				temp_width = Math.max(temp_width, ellipse.x + r);
			}
			width = temp_width;
		}

		@Override
		public void update() {
		}

		@Override
		public void paint(Applet applet, Graphics2D g, double scale) {
			if (weather == Weather.CLEAR) {
				return;
			}
			if (x != x) {
				x = (random.nextDouble() * applet.getWidth()) / scale;
			}
			x += speed;
			if (x * scale > applet.getWidth()) {
				x = -width * 10;
				speed = random.nextDouble() + 0.6;
			}
			g.setColor(cloud_color);
			for (Ellipse2D.Double component : components) {
				g.fillOval(
						(int) ((x + component.x * 10) * scale),
						(int) ((component.y - 30) * scale + Math.sin(((x + component.x * 10) * scale) / (50.0 * scale)) * (10 * scale)),
						(int) (component.width * 10 * scale),
						(int) (component.height * 10 * scale)
				);
			}
		}
	}

	public class Rain implements Paintable {
		private double y, x;
		private Point[] drops;

		public Rain() {
			drops = new Point[random.nextInt(8) + 8];
			for (int i = 0; i < drops.length; i++) {
				drops[i] = new Point(random.nextInt(256), random.nextInt(256));
			}
		}

		@Override
		public void update() {
			y++;
			x += 0.45;
		}

		@Override
		public void paint(Applet applet, Graphics2D g, double scale) {
			if (weather == Weather.CLEAR || weather == Weather.CLOUDY) {
				return;
			}
			g.setColor(rain_color);
			double w = 256 * scale;
			if (y >= w) {
				y = 0;
			}
			if (x >= w) {
				x = 0;
			}
			if (scale * 5 < 1.0) {
				return;
			}
			// Tesselate rain to cover applet
			for (double y = -w; y < applet.getHeight(); y += w) {
				for (double x = -w; x < applet.getWidth(); x += w) {
					for (Point point : drops) {
						int pos_x = (int) (point.x * scale + x + this.x);
						int pos_y = (int) (point.y * scale + y + this.y);
						g.fillRect(pos_x, pos_y, (int) (5 * scale), (int) (5 * scale));
					}
				}
			}
		}
	}

	public class Building implements Paintable {
		private final boolean has_cap;
		private final int x;
		private final int building_width;
		private final int building_height;
		private final boolean[][] window_states;

		public Building(int width, int height) {
			this.x = random.nextInt(50 - (width + 1)) * 2;
			has_cap = random.nextInt(3) == 0;
			building_width = width * 2 + 1;
			building_height = height * 2 + 1;
			window_states = new boolean[height][width];
		}

		@Override
		public void update() {
			// Randomly toggle window states
			for (boolean[] row : window_states) {
				for (int i = 0; i < row.length; i++) {
					if (random.nextInt(10000) == 0) {
						row[i] ^= true;
					}
				}
			}
		}

		@Override
		public void paint(Applet applet, Graphics2D g, double scale) {
			g.setColor(building_color);
			int w = (int) (building_width * window_width * scale);
			int h = (int) (building_height * window_width * scale);
			if (has_cap) {
				double p1x = x * scale * window_width;
				double p1y = applet.getHeight() - building_height * window_width * scale;
				double p2x = p1x + w;
				double p3x = (p1x + p2x) / 2.0;
				double p3y = p1y - w / 4.0;
				g.fillPolygon(
						new int[]{(int) p1x, (int) p2x, (int) p3x},
						new int[]{(int) Math.ceil(p1y), (int) Math.ceil(p1y), (int) Math.ceil(p3y)},
						3
				);
				g.fillPolygon(
						new int[]{(int) (p1x + w / 3), (int) (p2x - w / 3), (int) p3x},
						new int[]{(int) Math.ceil(p1y), (int) Math.ceil(p1y), (int) Math.ceil(p3y - w / 3)},
						3
				);
			}
			g.fillRect(
					(int) (x * scale * window_width),
					applet.getHeight() - (int) (building_height * window_width * scale),
					w,
					h
			);
			if (scale * window_width < 1.0) {
				return;
			}
			for (int y = 0; y < window_states.length; y++) {
				for (int x = 0; x < window_states[y].length; x++) {
					g.setColor(window_states[y][x] ? window_light_color : window_dark_color);
					g.fillRect(
							(int) ((this.x * scale * window_width) + (scale * (x * 2 + 1) * window_width)),
							applet.getHeight() - (int) (scale * (y * 2 + 2) * window_width),
							(int) (scale * window_width),
							(int) (scale * window_width)
					);
				}
			}
		}
	}

	public static Color getTimeColor() {
		Calendar c = Calendar.getInstance();
		// Get total hours
		double hours = (
				(
						c.get(Calendar.HOUR_OF_DAY)
								+ c.get(Calendar.MINUTE) / 60.0
								+ c.get(Calendar.SECOND) / 3600.0
								+ c.get(Calendar.MILLISECOND) / 3600000.0
				) * time_multiplier) % 24.0;
		// Standard sine wave for green/blue values
		float gb_val = (float) Math.sin(Math.PI / 24.0 * hours);
		// Compound sine wave for red values (makes a nice sunrise/sunset)
		float r_val = (float) Math.sin(2 * gb_val);
		return new Color(r_val * 0.75f, gb_val, gb_val);
	}

	public Weather getWeather() {
		fetching_weather = true;
		Weather weather = Weather.CLEAR;
		if (city_id != -1) {
			try {
				// Open connection to weather service
				HttpURLConnection connection = (HttpURLConnection)
						new URL("http://api.openweathermap.org/data/2.5/weather?id=" + city_id
								+ "&APPID=" + openweathermap_api_key).openConnection();
				// Prep for reading the response
				byte[] buffer = new byte[512];
				InputStream in = connection.getInputStream();
				StringBuilder builder = new StringBuilder();
				int read;
				// Append data to string builder
				while ((read = in.read(buffer)) > 0) {
					builder.append(new String(buffer, 0, read));
				}
				// Disconnect
				in.close();
				connection.disconnect();
				// Parse the response
				JSONParser parser = new JSONParser();
				JSONObject object = (JSONObject) parser.parse(builder.toString());
				long response_code = Long.valueOf(object.get("cod") + "");
				if (response_code != 200) {
					throw new IOException("Status code " + response_code);
				}
				// Get the city/country name and weather ID
				city_name = object.get("name") + ", " + ((JSONObject) object.get("sys")).get("country");
				long weather_id = (Long) ((JSONObject) ((JSONArray) object.get("weather")).get(0)).get("id");
				// Simplify into categories
				if (weather_id >= 200 && weather_id <= 299) {
					weather = Weather.STORM;
				}
				else if ((weather_id >= 300 && weather_id <= 399) || (weather_id >= 500 && weather_id <= 599)) {
					weather = Weather.RAIN;
				}
				else if (weather_id >= 600 && weather_id <= 699) {
					weather = Weather.SNOW;
				}
				else if (weather_id >= 800 && weather_id <= 809) {
					weather = Weather.CLOUDY;
				}
				weather_success = true;
			}
			catch (IOException | ParseException e) {
				weather_success = false;
			}
		}
		else {
			weather_success = false;
		}
		if (!weather_success) {
			// Cycle through all weather values
			weather = Weather.values()[fallback_weather_index++];
			if (fallback_weather_index >= Weather.values().length) {
				fallback_weather_index = 0;
			}
		}
		fetching_weather = false;
		return weather;
	}

	public enum Weather {
		CLEAR,
		CLOUDY,
		RAIN,
		STORM,
		SNOW,
	}
}
