package com.calebmilligan.compsci.projects.graphics;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;

/**
 * Name: DrawFace.java
 * Description: Practice graphics
 *
 * @author Caleb Milligan
 *         Created on 5/6/2016.
 */
public class DrawFace extends DoubleBufferedJApplet {

	@Override
	public void doPaint(Graphics2D g) {
		final double w = 48, h = 64, mid_x = getWidth() / 2, mid_y = getHeight() / 2, scale = w / getWidth() > h / getHeight() ? getWidth() / w : getHeight() / h;
		g.setColor(new Color(0xFFEEBB));
		Ellipse2D oval = new Ellipse2D.Double(mid_x - 20 * scale, mid_y - 30 * scale, 40 * scale, 60 * scale);
		Shape clip = new Ellipse2D.Double(mid_x - 30 * scale, mid_y - 20 * scale, 60 * scale, 100 * scale);
		g.setClip(clip);
		g.fill(oval);
		Polygon p = new Polygon();
		p.addPoint((int) (mid_x - 20 * scale), (int) (mid_y - 30 * scale));
		p.addPoint((int) (mid_x + 20 * scale), (int) (mid_y - 30 * scale));
		p.addPoint((int) (mid_x + 20 * scale), (int) (mid_y - 15 * scale));
		g.setColor(Color.BLACK);
		boolean toggle = false;
		for (int i = 20; i >= -20; i--) {
			p.addPoint((int) (mid_x + (i) * scale), (int) (((toggle ^= true) ? mid_y - 6 * scale : mid_y - 2 * scale) + 4));
		}
		g.fill(p);
		g.setClip(null);
		drawEye(g, mid_x - 10 * scale, mid_y - scale, scale);
		drawEye(g, mid_x + 5 * scale, mid_y - scale, scale);
		clip = new Ellipse2D.Double(mid_x - 7 * scale, mid_y + 13 * scale, 14 * scale, 7 * scale);
		g.setClip(clip);
		oval = new Ellipse2D.Double(mid_x - 9 * scale, mid_y + 15 * scale, 3 * scale, 3 * scale);
		g.draw(oval);
		oval = new Ellipse2D.Double(mid_x + 6 * scale, mid_y + 15 * scale, 3 * scale, 3 * scale);
		g.draw(oval);
		Line2D.Double line = new Line2D.Double(mid_x - 6 * scale, mid_y + 16.5 * scale, mid_x + 6 * scale, mid_y + 16.5 * scale);
		g.draw(line);
	}

	private void drawEye(Graphics2D g, double x, double y, double scale) {
		Shape old_clip = g.getClip();
		Color old_color = g.getColor();
		g.setColor(Color.BLACK);
		Ellipse2D oval = new Ellipse2D.Double(x, y, 5 * scale, 7 * scale);
		g.fill(oval);
		g.setClip(oval);
		g.setColor(Color.WHITE);
		oval = new Ellipse2D.Double(x - .5 * scale, y + scale, 6 * scale, 7 * scale);
		g.fill(oval);
		g.setColor(new Color(0x562200));
		oval = new Ellipse2D.Double(x + scale, y + scale * 4.5, scale * 3, scale * 3);
		g.fill(oval);
		g.setClip(old_clip);
		g.setColor(old_color);
	}
}
