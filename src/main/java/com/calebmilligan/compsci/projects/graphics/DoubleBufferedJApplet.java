package com.calebmilligan.compsci.projects.graphics;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Name: DoubleBufferedJApplet.java
 * Description: Prevent undesirable graphic artifacts during repaint
 *
 * @author Caleb Milligan
 *         Created on 5/6/2016.
 */
public class DoubleBufferedJApplet extends JApplet {
	public static final Map<RenderingHints.Key, Object> RENDERING_HINTS = new HashMap<>();
	private boolean double_buffered = true;

	private Image image;
	private Graphics2D graphics;

	@Override
	public final void paint(Graphics g) {
		if (double_buffered) {
			// If the frame has been resized, dispose the old image
			if (image != null && (image.getWidth(this) != getWidth() || image.getHeight(this) != getHeight())) {
				image.flush();
				graphics.dispose();
				image = null;
				graphics = null;
			}
			// If there is no buffer image, create one
			if (image == null) {
				image = createImage(this.getWidth(), this.getHeight());
				graphics = (Graphics2D) image.getGraphics();
				graphics.addRenderingHints(RENDERING_HINTS);
			}
		}
		else {
			graphics = (Graphics2D) g;
		}
		graphics.setBackground(getBackground());
		graphics.clearRect(0, 0, getWidth(), getHeight());
		graphics.setColor(getForeground());
		doPaint(graphics);
		g.drawImage(image, 0, 0, this);
	}

	@Override
	public final boolean isDoubleBuffered() {
		return double_buffered;
	}

	@Override
	public final void destroy() {
		if (graphics != null) {
			image = null;
			graphics.dispose();
			graphics = null;
		}
		doDestroy();
	}

	public void doDestroy() {

	}

	public void setDoubleBuffered(boolean double_buffered) {
		this.double_buffered = double_buffered;
		if (!double_buffered && graphics != null) {
			image = null;
			graphics.dispose();
			graphics = null;
		}
	}

	public void doPaint(Graphics2D g) {

	}
}