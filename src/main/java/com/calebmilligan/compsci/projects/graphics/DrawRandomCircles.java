package com.calebmilligan.compsci.projects.graphics;

import java.awt.*;
import java.util.Random;

/**
 * Name: DrawRandomCircles.java
 * Description: Practice graphics
 *
 * @author Caleb Milligan
 *         Created on 5/6/2016.
 */
public class DrawRandomCircles extends DoubleBufferedJApplet {
	private final Random random = new Random();

	@Override
	public void doPaint(Graphics2D g) {
		int max_diameter = Math.min(getHeight(), getWidth()) / 3;
		int num_circles = random.nextInt(10) + 10;
		for (int i = 0; i < num_circles; i++) {
			int color = random.nextInt(0xFFFFFF);
			// Have to calculate alpha value separately because random.nextInt() doesn't like negative bounds
			int alpha = random.nextInt(0xFF) << 24;
			color |= alpha;
			new Color(3);
			g.setColor(new Color(color, true));
			int diameter = random.nextInt(max_diameter);
			int x = getWidth() <= diameter ? 0 : random.nextInt(getWidth() - diameter);
			int y = getHeight() <= diameter ? 0 : random.nextInt(getHeight() - diameter);
			g.fillOval(x, y, diameter, diameter);
		}
	}
}
