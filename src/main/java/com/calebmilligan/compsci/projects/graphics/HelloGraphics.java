package com.calebmilligan.compsci.projects.graphics;

import java.awt.*;

/**
 * @author Caleb Milligan
 *         Created on 5/5/2016.
 */
public class HelloGraphics extends DoubleBufferedJApplet {

	@Override
	public void doPaint(final Graphics2D g) {
		g.drawString("Hello, world!", 10, 50);
		circle(g);
	}

	public void circle(Graphics g) {
		g.setColor(new Color(0xFEAB0F));
		g.drawOval(150, 100, 80, 300);
		g.fillOval(200, 400, 125, 75);
	}
}
