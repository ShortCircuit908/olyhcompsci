package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.ArrayList;

/**
 * Name: WrapperClasses.java
 * Description: Practice with wrapper classes
 *
 * @author Caleb Milligan
 *         Created on 2/3/2016
 */
@ProjectId(32)
public class WrapperClasses {
	public static void main(String... args) {
		ArrayList<Integer> nums = new ArrayList<>(50);
		for (int i = 1; i <= 50; i++) {
			nums.add(i);
		}
		System.out.println(nums);
		int sum = 0;
		int size = nums.size();
		int offset = 0;
		for (int i = 0; i < size; i++) {
			if ((i + 1) % 5 == 0) {
				sum += nums.get(offset);
				System.out.print(nums.get(offset++));
				if (i + 1 < size) {
					System.out.print("+");
				}
			}
			else {
				nums.remove(0);
			}
		}
		System.out.println("=" + sum);
	}
}
