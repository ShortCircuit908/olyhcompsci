package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Name: Lab6b.java
 * Description: Methods (calculator)
 *
 * @author ShortCircuit908
 *         Created on 11/12/2015
 */
@ProjectId(25)
public class Lab6b {
	private static final DecimalFormat output_format = new DecimalFormat(",##0.##################");

	public static void main(String... args) {
		System.out.println("Welcome to my not-so-fancy quadratic formula calculator!\n");
		Scanner scanner = new Scanner(System.in);
		while (true) {
			String line;
			do {
				System.out.print("Would you like to continue? (y/n): ");
				line = scanner.nextLine().toLowerCase();
			}
			while (!line.matches("[ynYN]"));
			if (line.equals("n")) {
				break;
			}
			System.out.println("\nQuadratic Formula: 0=ax^2+bx^2+c");
			double a = Double.NaN;
			double b = Double.NaN;
			double c = Double.NaN;
			while (a != a) {
				try {
					System.out.print("Enter the value of a: ");
					a = Double.parseDouble(scanner.nextLine());
				}
				catch (NumberFormatException e) {
					System.out.println("Invalid input.\n");
				}
			}
			while (b != b) {
				try {
					System.out.print("Enter the value of b: ");
					b = Double.parseDouble(scanner.nextLine());
				}
				catch (NumberFormatException e) {
					System.out.println("Invalid input.\n");
				}
			}
			while (c != c) {
				try {
					System.out.print("Enter the value of c: ");
					c = Double.parseDouble(scanner.nextLine());
				}
				catch (NumberFormatException e) {
					System.out.println("Invalid input.\n");
				}
			}
			compute(a, b, c);
		}
		scanner.close();
		System.out.println("\nThank you for calculating with me! Come back soon!");
	}

	private static void compute(double a, double b, double c) {
		double discriminant = Math.pow(b, 2) - (4 * a * c);
		if (discriminant < 0) {
			System.out.println("\nThere are no real solutions\n");
			return;
		}
		LinkedList<String> solutions = new LinkedList<>();
		solutions.add(output_format.format((-b - Math.sqrt(discriminant)) / (2.0 * a)));
		//if (discriminant != 0) {
		solutions.add(output_format.format((-b + Math.sqrt(discriminant)) / (2.0 * a)));
		//}
		Collections.sort(solutions);
		System.out.println("\nRoots: " + Arrays.toString(solutions.toArray()) + "\n");
	}
}
