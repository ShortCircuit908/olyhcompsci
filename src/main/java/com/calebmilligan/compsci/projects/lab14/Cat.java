package com.calebmilligan.compsci.projects.lab14;

/**
 * @author Caleb Milligan
 *         Created on 3/3/2016
 */
public class Cat extends Mammal implements Pet {
	private String name;

	public Cat() {
		this(null, null, null);
	}

	public Cat(String name) {
		this(name, null, null);
	}

	public Cat(String name, String hair_type) {
		this(name, null, hair_type);
	}

	public Cat(String name, Boolean gender) {
		this(name, gender, null);
	}

	public Cat(String name, Boolean gender, String hair_type) {
		super(gender, hair_type);
		this.name = name;
	}

	@Override
	public String speak() {
		return "meow";
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + getPossessivePronoun() + " name is " + (name == null ? "unknown" : name);
	}

	@Override
	public String doTricks() {
		return getPronoun() + " gave you a bored look and went back to ignoring you";
	}
}
