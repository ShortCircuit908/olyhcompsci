package com.calebmilligan.compsci.projects.lab14;

/**
 * Name: Parrot.java
 * Description: Parrot
 *
 * @author Caleb Milligan
 *         Created on 3/3/2016
 */
public class Parrot extends Bird implements Pet {
	private String name;

	public Parrot() {
		this(null, null, true);
	}

	public Parrot(String name) {
		this(name, null, true);
	}

	public Parrot(String name, Boolean gender) {
		this(name, gender, true);
	}

	public Parrot(String name, Boolean gender, boolean can_fly) {
		super(gender, can_fly);
		this.name = name;
	}

	@Override
	public String speak() {
		return "craaaaaaw!";
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + getPossessivePronoun() + " name is " + (name == null ? "unknown" : name);
	}

	@Override
	public String doTricks() {
		return getPronoun() + " recited the entirety of Finnegan's Wake in one sitting";
	}
}
