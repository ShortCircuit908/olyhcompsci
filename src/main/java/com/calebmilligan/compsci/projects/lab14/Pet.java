package com.calebmilligan.compsci.projects.lab14;

/**
 * Name: Pet.java
 * Description: Pet interface
 *
 * @author Caleb Milligan
 *         Created on 3/17/2016
 */
public interface Pet {
	String doTricks();
}
