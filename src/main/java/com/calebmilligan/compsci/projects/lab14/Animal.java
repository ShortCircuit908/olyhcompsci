package com.calebmilligan.compsci.projects.lab14;

import java.util.BitSet;

/**
 * Name: Animal.java
 * Description: The superclass for all animals
 *
 * @author Caleb Milligan
 *         Created on 2/26/2016
 */
public abstract class Animal {
	public static final boolean MALE = true;
	public static final boolean FEMALE = false;
	private Boolean gender;

	public Animal() {
		this(null);
	}

	public Animal(Boolean gender) {
		this.gender = gender;
	}

	public static void main(String... args) {
		printAnimal(new Cat("Pixel", MALE, "soft and black"));
		printAnimal(new Dog("Spork", FEMALE, "rough"));
		printAnimal(new Crow("Bubbit", FEMALE));
		printAnimal(new Robin("Dick Grayson", MALE, false));
		printAnimal(new Parrot("Drumbley", MALE));
		System.out.println();
		printAnimal(new Cat());
		printAnimal(new Dog());
		printAnimal(new Crow());
		printAnimal(new Robin());
	}

	private static void printAnimal(Animal animal) {
		System.out.println(animal);
		System.out.println(animal.speak());
		if (animal instanceof Pet) {
			System.out.println(((Pet) animal).doTricks());
		}
	}

	public abstract String speak();

	@Override
	public String toString() {
		return "This " + getClass().getSimpleName() + "'s gender is " + (gender == null ? "unknown" : (gender ? "male" : "female"));
	}

	protected String getPossessivePronoun() {
		return gender == null ? "Their" : (gender ? "His" : "Her");
	}

	protected String getPronoun() {
		BitSet x;
		return gender == null ? "They" : (gender ? "He" : "She");
	}
}
