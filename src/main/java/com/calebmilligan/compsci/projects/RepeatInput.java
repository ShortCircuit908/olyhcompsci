package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: RepeatInput.java
 * Description: User input
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/14/2015
 */
@ProjectId(15)
public class RepeatInput {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("What is your phrase? ");
		String phrase = scanner.nextLine();
		int num = -1;
		while (num < 0) {
			System.out.print("How many times should I repeat it? ");
			try {
				// Avoid using scanner.nextInt() here as it can result
				// in a deadlock if given a malformed number
				num = Integer.parseInt(scanner.nextLine());
			}
			catch (NumberFormatException e) {
				System.out.print("Invalid input. ");
			}
		}
		for (int i = 0; i < num; i++) {
			System.out.println(phrase);
		}
		scanner.close();
	}
}
