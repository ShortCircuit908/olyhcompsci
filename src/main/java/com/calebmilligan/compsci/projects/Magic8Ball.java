package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Random;
import java.util.Scanner;

/**
 * Name: Magic8Ball.java
 * Description: Random numbers and arrays
 *
 * @author Caleb Milligan
 *         Created on 12/16/2015
 */
@ProjectId(29)
public class Magic8Ball {
	private static final String[] queries = {
			"Greetings, traveller! What do you wish to ask?",
			"Ask me, Magic 8-Ball, anything!",
			"What question have you for me?",
			"What troubles your mind?",
			"Ask, and you shall receive!",
			"I'm not in right now, leave a message after the beep.\n\t*beep*",
			"Dear Abby...",
			"Whadda you got for me?",
			"Oy vey, another one. Whadda you want?"
	};
	private static final String[] responses = {
			"I checked the almanac... It says \"%1$s\"",
			"Barbara! What should I tell this nut?\n\t*muffled* \"%1$s\"\nThanks, toots.",
			"%1$s",
			"*spooky noises* %1$s",
			"Eenie meenie minie... moe! \"%1$s\"",
			"Your horoscope says \"%1$s\"",
			"Don't know, don't care. NEXT!",
			"My doctor said I shouldn't answer so many questions. Cholesterol, you know.",
			"A wise man once told me, \"%1$s\"\nAND HIS NAME IS JOHN CENAAAAAA\n*horns blaring*"
	};
	private static final String[] answers = {
			"It is certain",
			"It is decidedly so",
			"Without a doubt",
			"Yes, definitely",
			"You may rely on it",
			"As I see it, yes",
			"Most likely",
			"Outlook good",
			"Yes",
			"Signs point to yes",
			"Reply hazy try again",
			"Ask again later",
			"Better not tell you now",
			"Cannot predict now",
			"Concentrate and ask again",
			"Don't count on it",
			"My reply is no",
			"My sources say no",
			"Outlook not so good",
			"Very doubtful"
	};
	private static final Random query_random = new Random(System.currentTimeMillis());

	public static void main(String... args) {
		// A good old-fashion initialization
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();
		String question;
		long seed;
		System.out.println("Greetings! Ask me anything you wish, or say \"exit\" to quit!\n");
		displayQuery();
		// Work that magic!
		while (scanner.hasNextLine()) {
			// Read input and strip non-word characters
			question = scanner.nextLine().replaceAll("\\W", "").toLowerCase();
			// Check for empty input
			if (question.isEmpty()) {
				System.out.println("Are you gonna ask me a question, or what?\n");
			}
			// Check for exit statement
			else if (question.matches("(please)?(end|exit|quit)")) {
				// How rude!
				if (!question.startsWith("please")) {
					// Add a dash of salt...
					System.out.println("Who taught you manners? Say \"please\" next time!\n");
				}
				else {
					System.out.println("Farewell!");
					break;
				}
			}
			else {
				// Construct a seed from the question for consistency
				seed = stringToSeed(question);
				random.setSeed(seed);
				// Select the response
				String response = answers[random.nextInt(answers.length)];
				System.out.println(String.format(responses[query_random.nextInt(responses.length)], response) + "\n");
			}
			displayQuery();
		}
	}

	private static void displayQuery() {
		System.out.println(queries[query_random.nextInt(queries.length)]);
		System.out.print("> ");
	}

	private static long stringToSeed(String str) {
		long seed = 0;
		try {
			// Get the string as a byte array
			byte[] bytes = str.getBytes("UTF-8");
			// If the array is not divisible by 8 (the number of bytes in a long), expand it to the nearest multiple of 8
			if (bytes.length % 8 != 0) {
				byte[] temp = new byte[(int) (Math.ceil(bytes.length / 8.0) * 8.0)];
				System.arraycopy(bytes, 0, temp, temp.length - bytes.length, bytes.length);
				bytes = temp;
			}
			// Use a DataInputStream to easily read a series of longs
			DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes));
			while (in.available() > 0) {
				seed += in.readLong();
			}
		}
		// Should never happen, but hey, better safe than sorry
		// Will return different results
		catch (Exception e) {
			seed = 0;
			// Simply convert each char to a long, multiply, and add
			for (char c : str.toCharArray()) {
				seed += ((long) c) * 1000;
			}
		}
		return seed;
	}
}
