package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: Lab3a.java
 * Description: For loops
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/2/2015
 */
@ProjectId(10)
public class Lab3a {
	public static void main(String... args) {
		for (int line = 1; line <= 10; line++) {
			for (int i = 0; i < line; i++) {
				System.out.print('*');
			}
			System.out.println();
		}
	}
}
