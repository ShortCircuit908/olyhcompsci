package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: RocketLab2.java
 * Description: Draw a rocket using methods
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/17/2015
 */
@ProjectId(4)
public class RocketLab2 {
	private static final String[][] chunks = {{
			"   /\\   ",
			"  /  \\  ",
			" /    \\ "}, {
			" \\    / ",
			"  \\  /  ",
			"   \\/   "}, {
			"+------+",
			"|      |",
			"|      |",
			"+------+"}, {
			"|UNITED|",
			"|STATES|"}, {
			"        "}
	};
	private static final int[] indices = {0, 1, 4, 1, 0, 4, 0, 2, 3, 2, 0};

	public static void main(String... args) {
		for (int index : indices) {
			printChunk(index);
		}
	}

	private static void printChunk(int index) {
		for (String line : chunks[index]) {
			System.out.println(line);
		}
	}
}
