package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: EvenPractice.java
 * Description: Print Add the first 30 even numbers
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/1/2015
 */
@ProjectId(8)
public class EvenPractice {
	public static void main(String... args) {
		int even_count = 0;
		int count = 0;
		int even_total = 0;
		while (even_count < 30) {
			count++;
			if (count % 2 == 0) {
				even_count++;
				even_total += count;
			}
		}
		System.out.println(even_total);
	}
}
