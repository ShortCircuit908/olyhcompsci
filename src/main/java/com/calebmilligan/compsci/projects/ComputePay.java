package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: ComputePay.java
 * Description: Variables
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/28/2015
 */
@ProjectId(7)
public class ComputePay {
	public static void main(String... args) {
		int hours_worked = 21;
		double salary = 8.75;
		double total_pay = salary * hours_worked;
		double taxes_due = total_pay * 0.2;
		System.out.println("My total hours worked:	" + hours_worked);
		System.out.println("My hourly salary:	" + salary);
		System.out.println("My total pay:		" + total_pay);
		System.out.println("My taxes owed:		" + taxes_due);
	}
}
