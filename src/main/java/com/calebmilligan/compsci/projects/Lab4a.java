package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: Lab4a.java
 * Description: User input
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/14/2015
 */
@ProjectId(16)
public class Lab4a {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		int num;
		while (true) {
			System.out.print("Enter an integer: ");
			try {
				/* Avoid using "scanner.nextInt()" here as it can result in an infinite loop if given
				 * a malformed number. "scanner.nextInt()" CAN be used, but it requires an
				 * accompanying "scanner.nextLine()" in the catch block to flush the input if an
				 * invalid token is given
				 */
				num = Integer.parseInt(scanner.nextLine());
				break;
			}
			catch (NumberFormatException e) {
				System.out.print("Invalid input. ");
			}
		}
		System.out.println(num + " is " + (num % 2 == 0 ? "even" : "odd") + " and "
				+ (num < 0 ? "negative" : num == 0 ? "neither negative nor positive" : "positive"));
		scanner.close();
	}
}
