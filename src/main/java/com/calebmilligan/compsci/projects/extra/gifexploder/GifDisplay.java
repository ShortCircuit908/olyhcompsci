package com.calebmilligan.compsci.projects.extra.gifexploder;

import com.calebmilligan.compsci.image.gif.GifImage;
import com.calebmilligan.compsci.image.gif.GifRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

/**
 * @author ShortCircuit908
 *         Created on 10/21/2015
 */
public class GifDisplay {
	private final JFrame frame;
	private final BufferedImage image;
	private final GifRenderer renderer;
	private JPanel content_panel;
	private JPanel canvas;
	private JButton button_start;
	private JButton button_stop;
	private JButton button_pause;

	public GifDisplay(GifImage image) {
		this.image = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		frame = new JFrame();
		frame.setContentPane(content_panel);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
				renderer.stop();
				System.exit(0);
			}
		});
		canvas.setMinimumSize(new Dimension(image.getWidth(), image.getHeight()));
		canvas.setMaximumSize(new Dimension(image.getWidth(), image.getHeight()));
		canvas.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		frame.setLocationByPlatform(true);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		renderer = new GifRenderer(image, this.image.createGraphics(), 0, 0, canvas);
		renderer.setLooping(true);
		renderer.start();
		button_stop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renderer.stop();
				button_start.setEnabled(true);
				button_pause.setEnabled(false);
				button_stop.setEnabled(false);
			}
		});
		button_start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renderer.start();
				button_start.setEnabled(false);
				button_pause.setEnabled(true);
				button_stop.setEnabled(true);
			}
		});
		button_pause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renderer.pause();
				button_start.setEnabled(true);
				button_pause.setEnabled(false);
				button_stop.setEnabled(true);
			}
		});
	}

	private void createUIComponents() {
		canvas = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				if (image != null) {
					g.drawImage(image, 0, 0, this);
				}
			}
		};
	}
}
