package com.calebmilligan.compsci.projects.extra.gifexploder;


import com.calebmilligan.compsci.image.ImageUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileView;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author ShortCircuit908
 *         Created on 10/21/2015
 */
public class ImageFileView extends FileView {

	public String getTypeDescription(File f) {
		try {
			return Files.probeContentType(f.toPath());
		}
		catch (IOException e) {
			return null;
		}
	}

	public Icon getIcon(File f) {
		Icon icon = null;
		try {
			Image image = ImageIO.read(f);
			image = ImageUtils.scaleImage(image, 16, 16, BufferedImage.SCALE_SMOOTH);
			icon = new ImageIcon(image);
		}
		catch (Exception e) {
			// Do nothing
		}
		return icon;
	}
}
