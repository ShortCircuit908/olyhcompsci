package com.calebmilligan.compsci.projects.extra;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ShortCircuit908
 *         Created on 2/3/2016
 */
public class OptionsParser {
	private Properties default_options;
	private Properties options;
	private String prefix;

	public OptionsParser() {
		this(null, null);
	}

	public OptionsParser(String prefix) {
		this(prefix, null);
	}

	public OptionsParser(Properties default_options) {
		this(null, default_options);
	}

	public OptionsParser(String prefix, Properties default_options) {
		if (prefix != null && prefix.matches(".*\\s.*")) {
			throw new IllegalArgumentException("prefix may not contain whitespace characters");
		}
		this.prefix = prefix == null ? "" : prefix;
		this.default_options = default_options == null ? new Properties() : default_options;
	}

	public void parseOptions(String options_string) {
		options = new Properties(default_options);
		Matcher matcher = Pattern.compile(prefix + "\\S+[=:\\s](\".+?\"|\\S*)").matcher(options_string);
		while (matcher.find()) {
			String[] parts = matcher.group().split("[=:\\s]", 2);
			options.setProperty(parts[0].replaceFirst(prefix, ""), parts[1].replaceAll("^\"|\"$", ""));
		}
	}

	public Properties getParsedOptions() {
		return options;
	}
}
