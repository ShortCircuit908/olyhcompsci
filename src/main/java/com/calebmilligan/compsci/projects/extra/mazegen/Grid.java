package com.calebmilligan.compsci.projects.extra.mazegen;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.List;
import java.util.Timer;

/**
 * Name: Grid.java
 * Description: Driver class and maze generator/container
 * <p>
 * Starting the program with no command-line arguments brings up
 * a dialog window containing instructions and settings. Because
 * the dialog is managed by IntelliJ IDEA's GUI designer, it may
 * not initialize properly if compiled with a different IDE. By
 * providing arguments, the program starts without a dialog, and
 * simply prints instructions to the console.
 * <p>
 * Available arguments:
 * width=[width] // Width, in cells, of the initial maze. min=2, max=100, default=5
 * height=[height] // Height, in cells, of the initial maze. min=2, max=100, default=5
 * step=[step] // Increment width and height by this after a maze is completed. min=0, max=100, default=5
 * <p>
 * Example arguments:
 * width=6 height=3 step=4
 *
 * @author Caleb Milligan
 *         Created on 6/2/2016.
 */
public class Grid extends Container {
	public static final int MIN_DIM = 2;
	public static final int MAX_DIM = 100;
	private static final Random random = new Random();
	private Cell[] cells;
	private int increment;
	private int width;
	private int height;
	private final LinkedList<Point> stack = new LinkedList<>();
	private Point cur_pos;
	private Point next_pos;
	private final Timer gen_timer = new Timer();
	private boolean gen_complete = false;
	private int complete_count = 0;
	private TimerTask gen_task;
	private static final float hue_step = 0.001f;
	private float hue = 0;
	private Point start_point;
	private Point end_point;
	private boolean left_click;

	public static void main(String... args) {
		// Set the appearance of the window
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (ClassNotFoundException | InstantiationException | UnsupportedLookAndFeelException | IllegalAccessException e) {
			e.printStackTrace();
		}
		// Create the new Grid
		Grid grid = new Grid();
		// Init the grid for a random icon
		grid.initGrid(5, 5);
		// Force-generate the grid
		while (!grid.gen_complete) {
			grid.gen(grid.next_pos);
		}
		// Generate icon images
		int[] sizes = {16, 24, 32, 48, 256};
		LinkedList<Image> icon_images = new LinkedList<>();
		for (int size : sizes) {
			grid.setSize(new Dimension(size, size));
			BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_3BYTE_BGR);
			Graphics g = image.getGraphics();
			grid.paint(g);
			icon_images.add(image);
			g.dispose();
		}
		// Create the frame
		JFrame frame = new JFrame("Caleb Milligan - Mazed and Confused");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setIconImages(icon_images);
		frame.setContentPane(grid);
		frame.setLocationByPlatform(true);
		frame.getRootPane().setPreferredSize(new Dimension(500, 500));
		// Create the menu bar
		JMenuBar menu_bar = new JMenuBar();
		// Create the file menu
		JMenu menu = new JMenu("File");
		// Create the print button
		JMenuItem menu_item = new JMenuItem("Print...");
		menu_item.setMnemonic('P');
		menu_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							grid.print();
						}
						catch (PrinterException e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		});
		menu.add(menu_item);
		// Create the save button
		menu_item = new JMenuItem("Save As...");
		menu_item.setMnemonic('S');
		menu_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							grid.save();
						}
						catch (IOException e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		});
		menu.add(menu_item);
		menu.setMnemonic('F');
		menu_bar.add(menu);
		menu = new JMenu("Maze");
		menu_item = new JMenuItem("Clear Lines");
		menu_item.setMnemonic('C');
		menu_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				grid.clearMaze();
			}
		});
		menu.add(menu_item);
		menu_item = new JMenuItem("Regenerate Maze");
		menu_item.setMnemonic('R');
		menu_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				grid.initGrid(grid.width, grid.height);
			}
		});
		menu.add(menu_item);
		menu.setMnemonic('M');
		menu_bar.add(menu);

		// Add the menu bar
		frame.setJMenuBar(menu_bar);
		frame.pack();
		frame.setResizable(true);

		if (args.length == 0) {
			// Create the settings dialog
			new StartupDialog(frame, new StartupDialog.Callback() {
				@Override
				public void ok(int width, int height, int step) {
					// Start the game
					grid.initGrid(width, height);
					grid.increment = step;
					frame.setVisible(true);
				}

				@Override
				public void cancel() {
					// User exited
					frame.dispose();
					System.exit(0);
				}
			});
		}
		else {
			int w = 5;
			int h = 5;
			int step = 5;
			for (String arg : args) {
				if (arg.indexOf('=') < 0) {
					continue;
				}
				String arg_name = arg.substring(0, arg.lastIndexOf('='));
				String arg_value = arg.substring(arg.lastIndexOf('=') + 1);
				if (arg_value.isEmpty()) {
					continue;
				}
				switch (arg_name.toLowerCase()) {
					case "width":
						w = Integer.parseInt(arg_value);
						break;
					case "height":
						h = Integer.parseInt(arg_value);
						break;
					case "step":
					case "increment":
						step = Integer.parseInt(arg_value);
						break;
					default:
						System.out.println("Invalid arguments\n[width]=5 [height]=5 [step]=5");
						frame.dispose();
						System.exit(0);
						return;
				}
			}
			for (String line : StartupDialog.INSTRUCTIONS_BLOCK.split("\n")) {
				System.out.println(line.replaceAll("<.+?>", "").trim());
			}
			grid.initGrid(w, h);
			grid.increment = step;
			frame.setVisible(true);
		}
	}

	private Point player_loc;

	public Grid() {
		setFocusable(true);
		requestFocus();
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (!gen_complete || player_loc == null) {
					return;
				}
				Cell cell = getCell(player_loc.x, player_loc.y);
				boolean moved = false;
				switch (e.getKeyCode()) {
					case KeyEvent.VK_KP_UP:
					case KeyEvent.VK_UP:
					case KeyEvent.VK_W:
						if (player_loc.y <= 0 || !cell.isConnectedTop()) {
							break;
						}
						moved = true;
						player_loc.y--;
						break;
					case KeyEvent.VK_KP_RIGHT:
					case KeyEvent.VK_RIGHT:
					case KeyEvent.VK_D:
						if (player_loc.x >= width || !cell.isConnectedRight()) {
							break;
						}
						moved = true;
						player_loc.x++;
						break;
					case KeyEvent.VK_KP_DOWN:
					case KeyEvent.VK_DOWN:
					case KeyEvent.VK_S:
						if (player_loc.y >= height || !cell.isConnectedBottom()) {
							break;
						}
						moved = true;
						player_loc.y++;
						break;
					case KeyEvent.VK_KP_LEFT:
					case KeyEvent.VK_LEFT:
					case KeyEvent.VK_A:
						if (player_loc.x <= 0 || !cell.isConnectedLeft()) {
							break;
						}
						moved = true;
						player_loc.x--;
						break;
					default:
						return;
				}
				Cell other = getCell(player_loc.x, player_loc.y);
				if (other.getColor().equals(Color.RED)) {
					initGrid(width + increment, height + increment);
					return;
				}
				if (moved) {
					if (cell.getColor().equals(Color.BLUE)) {
						cell.setColor(Color.WHITE);
					}
					if (!other.getColor().equals(Color.RED) && !other.getColor().equals(Color.GREEN)) {
						other.setColor(Color.BLUE);
					}
				}
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				left_click = e.getButton() == MouseEvent.BUTTON1;
				// Ensure the mouse is within bounds
				if (!gen_complete || e.getX() < 0 || e.getY() < 0 || e.getX() >= getWidth() || e.getY() >= getHeight()) {
					return;
				}
				// Calculate which cell the mouse is over
				double scale_x = (getWidth() / Cell.CELL_DIM / width);
				double scale_y = (getHeight() / Cell.CELL_DIM / height);
				int x_pos = (int) (e.getX() / (Cell.CELL_DIM * scale_x));
				int y_pos = (int) (e.getY() / (Cell.CELL_DIM * scale_y));
				Cell cell = getCell(x_pos, y_pos);
				// Only draw from existing blue/green cells
				if (!cell.getColor().equals(Color.BLUE) && !cell.getColor().equals(Color.GREEN)) {
					return;
				}
				start_point = new Point(x_pos, y_pos);
				// Set the single cell color
				if (!cell.getColor().equals(Color.GREEN)) {
					cell.setColor(left_click ? Color.BLUE : Color.WHITE);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				start_point = null;
				end_point = null;
			}
		});
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				// Ensure the mouse is within bounds
				if (start_point == null || !gen_complete || e.getX() < 0 || e.getY() < 0 || e.getX() >= getWidth() || e.getY() >= getHeight()) {
					return;
				}
				// Calculate which cell the mouse is over
				double scale_x = (getWidth() / Cell.CELL_DIM / width);
				double scale_y = (getHeight() / Cell.CELL_DIM / height);
				int x_pos = (int) (e.getX() / (Cell.CELL_DIM * scale_x));
				int y_pos = (int) (e.getY() / (Cell.CELL_DIM * scale_y));
				end_point = new Point(x_pos, y_pos);
				if (Math.abs(end_point.x - start_point.x) > Math.abs(end_point.y - start_point.y)) {
					boolean check_dir = start_point.x - end_point.x > 0;
					// Horizontal move
					for (int i = start_point.x; i != end_point.x + Math.signum(end_point.x - start_point.x); i += Math.signum(end_point.x - start_point.x)) {
						Cell cell = getCell(i, start_point.y);
						// Player made it to the end!
						if (cell.getColor().equals(Color.RED)) {
							initGrid(width + increment, height + increment);
							return;
						}
						// Set the cell color
						if (i != 0 || start_point.y != 0) {
							cell.setColor(left_click ? Color.BLUE : Color.WHITE);
						}
						// Stop coloring if there's a wall
						if (check_dir) {
							if (!cell.isConnectedLeft()) {
								break;
							}
						}
						else {
							if (!cell.isConnectedRight()) {
								break;
							}
						}
					}
				}
				else {
					boolean check_dir = start_point.y - end_point.y > 0;
					// Vertical move
					for (int i = start_point.y; i != end_point.y + Math.signum(end_point.y - start_point.y); i += Math.signum(end_point.y - start_point.y)) {
						Cell cell = getCell(start_point.x, i);
						// Player made it to the end!
						if (cell.getColor().equals(Color.RED)) {
							initGrid(width + increment, height + increment);
							return;
						}
						// Set the cell color
						if (i != 0 || start_point.x != 0) {
							cell.setColor(left_click ? Color.BLUE : Color.WHITE);
						}
						// Stop drawing if there's a wall
						if (check_dir) {
							if (!cell.isConnectedTop()) {
								break;
							}
						}
						else {
							if (!cell.isConnectedBottom()) {
								break;
							}
						}
					}
				}
			}
		});
		// Start the independent draw timer
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				repaint();
			}
		}, 0, 1000 / 60);
	}

	private TimerTask getNewTask() {
		if (gen_task != null) {
			gen_task.cancel();
			gen_timer.purge();
		}
		return gen_task = new TimerTask() {
			@Override
			public void run() {
				if (!gen_complete) {
					gen(next_pos);
					if(next_pos != null){
						Cell other = getCell(next_pos.x, next_pos.y);
						other.setColor(new Color(other.getColor().getRGB() ^ 0xFFFFFF));
					}
				}
			}
		};
	}

	public void initGrid(int width, int height) {
		// Create the new gen task
		getNewTask();
		// Ensure min/max dimensions
		width = Math.min(Math.max(width, MIN_DIM), MAX_DIM);
		height = Math.min(Math.max(height, MIN_DIM), MAX_DIM);
		this.hue = 0;
		this.width = width;
		this.height = height;
		this.cells = new Cell[width * height];
		this.complete_count = 0;
		this.gen_complete = false;
		this.player_loc = new Point(0, 0);
		this.stack.clear();
		this.start_point = null;
		this.end_point = null;
		// Find a random place to start
		this.next_pos = new Point(random.nextInt(width), random.nextInt(height));
		// Initialize cells
		for (int i = 0; i < cells.length; i++) {
			cells[i] = new Cell(i % width, i / width);
		}
		// Calculate a good generation speed
		long ms_per_cell = Math.max((long) (6000.0 / (width * height)), 1);
		gen_timer.schedule(gen_task, 1000, ms_per_cell);
	}

	public void print() throws PrinterException {
		if (getWidth() <= 0 || getHeight() <= 0) {
			return;
		}
		// Get a new printer job
		PrinterJob job = PrinterJob.getPrinterJob();
		// Create a new image to draw the maze
		BufferedImage image = new BufferedImage((int) (width * Cell.CELL_DIM), (int) (height * Cell.CELL_DIM), BufferedImage.TYPE_3BYTE_BGR);
		Graphics g = image.getGraphics();
		// Draw the maze on the image
		paint(g, image.getWidth(), image.getHeight());
		g.dispose();
		// Set the printed item
		job.setPrintable(new ImagePrinter(image));
		// Show the dialog, and print if confirmed
		if (job.printDialog()) {
			job.print();
		}
	}

	public void save() throws IOException {
		// Create the file chooser
		JFileChooser chooser = new JFileChooser();
		List<String> types = Arrays.asList(ImageIO.getWriterMIMETypes());
		StringBuilder builder = new StringBuilder("Image types (");
		for (String extension : ImageIO.getWriterFileSuffixes()) {
			builder.append("*.").append(extension).append(", ");
		}
		String extensions = builder.delete(builder.length() - 2, builder.length()).append(')').toString();
		// Set the file filter to accept image files
		chooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				}
				try {
					String mime_type = Files.probeContentType(f.toPath());
					return mime_type != null && types.contains(mime_type);
				}
				catch (IOException e) {
					// Do nothing
				}
				return false;
			}

			@Override
			public String getDescription() {
				return extensions;
			}
		});
		String type = "png";
		chooser.setMultiSelectionEnabled(false);
		chooser.setSelectedFile(new File(chooser.getCurrentDirectory() + "/Maze." + type));
		// Show the save dialog
		chooser.showSaveDialog(new JDialog());
		// Get the selected file
		File file = chooser.getSelectedFile();
		if (file == null) {
			return;
		}
		// Create a new file
		if (!file.exists()) {
			if (file.getName().indexOf('.') == -1) {
				file = new File(file + "." + type);
			}
			file.createNewFile();
		}
		// Overwrite existing file
		else {
			// Determine the MIME type of the existing file
			type = Files.probeContentType(file.toPath());
			if (type != null) {
				type = type.substring(type.lastIndexOf('/') + 1);
			}
		}
		if (type == null) {
			type = "png";
		}
		// Create a new image to draw the maze
		BufferedImage image = new BufferedImage((int) (width * Cell.CELL_DIM), (int) (height * Cell.CELL_DIM), BufferedImage.TYPE_3BYTE_BGR);
		Graphics g = image.getGraphics();
		// Draw the maze on the image
		paint(g, image.getWidth(), image.getHeight());
		g.dispose();
		// Write the image to the file
		FileOutputStream out = new FileOutputStream(file);
		ImageIO.write(image, type, out);
		out.flush();
		out.close();
	}

	public static class ImagePrinter implements Printable {
		private final Image image;

		public ImagePrinter(Image image) {
			this.image = image;
		}

		@Override
		public int print(Graphics graphics, PageFormat format, int page_index) throws PrinterException {
			if (page_index != 0) {
				return NO_SUCH_PAGE;
			}
			// Get orientations of the page/image
			boolean is_format_horizontal = format.getImageableWidth() > format.getImageableHeight();
			boolean is_image_horizontal = image.getWidth(null) > image.getHeight(null);
			Image print_image = image;
			// Rotate the image if necessary
			if (is_format_horizontal != is_image_horizontal) {
				// Create a new rotated image
				print_image = new BufferedImage(image.getHeight(null), image.getWidth(null), BufferedImage.TYPE_3BYTE_BGR);
				Graphics2D g = ((BufferedImage) print_image).createGraphics();
				// Calculate the rotation offset
				int off = Math.min(image.getHeight(null), image.getWidth(null));
				// Rotate the canvas 90 degress (PI/2 radians)
				g.rotate(Math.PI / 2.0, off / 2.0, off / 2.0);
				// Draw the image on the new canvas
				g.drawImage(image, 0, 0, Color.WHITE, null);
				g.dispose();
			}
			// Get the paper margin offsets
			int offset_x = (int) (format.getImageableX());
			int offset_y = (int) (format.getImageableY());
			// Get the printable area
			double print_w = format.getImageableWidth();
			double print_h = format.getImageableHeight();
			// Find the horizontal/vertical scales to optimally fill the page
			double scale_x = print_w / (double) print_image.getWidth(null);
			double scale_y = print_h / (double) print_image.getHeight(null);
			// Scale the image
			if (scale_x > scale_y) {
				print_image = print_image.getScaledInstance((int) (print_image.getWidth(null) * scale_y), (int) (image.getHeight(null) * scale_y), Image.SCALE_FAST);
			}
			else {
				print_image = print_image.getScaledInstance((int) (print_image.getWidth(null) * scale_x), (int) (image.getHeight(null) * scale_x), Image.SCALE_FAST);
			}
			// Draw the image
			graphics.drawImage(print_image, offset_x, offset_y, null);
			return PAGE_EXISTS;
		}
	}

	public Cell getCell(int x, int y) throws ArrayIndexOutOfBoundsException {
		return cells[y * width + x];
	}

	@Override
	public void paint(Graphics g) {
		paint(g, getWidth(), getHeight());
	}

	public void paint(Graphics g, int app_width, int app_height) {
		if (cells == null) {
			return;
		}
		double scale_x = (app_width / Cell.CELL_DIM / width);
		double scale_y = (app_height / Cell.CELL_DIM / height);
		for (Cell cell : cells) {
			cell.draw(g, scale_x, scale_y);
		}
		/*
		if (next_pos != null) {
			double stack_scale = (double) getHeight() / (width * height);
			int stack_h = (int) (stack.size() * stack_scale);
			int complete_h = (int) (complete_count * stack_scale);
			g.setColor(new Color(0, 255, 0, 96));
			g.fillRect(0, getHeight() - complete_h, getWidth() / 2, complete_h);
			g.setColor(new Color(255, 0, 0, 96));
			g.fillRect(getWidth() / 2, getHeight() - stack_h, getWidth() / 2, stack_h);
		}
		*/
	}

	public void clearMaze() {
		if (!gen_complete) {
			return;
		}
		next_pos = null;
		cur_pos = null;
		// Reset cell colors
		for (Cell cell : cells) {
			cell.setColor(Color.WHITE);
		}
		player_loc = new Point(0, 0);
		// Stylize the start/end cells
		getCell(0, 0).setConnectedLeft();
		getCell(0, 0).setColor(Color.GREEN);
		getCell(width - 1, height - 1).setConnectedRight();
		getCell(width - 1, height - 1).setColor(Color.RED);
	}

	private void gen(Point p) {
		// Get the probe cell
		Cell current = getCell(p.x, p.y);
		current.setColor(new Color(Color.HSBtoRGB(hue, 1.0f, 1.0f)));
		//current.setColor(Color.WHITE);
		cur_pos = p;
		// Color the probe cell color

		// Get possible directions
		int[] possible = getPossibleDirections(p);
		// There are no possible directions!
		if (possible.length == 0) {
			// The stack is empty, so we must be done with the maze, and back at the origin
			if (stack.isEmpty()) {
				gen_task.cancel();
				gen_task = null;
				gen_timer.purge();
				gen_complete = true;
				clearMaze();
				return;
			}
			hue -= hue_step;
			// Remove one cell from the stack to backtrace
			next_pos = stack.removeLast();
			return;
		}
		// Choose a random direction
		int dir = possible[random.nextInt(possible.length)];
		Cell other = null;
		complete_count++;
		// Add the current position to the stack
		stack.addLast(p);
		// Connect cells
		switch (dir) {
			case 0:
				other = getCell(p.x - 1, p.y);
				other.setConnectedRight();
				current.setConnectedLeft();
				next_pos = new Point(p.x - 1, p.y);
				break;
			case 1:
				other = getCell(p.x + 1, p.y);
				other.setConnectedLeft();
				current.setConnectedRight();
				next_pos = new Point(p.x + 1, p.y);
				break;
			case 2:
				other = getCell(p.x, p.y - 1);
				other.setConnectedBottom();
				current.setConnectedTop();
				next_pos = new Point(p.x, p.y - 1);
				break;
			case 3:
				other = getCell(p.x, p.y + 1);
				other.setConnectedTop();
				current.setConnectedBottom();
				next_pos = new Point(p.x, p.y + 1);
				break;
		}
		hue += hue_step;
	}

	public int[] getPossibleDirections(Point p) {
		int[] directions = new int[4];
		int dir_count = 0;
		// left
		if (p.x >= 1 && !getCell(p.x - 1, p.y).hasConnection()) {
			directions[dir_count++] = 0;
		}
		// right
		if (p.x < width - 1 && !getCell(p.x + 1, p.y).hasConnection()) {
			directions[dir_count++] = 1;
		}
		// up
		if (p.y >= 1 && !getCell(p.x, p.y - 1).hasConnection()) {
			directions[dir_count++] = 2;
		}
		// down
		if (p.y < height - 1 && !getCell(p.x, p.y + 1).hasConnection()) {
			directions[dir_count++] = 3;
		}
		int[] fin = new int[dir_count];
		System.arraycopy(directions, 0, fin, 0, dir_count);
		return fin;
	}
}
