package com.calebmilligan.compsci.projects.extra;

/**
 * @author ShortCircuit908
 *         Created on 3/25/2016
 */
public class ByteArrayShifter {
	byte[] buffer;

	public ByteArrayShifter(byte[] buffer) {
		this.buffer = buffer;
	}

	public static void main(String... args) {
		byte[] buffer = {
				(byte) 0xFF, 0x00,
				(byte) 0xFF, 0x00,
				(byte) 0xFF, 0x00,
				(byte) 0xFF, 0x00
		};
		ByteArrayShifter shifter = new ByteArrayShifter(buffer);
		printByteArray(shifter.shiftInRight(new byte[]{(byte) 0xFF}, 3));
		printByteArray(buffer);
	}

	public static byte[] shiftInLeft(byte[] buffer, byte[] bits, int bit_len) {
		byte[] overflow = shiftLeft(buffer, bit_len);
		shiftLeft(bits, 8 * bits.length - bit_len);
		for (int i = 0; i < bits.length; i++) {
			buffer[(buffer.length - bits.length) + i] |= bits[i];
		}
		return overflow;
	}

	public static byte[] shiftInRight(byte[] buffer, byte[] bits, int bit_len) {
		byte[] overflow = shiftRight(buffer, bit_len);
		shiftRight(bits, 8 * bits.length - bit_len);
		for (int i = 0; i < bits.length; i++) {
			buffer[i] |= bits[i];
		}
		return overflow;
	}

	private static byte[] shiftLeft(byte[] buffer, int bits) {
		int index_shift = bits / 8;
		int bit_shift = bits % 8;
		byte[] overflow = new byte[index_shift + (bit_shift != 0 ? 1 : 0)];
		System.arraycopy(buffer, 0, overflow, overflow.length - index_shift, index_shift);
		System.arraycopy(buffer, index_shift, buffer, 0, buffer.length - index_shift);
		System.arraycopy(new byte[index_shift], 0, buffer, buffer.length - index_shift, index_shift);
		byte over = 0;
		for (int i = buffer.length - 1; i >= 0; i--) {
			int b = ((int) buffer[i]) & 0xFF;
			buffer[i] = (byte) (((b >>> bit_shift << bit_shift) & 0xFF) | over);
			over = (byte) (b << bit_shift >>> 8 & 0xFF);
		}
		if (overflow.length > 0 && over != 0) {
			shiftLeft(overflow, bit_shift);
			overflow[overflow.length - 1] |= over;
		}
		return overflow;
	}

	private static byte[] shiftRight(byte[] buffer, int bits) {
		int index_shift = bits / 8;
		int bit_shift = bits % 8;
		byte[] overflow = new byte[index_shift + (bit_shift != 0 ? 1 : 0)];
		System.arraycopy(buffer, buffer.length - index_shift, overflow, 0, index_shift);
		System.arraycopy(buffer, 0, buffer, index_shift, buffer.length - index_shift);
		System.arraycopy(new byte[index_shift], 0, buffer, 0, index_shift);
		byte over = 0;
		for (int i = 0; i < buffer.length; i++) {
			int b = ((int) buffer[i]) & 0xFF;
			buffer[i] = (byte) (((b >>> bit_shift) & 0xFF) | over);
			over = (byte) (b >>> (8 - bit_shift) << (8 - bit_shift) & 0xFF);
		}
		if (overflow.length > 0 && over != 0) {
			shiftRight(overflow, bit_shift);
			overflow[0] |= over;
		}
		return overflow;
	}

	public static void printByte(byte b) {
		System.out.print(String.format("%8s",
				Integer.toBinaryString((b + 256) % 256)).replace(' ', '0'));
	}

	public static void printByteArray(byte[] array) {
		for (byte b : array) {
			printByte(b);
			System.out.print(",");
		}
		System.out.println();
	}

	public byte[] shiftInLeft(byte[] bits, int bit_len) {
		return shiftInLeft(buffer, bits, bit_len);
	}

	public byte[] shiftInRight(byte[] bits, int bit_len) {
		return shiftInRight(buffer, bits, bit_len);
	}

	public byte[] shiftRight(int bits) {
		return shiftRight(buffer, bits);
	}

	public byte[] shiftLeft(int bits) {
		return shiftLeft(buffer, bits);
	}
}
