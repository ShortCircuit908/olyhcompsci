package com.calebmilligan.compsci.projects.extra;

/**
 * Name: HiddenWord.java
 * Description: Guesing game assignment
 *
 * @author Caleb Milligan, period 4
 *         Created on 12/18/2015
 */
public class HiddenWord {
	private final String word;

	public HiddenWord(String word) {
		this.word = word.toUpperCase();
	}

	public static void main(String... args) {
		HiddenWord word = new HiddenWord("harps");
		System.out.println(word.getHint("aaaaa"));
		System.out.println(word.getHint("hello"));
		System.out.println(word.getHint("heart"));
		System.out.println(word.getHint("harms"));
		System.out.println(word.getHint("harps"));
	}

	public String getHint(String guess) {
		guess = guess.toUpperCase();
		StringBuilder builder = new StringBuilder(guess);
		for (int i = 0; i < guess.length(); i++) {
			char ch = guess.charAt(i);
			if (ch == word.charAt(i)) {
				continue;
			}
			boolean found = false;
			for (int j = 0; j < word.length(); j++) {
				if (word.charAt(j) == ch) {
					builder.setCharAt(i, '+');
					found = true;
				}
			}
			if (!found) {
				builder.setCharAt(i, '*');
			}
		}
		return builder.toString();
	}
}
