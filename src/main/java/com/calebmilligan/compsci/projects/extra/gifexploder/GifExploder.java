package com.calebmilligan.compsci.projects.extra.gifexploder;

import com.calebmilligan.compsci.ProjectId;
import com.calebmilligan.compsci.image.gif.GifFrame;
import com.calebmilligan.compsci.image.gif.GifImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author Caleb Milligan, Period 4
 *         Created on 10/21/2015
 */
@ProjectId(-4)
public class GifExploder {
	public static void main(String... args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (UnsupportedLookAndFeelException | ReflectiveOperationException e) {
			e.printStackTrace();
		}
		JFileChooser file_chooser = new JFileChooser(new File(System.getProperty("user.home")));
		file_chooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				}
				try {
					return "image/gif".equalsIgnoreCase(Files.probeContentType(f.toPath()));
				}
				catch (IOException e) {
					return false;
				}
			}

			@Override
			public String getDescription() {
				return "GIF images (.gif)";
			}
		});
		file_chooser.setFileView(new ImageFileView());
		file_chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		file_chooser.showOpenDialog(null);
		File file = file_chooser.getSelectedFile();
		if (file == null) {
			return;
		}
		try {
			GifImage image = GifImage.explodeGif(new FileInputStream(file));
			new GifDisplay(image);
			BufferedImage new_image = new BufferedImage(image.getWidth() * 2, image.getHeight() * image.getFrames().size(), BufferedImage.TYPE_INT_ARGB);
			File dir = new File(file_chooser.getCurrentDirectory() + "/output");
			dir.mkdirs();
			for (int i = 0; i < image.getFrames().size(); i++) {
				GifFrame frame = image.getFrames().get(i);
				new_image.getGraphics().drawImage(frame.getImage(), frame.getOffsetX(), i * image.getHeight() + frame.getOffsetY(), null);
				new_image.getGraphics().drawImage(frame.getCompositeImage(), image.getWidth(), i * image.getHeight(), null);
				ImageIO.write(frame.getCompositeImage(), "png", new File(dir + "/" + i + ".png"));
			}
			ImageIO.write(new_image, "png", new File(dir + "/map.png"));
			Desktop.getDesktop().browse(dir.toURI());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
