package com.calebmilligan.compsci.projects.extra;

import java.awt.*;

/**
 * Name: Sierpinski.java
 * Description: Recursive graphics
 *
 * @author Caleb Milligan
 *         Created on 5/2/2016.
 */
public class Sierpinski {

	public static void drawSierpinski(final Graphics g, int width, int height, final int level) {
		drawSierpinski(level, g, 0, height, width, height, width / 2, 0);
	}

	public static void drawSierpinski(final int level, final Graphics g, final int p1x, final int p1y, final int p2x, final int p2y, final int p3x, final int p3y) {
		if (level <= 1) {
			g.fillPolygon(new int[]{p1x, p2x, p3x}, new int[]{p1y, p2y, p3y}, 3);
			return;
		}
		final int
				p4x = (p1x + p2x) / 2, p4y = (p1y + p2y) / 2,
				p5x = (p2x + p3x) / 2, p5y = (p2y + p3y) / 2,
				p6x = (p3x + p1x) / 2, p6y = (p3y + p1y) / 2;
		drawSierpinski(level - 1, g, p1x, p1y, p4x, p4y, p6x, p6y);
		drawSierpinski(level - 1, g, p4x, p4y, p2x, p2y, p5x, p5y);
		drawSierpinski(level - 1, g, p6x, p6y, p5x, p5y, p3x, p3y);
	}
}
