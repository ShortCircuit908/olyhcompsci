package com.calebmilligan.compsci.projects.extra.mazegen;

import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

/**
 * Name: StartupDialog.java
 * Description: Initial instructions/settings dialog
 * <p>
 * Note: This class is bound to StartupDialog.form and managed
 * by IntelliJ IDEA's GUI designer. As such, it may not
 * function if compiled using a different IDE. See Grid.java
 * for using command-line arguments in place of this dialog
 *
 * @author Caleb Milligan
 *         Created on 6/2/2016.
 */
public class StartupDialog extends JDialog {
	public static final String INSTRUCTIONS_BLOCK = "Your objective is simple: <span style=\"font-weight: bold;\">Solve the maze!</span><br>\n" +
			"This is accomplished by drawing a line between the red and green cells<br>\n" +
			"You may begin drawing a new line by <span style=\"font-weight: bold;\">left-clicking</span> on a blue or green cell<br>\n" +
			"Then, <span style=\"font-weight: bold;\">drag the mouse vertically or horizontally</span> to draw a blue line<br>\n" +
			"You may fill in as many cells as you'd like, but you <span style=\"font-weight: bold;\">may not cross a solid black border</span><br>\n" +
			"You can branch off an existing line by clicking it at a junction<br>\n" +
			"You can clear lines by <span style=\"font-weight: bold;\">right-clicking</span>, then <span style=\"font-weight: bold;\">dragging the mouse</span> as normal<br>\n" +
			"<br>\n" +
			"Alternatively, you may use <span style=\"font-weight: bold;\">W/A/S/D</span> or the <span style=\"font-weight: bold;\">arrow keys</span> to navigate the maze";
	private JPanel contentPane;
	private JButton button_ok;
	private JSpinner spinner_width;
	private JSpinner spinner_height;
	private JSpinner spinner_increment;
	private JTextPane text_pane_instructions;
	private SpinnerNumberModel width_model = new SpinnerNumberModel(5, Grid.MIN_DIM, Grid.MAX_DIM, 1);
	private SpinnerNumberModel height_model = new SpinnerNumberModel(5, Grid.MIN_DIM, Grid.MAX_DIM, 1);
	private SpinnerNumberModel increment_model = new SpinnerNumberModel(5, 0, Grid.MAX_DIM / 2, 1);
	private final Callback callback;

	public StartupDialog(JFrame parent, Callback callback) {
		super(parent, "Caleb Milligan - Mazed and Confused", true);
		this.callback = callback;
		setLocationByPlatform(true);
		setContentPane(contentPane);
		setModal(true);
		getRootPane().setDefaultButton(button_ok);

		button_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onOK();
			}
		});

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				onCancel();
			}
		});

		contentPane.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onCancel();
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

		spinner_width.setModel(width_model);
		spinner_height.setModel(height_model);
		spinner_increment.setModel(increment_model);

		String instructions = "<!DOCTYPE html>\n" +
				"<html>\n" +
				"<head>\n" +
				"</head>\n" +
				"<body style=\"text-align: center; font: 16pt 'Helvetica Neue', Helvetica, Arial, sans-serif\">\n" +
				"<h1>Mazed and Confused</h1>\n" +
				"<hr>\n" +
				"<p style=\"display: block;\">\n" +
				INSTRUCTIONS_BLOCK +
				"</p>\n" +
				"</body>\n" +
				"</html>";
		text_pane_instructions.setText(instructions);
		BufferedImage b = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
		Graphics g = b.getGraphics();
		HTMLDocument document = (HTMLDocument) text_pane_instructions.getStyledDocument();
		Font font = document.getFont(document.getDefaultRootElement().getElement(1).getAttributes());
		g.setFont(font.deriveFont(Font.BOLD));
		int max_width = 0;
		for (String line : instructions.split("\n")) {
			line = line.replaceAll("<.+?>", "").trim();
			int width = g.getFontMetrics().stringWidth(line);
			if (width > max_width) {
				max_width = width;
			}
		}
		int h = g.getFontMetrics().getHeight() * instructions.split("\n").length;
		g.dispose();
		setMinimumSize(new Dimension(max_width, h + 100));
		setPreferredSize(getMinimumSize());
		pack();
		setVisible(true);
	}

	private void onOK() {
		int width = (Integer) width_model.getValue();
		int height = (Integer) height_model.getValue();
		int step = (Integer) increment_model.getValue();
		dispose();
		callback.ok(width, height, step);
	}

	private void onCancel() {
		dispose();
		callback.cancel();
	}

	public static abstract class Callback {
		public abstract void ok(int width, int height, int step);

		public abstract void cancel();
	}
}
