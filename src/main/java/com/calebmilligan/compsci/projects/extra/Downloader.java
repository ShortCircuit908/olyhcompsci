package com.calebmilligan.compsci.projects.extra;

import com.calebmilligan.compsci.ProjectId;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * @author ShortCircuit908
 *         Created on 10/5/2015
 */
@ProjectId(-2)
public class Downloader {

	public static void main(String... args) {
		if (args.length < 1) {
			System.out.println("Too few arguments");
			return;
		}
		File dir = new File("downloads\\");
		if (args.length >= 2) {
			if (!args[1].endsWith("/") && !args[1].endsWith("\\")) {
				args[1] += "\\";
			}
			dir = new File(args[1]);
		}
		dir.mkdirs();
		try {
			URL url = new URL(args[0]);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			String safe_string = StringEscapeUtils.escapeHtml4(args[0].split("[\\\\/]")[args[0].split("[\\\\/]").length - 1]);
			File file = new File(dir + "\\" + safe_string);
			Files.copy(connection.getInputStream(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
