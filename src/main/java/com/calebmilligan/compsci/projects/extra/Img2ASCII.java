package com.calebmilligan.compsci.projects.extra;

import com.calebmilligan.compsci.ProjectId;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 3/14/2016
 */
@ProjectId(-13)
public class Img2ASCII {
	// Map extended ASCII code points to their corresponding Unicode character
	public static final char[] ASCII_EXTENDED = {
			0x0000, 0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0009, 0x000A, 0x000B, 0x000C, 0x000D, 0x000E, 0x000F,
			0x0010, 0x0011, 0x0012, 0x0013, 0x0014, 0x0015, 0x0016, 0x0017, 0x0018, 0x0019, 0x001A, 0x001B, 0x001C, 0x001D, 0x001E, 0x001F,
			0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027, 0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
			0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037, 0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
			0x0040, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047, 0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
			0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057, 0x0058, 0x0059, 0x005A, 0x005B, 0x005C, 0x005D, 0x005E, 0x005F,
			0x0060, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067, 0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
			0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077, 0x0078, 0x0079, 0x007A, 0x007B, 0x007C, 0x007D, 0x007E, 0x007F,
			0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7, 0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
			0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9, 0x00FF, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x00D7, 0x0192,
			0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA, 0x00BF, 0x00AE, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
			0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x00C1, 0x00C2, 0x00C0, 0x00A9, 0x2563, 0x2551, 0x2557, 0x255D, 0x00A2, 0x00A5, 0x2510,
			0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x00E3, 0x00C3, 0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
			0x00F0, 0x00D0, 0x00CA, 0x00CB, 0x00C8, 0x0131, 0x00CD, 0x00CE, 0x00CF, 0x2518, 0x250C, 0x2588, 0x2584, 0x00A6, 0x00CC, 0x2580,
			0x00D3, 0x00DF, 0x00D4, 0x00D2, 0x00F5, 0x00D5, 0x00B5, 0x00FE, 0x00DE, 0x00DA, 0x00DB, 0x00D9, 0x00FD, 0x00DD, 0x00AF, 0x00B4,
			0x2261, 0x00B1, 0x2017, 0x00BE, 0x00B6, 0x00A7, 0x00F7, 0x00B8, 0x00B0, 0x00A8, 0x00B7, 0x00B9, 0x00B3, 0x00B2, 0x25A0, 0x0008
	};

	public static void main(String... args) throws IOException {
		//BufferedImage raw_charsheet = ImageIO.read(Img2ASCII.class.getResourceAsStream("/img/default_ascii_charsheet_nopadding.png"));
		BufferedImage raw_charsheet = ImageIO.read(Img2ASCII.class.getResourceAsStream("/img/extended_ascii_charsheet.png"));
		FontNormalizer normalizer = new Charsheet(raw_charsheet);

		//FontNormalizer normalizer = new FontNormalizer(new Font("Lucida Console", Font.PLAIN, 9));

		BufferedImage base = ImageIO.read(Img2ASCII.class.getResourceAsStream("/img/Img2ASCII_test.jpg"));

		//String str = imageToAsciiString(base, normalizer, false);
		//System.out.println(str);

		BufferedImage asciified = imageToAsciiImage(base, normalizer, false);
		File output_file = new File("Img2ASCII_Out.png");
		ImageIO.write(asciified, "png", output_file);
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(output_file.toURI());
				return;
			}
			catch (IOException e) {
				// Do nothing
			}
		}
		System.out.println(output_file.getAbsolutePath());
	}

	private static char asciiCodePointToUnicode(int code_point) {
		return ASCII_EXTENDED[code_point];
	}

	public static String imageToAsciiString(BufferedImage base, FontNormalizer font_normalizer, boolean extended) {
		int new_width = (int) Math.round((double) base.getWidth() / (double) font_normalizer.max_dimension.width) * font_normalizer.max_dimension.width;
		int new_height = (int) Math.round((double) base.getHeight() / (double) font_normalizer.max_dimension.height) * font_normalizer.max_dimension.height;
		Image scaled = base.getScaledInstance(new_width, new_height, BufferedImage.SCALE_SMOOTH);
		BufferedImage buff_scaled = new BufferedImage(new_width, new_height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = buff_scaled.getGraphics();
		g.setColor(new Color(0, true));
		g.fillRect(0, 0, buff_scaled.getWidth(), buff_scaled.getHeight());
		g.drawImage(scaled, 0, 0, null);
		g.dispose();
		base = buff_scaled;
		int line_height = font_normalizer.max_dimension.height;
		int line_width = font_normalizer.max_dimension.width;
		StringBuilder builder = new StringBuilder();
		for (int y = 0; y < base.getHeight() / line_height; y++) {
			for (int x = 0; x < base.getWidth() / line_width; x++) {
				int closest = getClosestChar(base, font_normalizer, x, y, extended);
				builder.append(asciiCodePointToUnicode(closest));
			}
			builder.append('\n');
		}
		return builder.toString();
	}

	public static BufferedImage imageToAsciiImage(BufferedImage base, FontNormalizer font_normalizer, boolean extended) {
		int new_width = (int) Math.round((double) base.getWidth() / (double) font_normalizer.max_dimension.width) * font_normalizer.max_dimension.width;
		int new_height = (int) Math.round((double) base.getHeight() / (double) font_normalizer.max_dimension.height) * font_normalizer.max_dimension.height;
		Image scaled = base.getScaledInstance(new_width, new_height, BufferedImage.SCALE_SMOOTH);
		BufferedImage buff_scaled = new BufferedImage(new_width, new_height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = buff_scaled.getGraphics();
		g.setColor(new Color(0, true));
		g.fillRect(0, 0, buff_scaled.getWidth(), buff_scaled.getHeight());
		g.drawImage(scaled, 0, 0, null);
		g.dispose();
		base = buff_scaled;
		BufferedImage new_image = new BufferedImage(new_width, new_height, BufferedImage.TYPE_4BYTE_ABGR);
		g = new_image.getGraphics();
		int line_height = font_normalizer.max_dimension.height;
		int line_width = font_normalizer.max_dimension.width;
		for (int y = 0; y < base.getHeight() / line_height; y++) {
			for (int x = 0; x < base.getWidth() / line_width; x++) {
				int closest_char = getClosestChar(base, font_normalizer, x, y, extended);
				g.drawImage(font_normalizer.getCharImage(closest_char), x * font_normalizer.max_dimension.width, y * font_normalizer.max_dimension.height, null);
			}
		}
		g.dispose();
		return new_image;
	}

	private static int getClosestChar(BufferedImage image, FontNormalizer charsheet, int start_x, int start_y, boolean extended) {
		BufferedImage sub = image.getSubimage(start_x * charsheet.max_dimension.width, start_y * charsheet.max_dimension.height, charsheet.max_dimension.width, charsheet.max_dimension.height);
		int average_color = getAverageColor(sub);
		int closest_char = 32;
		int tmp_average;
		double lowest_distance = Double.MAX_VALUE;
		for (int ch = 32; ch < (extended ? 256 : 128); ch++) {
			tmp_average = charsheet.getAverageColor(ch);
			double distance = getDistance(average_color, tmp_average);
			if (distance < lowest_distance) {
				lowest_distance = distance;
				closest_char = ch;
			}
		}
		return closest_char;
	}

	private static int getAverageColor(BufferedImage image) {
		double red = 0;
		double green = 0;
		double blue = 0;
		double alpha = 0;
		int size = image.getWidth() * image.getHeight();
		int[] rgb = new int[size];
		rgb = image.getRGB(0, 0, image.getWidth(), image.getHeight(), rgb, 0, image.getWidth());
		for (int raw_color : rgb) {
			int tmp_alpha = ((raw_color >> 24) & 0xFF);
			alpha += tmp_alpha;
			if (tmp_alpha > 0) {
				red += ((raw_color >> 16) & 0xFF);
				green += ((raw_color >> 8) & 0xFF);
				blue += (raw_color & 0xFF);
			}
			else {
				red += 255;
				green += 255;
				blue += 255;
			}
		}
		alpha = Math.min(Math.round(alpha / size), 255);
		red = Math.min(Math.round(red / size), 255);
		green = Math.min(Math.round(green / size), 255);
		blue = Math.min(Math.round(blue / size), 255);
		return (((int) alpha & 0xFF) << 24) |
				(((int) red & 0xFF) << 16) |
				(((int) green & 0xFF) << 8) |
				(((int) blue & 0xFF));
	}

	private static double getDistance(int color_1, int color_2) {
		int
				blue_1 = (color_1) & 0xFF,
				blue_2 = (color_2) & 0xFF,
				green_1 = (color_1 >> 8) & 0xFF,
				green_2 = (color_2 >> 8) & 0xFF,
				red_1 = (color_1 >> 16) & 0xFF,
				red_2 = (color_2 >> 16) & 0xFF,
				alpha_1 = (color_1 >> 24) & 0xFF,
				alpha_2 = (color_2 >> 24) & 0xFF;
		double
				raw_dist = Math.sqrt(
				Math.pow(red_2 - red_1, 2)
						+ Math.pow(green_2 - green_1, 2)
						+ Math.pow(blue_2 - blue_1, 2)),
				alpha_dist = alpha_2 - alpha_1;
		if (alpha_dist <= 5 && Math.max(alpha_2, alpha_1) <= 10) {
			return 0;
		}
		return raw_dist;
	}

	public static class Charsheet extends FontNormalizer {
		private final BufferedImage sheet;
		private int char_width;
		private int char_height;

		public Charsheet(BufferedImage sheet) {
			super(null);
			this.sheet = sheet;
			char_width = sheet.getWidth() / 16;
			char_height = sheet.getHeight() / 16;
			super.max_dimension = new Dimension(char_width, char_height);
		}

		@Override
		public void normalize() {
		}

		@Override
		public BufferedImage getCharImage(int ch) {
			if (ch >= chars.length) {
				return null;
			}
			if (chars[ch] != null) {
				return chars[ch];
			}
			int x = ch % 16;
			int y = ch / 16;
			return (chars[ch] = sheet.getSubimage(x * char_width, y * char_height, char_width, char_height));
		}
	}

	public static class FontNormalizer {
		protected BufferedImage[] chars = new BufferedImage[256];
		protected Integer[] average_colors = new Integer[chars.length];
		private Font font;
		private BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR);
		private Graphics g = image.getGraphics();
		private FontMetrics metrics;
		private Dimension max_dimension;

		public FontNormalizer(Font font) {
			this.font = font;
			normalize();
		}

		protected void normalize() {
			metrics = g.getFontMetrics(font);
			int max_width = 0;
			int max_height = metrics.getHeight();
			for (int ch = 32; ch < 256; ch++) {
				if (metrics.charWidth(asciiCodePointToUnicode(ch)) > max_width) {
					max_width = metrics.charWidth(ch);
				}
			}
			max_dimension = new Dimension(max_width, max_height);
		}

		public BufferedImage getCharImage(int ch) {
			if (ch >= chars.length) {
				return null;
			}
			if (chars[ch] != null) {
				return chars[ch];
			}
			Dimension dim = max_dimension;
			BufferedImage img = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics img_g = img.getGraphics();
			img_g.setColor(new Color(255, 255, 255, 1));
			img_g.fillRect(0, 0, img.getWidth(), img.getHeight());
			img_g.setColor(Color.BLACK);
			img_g.setFont(font);
			img_g.drawString(asciiCodePointToUnicode(ch) + "", 0, metrics.getLeading() + metrics.getAscent());
			img_g.dispose();
			BufferedImage final_img = new BufferedImage(max_dimension.width, max_dimension.height, BufferedImage.TYPE_4BYTE_ABGR);
			int h = (int) Math.round((final_img.getWidth() - img.getWidth()) / 2.0);
			int v = (int) Math.round((final_img.getHeight() - img.getHeight()) / 2.0);
			Graphics final_g = final_img.getGraphics();
			final_g.setColor(new Color(255, 255, 255, 1));
			final_g.fillRect(0, 0, img.getWidth(), img.getHeight());
			final_g.drawImage(img, h, v, null);
			final_g.dispose();
			return (chars[ch] = final_img);
		}

		public int getAverageColor(int ch) {
			if (ch < 0 || ch >= average_colors.length) {
				return 0;
			}
			if (average_colors[ch] != null) {
				return average_colors[ch];
			}
			BufferedImage image = getCharImage(ch);
			if (image == null) {
				return 0;
			}
			return (average_colors[ch] = Img2ASCII.getAverageColor(image));
		}
	}
}
