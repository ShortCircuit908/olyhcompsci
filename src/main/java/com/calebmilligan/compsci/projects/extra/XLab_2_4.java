package com.calebmilligan.compsci.projects.extra;

import com.calebmilligan.compsci.ProjectId;

import java.util.LinkedList;

/**
 * @author Caleb Milligan, Period 4
 *         Created on 10/9/2015
 */
@ProjectId(-3)
public class XLab_2_4 {
	private static final int total_steps = 7;
	private static final String[][] components = {
			{
					"     ",
					"     ",
					"     "
			},
			{
					"  o  ",
					" /|\\ ",
					" / \\ "
			},
			{
					"******",
					"*    ",
					"*    "
			},
			{
					"*****"
			}
	};

	public static void main(String... args) {
		LinkedList<Integer> component_indices = new LinkedList<>();
		for (int i = 0; i <= total_steps; i++) {
			if (i < total_steps - 1) {
				component_indices.add(0);
			}
			else if (i < total_steps) {
				component_indices.add(1);
			}
			else {
				component_indices.add(2);
			}
		}
		for (int i = 0; i <= total_steps; i++) {
			if (i < total_steps) {
				printComponent(component_indices.toArray(new Integer[0]));
				component_indices.pop();
				component_indices.add(0);
			}
			else {
				for (int j = 0; j <= total_steps; j++) {
					System.out.print(components[3][0]);
				}
				System.out.print("**");
			}
		}
	}


	private static void printComponent(Integer... component_indices) {
		for (int y = 0; y < 3; y++) {
			for (int component_index : component_indices) {
				System.out.print(components[component_index][y]);
			}
			if (y > 0) {
				System.out.print(' ');
			}
			System.out.println('*');
		}
	}
}
