package com.calebmilligan.compsci.projects.extra;

import com.calebmilligan.compsci.ProjectId;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Name: Lab6a_invalid.java
 * Description: Methods (calculator)
 *
 * @author ShortCircuit908
 *         Created on 11/10/2015
 */
@ProjectId(-24)
public class Lab6a_invalid {
	private static final DecimalFormat output_format = new DecimalFormat(",##0.###################");
	private static final String[] operators = {"\\^", "\\?", "\\*", "/", "%", "\\+", "\\-"};
	private static final HashMap<String, Double> vars = new HashMap<>();

	static {
		vars.put("pi", Math.PI);
		vars.put("PI", Math.PI);
		vars.put("Pi", Math.PI);
		vars.put("pI", Math.PI);
		vars.put("e", Math.E);
	}

	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		/*
		Globals globals = JmePlatform.standardGlobals();
		while (scanner.hasNextLine()) {
			String input = "return 0.0 + (" + scanner.nextLine() + ")";
			try {
				LuaValue chunk = globals.load(input);
				LuaValue result = chunk.call();
				System.out.println(result);
			}
			catch (LuaError e) {
				System.out.println("Invalid input");
			}
		}
		*/
		while (scanner.hasNextLine()) {
			// Get input and trim whitespace
			String raw_input = scanner.nextLine().replaceAll("\\s", "");
			// Check if user wants to quit
			if (raw_input.toLowerCase().matches("exit|stop|quit|end")) {
				return;
			}
			// Parse input
			try {
				System.out.println("= " + output_format.format(parse(raw_input)));
			}
			catch (IllegalArgumentException e) {
				System.out.println("Invalid input: " + e.getMessage());
			}
			catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		scanner.close();
	}

	/**
	 * Recursively parse input to perform mathematical operations
	 *
	 * @param input the string to parse
	 * @return the result of the mathematical operations
	 * @throws NumberFormatException
	 */
	private static double parse(String input) throws IllegalArgumentException {
		if (input.trim().isEmpty()) {
			return 0.0;
		}

		// Set up assignment operator
		String var = null;
		String[] bits = input.split("=");
		if (bits.length > 2) {
			throw new IllegalArgumentException("Multiple assignment operators");
		}
		if (bits.length == 2) {
			var = bits[0];
			if (!var.matches("[a-zA-Z_]+") || var.toLowerCase().matches("pi|e")) {
				throw new IllegalArgumentException("Invalid variable identifier");
			}
			input = bits[1];
		}

		// Convert natural logarithm to custom operator
		// natural log of 10 == ln(10) == loge(10) == e?10
		input = input.replaceAll("ln\\(?([\\d\\.]*)\\)?", "log\\(e\\)\\($1\\)");

		// Convert standard logarithm to custom operator
		// log base 5 of 10 == log5(10) == 5?10
		input = input.replaceAll("log(\\-?[\\d\\.]*)\\((.*)\\)", "\\($1?$2\\)");

		// Replace stored values
		for (String key : vars.keySet()) {
			if (key == null) {
				continue;
			}
			input = input.replaceAll("\\(?(?<![a-zA-DF-Z_])\\)?" + key + "(?![a-zA-DF-Z_])", "(" + vars.get(key) + ")");
		}

		// Check for undefined variables
		if (input.matches(".*[a-zA-DF-Z_].*")) {
			Matcher matcher = Pattern.compile("[a-zA-DF-Z_]+").matcher(input);
			if (matcher.find()) {
				throw new IllegalArgumentException("Undefined variable: " + matcher.group());
			}
		}

		// Handle subtracting a negative
		// 3-(-4) == 3+4
		// 3--4 == 3+4
		input = input.replaceAll("\\-\\-([\\d\\.]+)", "+$1");

		double result = Double.NaN;

		// Extract and parse parentheticals
		if (input.indexOf('(') != -1 || input.indexOf(')') != -1) {
			int start = 0;
			int end = 0;
			int num_paren = 0;
			boolean in_paren = false;
			for (int i = 0; i < input.length(); i++) {
				char ch = input.charAt(i);
				if (ch == '(') {
					if (num_paren == 0) {
						in_paren = true;
						start = i;
					}
					num_paren++;
				}
				if (ch == ')') {
					// Check for mismatched parentheses
					if (!in_paren) {
						throw new IllegalArgumentException("missing '('");
					}
					num_paren--;
					if (num_paren == 0) {
						in_paren = false;
						end = i;
					}
				}
			}

			// Check for mismatched parentheses
			if (num_paren > 0) {
				throw new IllegalArgumentException("missing ')'");
			}
			String pre = input.substring(0, start);
			String paren = input.substring(start + 1, end);
			String post = input.substring(end + 1);

			// Interpret adjacent parentheticals as multiplication
			// 3(-4) == 3*-4
			// 3-(4) == 3-4
			if (!pre.isEmpty() && !pre.matches(".*[\\*/\\+\\-\\^%\\?]+")) {
				pre += "*";
			}
			if (!post.isEmpty() && !post.matches("[\\*/\\+\\-\\^%\\?].*+")) {
				post = "*" + post;
			}
			result = parse(pre + parse(paren) + post);
			if (Double.isInfinite(result)) {
				throw new IllegalArgumentException("Operation produced infinity");
			}
			else if (!Double.isNaN(result)) {
				// Store result
				if (var != null) {
					vars.put(var, result);
				}
				vars.put("ans", result);
				return result;
			}
			else {
				throw new IllegalArgumentException("Operation produced NaN");
			}
		}

		// Check for constant term without operators (+ and - accepted in constants)
		if (input.matches("[\\+\\-]?\\d*(\\.(\\d*))?(E(\\d*))?")) {
			result = Double.parseDouble(input);
			if (Double.isInfinite(result)) {
				throw new IllegalArgumentException("Operation produced infinity");
			}
			else if (!Double.isNaN(result)) {
				// Store result
				if (var != null) {
					vars.put(var, result);
				}
				vars.put("ans", result);
				return result;
			}
			else {
				throw new IllegalArgumentException("Operation produced NaN");
			}
		}
		boolean found_operation = false;
		// Perform operations in order
		for (int i = 0; i < operators.length; i++) {
			try {
				result = performOperation(input, i);
				found_operation = true;
				break;
			}
			catch (NumberFormatException e) {
				throw e;
			}
			catch (IllegalArgumentException e) {
				//e.printStackTrace();
			}
		}
		if (!found_operation) {
			throw new IllegalArgumentException("Unknown operator");
		}
		if (Double.isInfinite(result)) {
			throw new IllegalArgumentException("Operation produced infinity");
		}
		else if (!Double.isNaN(result)) {
			// Store result
			if (var != null) {
				vars.put(var, result);
			}
			vars.put("ans", result);
			return result;
		}
		else {
			throw new IllegalArgumentException("Operation produced NaN");
		}
	}

	/**
	 * Parse input and perform mathematical operations
	 *
	 * @param input     the input to parse
	 * @param operation the numerical index of the operation to perform
	 * @return the parsed value
	 * @throws IllegalArgumentException
	 */
	private static double performOperation(String input, int operation) throws IllegalArgumentException {
		// Split the input around the relevant operator
		String[] mult = input.split(operators[operation], 2);
		if (mult.length > 1) {
			String pre = mult[0].substring(0, getLastTerm(mult[0]));
			String first = mult[0].substring(getLastTerm(mult[0]));
			String last = mult[1].substring(0, getFirstTerm(mult[1]));
			String post = mult[1].substring(getFirstTerm(mult[1]));
			if (first.isEmpty()) {
				first = pre;
				pre = "";
			}
			if (last.isEmpty()) {
				last = post;
				post = "";
			}
			double first_num;
			if (operation == 1 && first.isEmpty()) {
				first_num = 10.0;
			}
			else {
				first_num = Double.parseDouble(first);
			}
			double last_num = Double.parseDouble(last);
			double result;
			switch (operation) {
				case 0:
					result = Math.pow(first_num, Double.parseDouble(last));
					break;
				case 1:
					result = Math.log10(last_num) / Math.log10(first_num);
					break;
				case 2:
					result = first_num * last_num;
					break;
				case 3:
					result = first_num / last_num;
					break;
				case 4:
					result = first_num % last_num;
					break;
				case 5:
					result = first_num + last_num;
					break;
				case 6:
					result = first_num - last_num;
					break;
				default:
					throw new IllegalArgumentException("Unknown operator code");
			}
			if (Double.isInfinite(result)) {
				throw new NumberFormatException("Operation returned infinity");
			}
			if (Double.isNaN(result)) {
				throw new NumberFormatException("Operation returned NaN");
			}
			// Recursively parse result
			return parse(pre + result + post);
		}
		throw new IllegalArgumentException("Operation not contained within input");
	}

	/**
	 * Get the index of the first operator present in a string
	 *
	 * @param input the string to search
	 * @return the index of the first operator
	 */
	private static int getFirstTerm(String input) {
		for (int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			if (i < input.length() - 1 && (ch == '+' || ch == '-' || ch == '*'
					|| ch == '/' || ch == '^' || ch == '%' || ch == '?')) {
				return i;
			}
		}
		return input.length();
	}

	/**
	 * Get the index of the last operator present in a string
	 *
	 * @param input the string to search
	 * @return the index of the last operator
	 */
	private static int getLastTerm(String input) {
		for (int i = input.length() - 1; i >= 0; i--) {
			char ch = input.charAt(i);
			if (i > 0 && (ch == '+' || ch == '-' || ch == '*'
					|| ch == '/' || ch == '^' || ch == '%' || ch == '?')) {
				return i + 1;
			}
		}
		return 0;
	}
}
