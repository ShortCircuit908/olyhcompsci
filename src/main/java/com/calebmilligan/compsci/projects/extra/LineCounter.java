package com.calebmilligan.compsci.projects.extra;

import com.calebmilligan.compsci.ProjectId;

import java.io.File;
import java.util.Scanner;

/**
 * @author Caleb Milligan, period 4
 *         Created on 9/25/2015
 */
@ProjectId(-1)
public class LineCounter {

	public static void main(String... args) {
		if (args.length < 1) {
			System.out.println("Too few arguments");
			return;
		}
		File top = new File(args[0]);
		System.out.println(getLineCounts(top, 0));
	}

	private static long getLineCounts(File file, int indent) {
		long line_count = 0;
		if (file.getName().startsWith(".")) {
			return 0;
		}
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			if (files == null || files.length == 0) {
				return 0;
			}
			printIndent(indent);
			System.out.println("Scanning directory: " + file.getName());
			for (File sub : files) {
				line_count += getLineCounts(sub, indent + 1);
			}
		}
		else {
			printIndent(indent);
			System.out.print("Scanning file: " + file.getName());
			try {
				Scanner scanner = new Scanner(file);
				while (scanner.hasNextLine()) {
					line_count++;
					scanner.nextLine();
				}
			}
			catch (Exception e) {
				System.out.println(" (invalid)");
				//e.printStackTrace();
			}
			System.out.println(" (" + line_count + ")");
		}
		return line_count;
	}

	private static void printIndent(int indent) {
		for (int i = 0; i < indent; i++) {
			System.out.print(" ");
		}
	}
}
