package com.calebmilligan.compsci.projects.extra.mazegen;

import java.awt.*;

/**
 * Name: Cell.java
 * Description: Track the state of a single maze cell
 *
 * @author Caleb Milligan
 *         Created on 6/2/2016.
 */
public class Cell {
	public static final double CELL_DIM = 50.0;
	private static final double border_width = CELL_DIM / 6.0;
	private final int x;
	private final int y;
	private boolean connected_top;
	private boolean connected_right;
	private boolean connected_bottom;
	private boolean connected_left;
	private Color color = Color.BLACK;

	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public void draw(Graphics g, double scale_x, double scale_y) {
		// Fill the background
		g.setColor(color);
		g.fillRect((int) ((x * CELL_DIM) * scale_x), (int) ((y * CELL_DIM) * scale_y), (int) (CELL_DIM * scale_x) + 1, (int) (CELL_DIM * scale_y) + 1);
		// Draw the corners
		g.setColor(Color.BLACK);
		g.fillRect((int) ((x * CELL_DIM) * scale_x), (int) ((y * CELL_DIM) * scale_y), (int) (border_width * scale_x) + 1, (int) (border_width * scale_y) + 1);
		g.fillRect((int) ((x * CELL_DIM + border_width * 5) * scale_x), (int) ((y * CELL_DIM) * scale_y), (int) (border_width * scale_x) + 1, (int) (border_width * scale_y) + 1);
		g.fillRect((int) ((x * CELL_DIM) * scale_x), (int) ((y * CELL_DIM + border_width * 5) * scale_y), (int) (border_width * scale_x) + 1, (int) (border_width * scale_y) + 1);
		g.fillRect((int) ((x * CELL_DIM + border_width * 5) * scale_x), (int) ((y * CELL_DIM + border_width * 5) * scale_y), (int) (border_width * scale_x) + 1, (int) (border_width * scale_y) + 1);
		// Draw any present walls
		if (!connected_top) {
			g.fillRect((int) ((x * CELL_DIM) * scale_x), (int) ((y * CELL_DIM) * scale_y), (int) (CELL_DIM * scale_x) + 1, (int) (border_width * scale_y) + 1);
		}
		if (!connected_right) {
			g.fillRect((int) ((x * CELL_DIM + border_width * 5) * scale_x), (int) ((y * CELL_DIM) * scale_y), (int) (border_width * scale_x) + 1, (int) (CELL_DIM * scale_y) + 1);
		}
		if (!connected_bottom) {
			g.fillRect((int) ((x * CELL_DIM) * scale_x), (int) ((y * CELL_DIM + border_width * 5) * scale_y), (int) (CELL_DIM * scale_x) + 1, (int) (border_width * scale_y) + 1);
		}
		if (!connected_left) {
			g.fillRect((int) ((x * CELL_DIM) * scale_x), (int) ((y * CELL_DIM) * scale_y), (int) (border_width * scale_x) + 1, (int) (CELL_DIM * scale_y) + 1);
		}
	}

	public boolean isConnectedTop() {
		return connected_top;
	}

	public boolean isConnectedRight() {
		return connected_right;
	}

	public boolean isConnectedBottom() {
		return connected_bottom;
	}

	public boolean isConnectedLeft() {
		return connected_left;
	}

	public void setConnectedTop() {
		this.connected_top = true;
	}

	public void setConnectedRight() {
		this.connected_right = true;
	}

	public void setConnectedBottom() {
		this.connected_bottom = true;
	}

	public void setConnectedLeft() {
		this.connected_left = true;
	}

	public boolean hasConnection() {
		return connected_top || connected_right || connected_bottom || connected_left;
	}
}
