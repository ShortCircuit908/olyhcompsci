package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: HelloWorld.java
 * Description: First program!
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/15/2015
 */
@ProjectId(0)
public class HelloWorld {
	public static void main(String... args) {
		System.out.println("Hello, world!");
	}
}
