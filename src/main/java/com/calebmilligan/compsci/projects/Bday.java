package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: Bday.java
 * Description: Variables
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/28/2015
 */
@ProjectId(6)
public class Bday {
	public static void main(String... args) {
		String name = "Caleb Milligan";
		int month = 5;
		int day = 15;
		String other_name = "Meagan";
		int other_month = 6;
		int other_day = 4;
		System.out.println("My name is " + name + " and my birthday is " + month + "/" + day);
		System.out.println("My neighbor's name is " + other_name + " and her birthday is " + other_month + "/" + other_day);
	}
}
