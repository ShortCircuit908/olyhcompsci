package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: Lab3b.java
 * Description: For loops
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/7/2015
 */
@ProjectId(11)
public class Lab3b {
	public static void main(String... args) {
		for (int line = 0; line < 10; line++) {
			for (int i = 10; i > line; i--) {
				System.out.print('*');
			}
			System.out.println();
		}
	}
}
