package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.text.DecimalFormat;

/**
 * Name: Lab8.java
 * Description: 2D arrays
 *
 * @author Caleb Milligan
 *         Created on 1/11/2016
 */
@ProjectId(30)
public class Lab8 {
	private static final DecimalFormat format = new DecimalFormat("00");

	public static void main(String... args) {
		int[][] int_matrix = new int[4][5];
		int[] column_sums = new int[int_matrix[0].length];
		int[] row_sums = new int[int_matrix.length];
		int total = 0;
		for (int y = 0; y < int_matrix.length; y++) {
			System.out.print('|');
			for (int x = 0; x < int_matrix[y].length; x++) {
				int value = (int) ((Math.random() * 19.0) + 1.0);
				int_matrix[y][x] = value;
				row_sums[y] += value;
				column_sums[x] += value;
				total += value;
				System.out.print(format.format(value) + "|");
			}
			System.out.println();
		}
		for (int i = 0; i < row_sums.length; i++) {
			System.out.println("Row " + i + " sum = " + row_sums[i]);
		}
		for (int i = 0; i < column_sums.length; i++) {
			System.out.println("Column " + i + " sum = " + column_sums[i]);
		}
		System.out.println("Total sum = " + total);
	}
}
