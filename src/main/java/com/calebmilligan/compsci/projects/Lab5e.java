package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: Lab5e.java
 * Description: Strings (identify palindromes)
 *
 * @author Caleb Milligan, Period 4
 *         Created on 11/2/2015
 */
@ProjectId(23)
public class Lab5e {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please enter a phrase: ");
		String phrase = scanner.nextLine().replaceAll("\\W", ""); // Get user input and strip non-word characters ^[a-zA-Z_0-9]
		String check = new StringBuilder(phrase).reverse().toString(); // Reverse the string. If the phrase is a palindrome, the two strings will equal each other (ignoring case)
		System.out.println("The phrase is " + (phrase.equalsIgnoreCase(check) ? "" : "not ") + "a palindrome");
	}
}
