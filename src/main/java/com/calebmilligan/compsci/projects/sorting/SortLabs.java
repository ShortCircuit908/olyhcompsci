package com.calebmilligan.compsci.projects.sorting;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Name: SortLabs.java
 * Description: Sorting labs
 *
 * @author Caleb Milligan
 *         Created on 4/15/2016.
 */
public class SortLabs {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		printMenu();
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine().toLowerCase();
			if (!line.matches("(e(xit)?)|(s(election)?)|(i(nsertion)?)|(m(erge)?)")) {
				System.out.println("Invalid selection");
				continue;
			}
			if (line.matches("e(xit)?")) {
				break;
			}
			Random random = new Random();
			Integer[] arr = new Integer[100];
			for (int i = 0; i < arr.length; i++) {
				arr[i] = random.nextInt(100);
			}
			System.out.println(Arrays.toString(arr));
			long now;
			long elapsed;
			if (line.matches("s(election)?")) {
				now = System.nanoTime();
				selectionSort(arr);
				elapsed = System.nanoTime() - now;
			}
			else if (line.matches("i(nsertion)?")) {
				now = System.nanoTime();
				insertionSort(arr);
				elapsed = System.nanoTime() - now;
			}
			else {
				now = System.nanoTime();
				mergeSort(arr);
				elapsed = System.nanoTime() - now;
			}
			System.out.println(Arrays.toString(arr));
			System.out.println(elapsed + " nanoseconds elapsed\n");
			printMenu();
		}

	}

	private static void printMenu() {
		System.out.println("Choose a sort:\n\t(s)election\n\t(i)nsertion\n\t(m)erge\nOr:\n\t(e)xit");
		System.out.print("Your selection: ");
	}

	public static <T extends Comparable<T>> void selectionSort(T[] arr) {
		int index = 0, i, min_index;
		T tmp;
		while (index < arr.length - 1) {
			min_index = index;
			for (i = min_index + 1; i < arr.length; i++) {
				if (compare(arr[i], arr[min_index]) < 0) {
					min_index = i;
				}
			}
			tmp = arr[index];
			arr[index++] = arr[min_index];
			arr[min_index] = tmp;
		}
	}

	public static <T extends Comparable<T>> void insertionSort(T[] arr) {
		int i, j;
		T tmp;
		for (i = 1; i < arr.length; i++) {
			j = i;
			tmp = arr[j];
			while (j > 0 && compare(arr[j - 1], tmp) > 0) {
				arr[j] = arr[j - 1];
				j--;
			}
			arr[j] = tmp;
		}
	}

	@SuppressWarnings("unchecked")
	public static <T extends Comparable<T>> void mergeSort(T[] arr) {
		mergeSort(arr, 0, arr.length, (T[]) Array.newInstance(arr.getClass().getComponentType(), arr.length));
	}

	public static <T extends Comparable<T>> void mergeSort(T[] arr, int start, int len, T[] working) {
		if (len <= 1) {
			return;
		}
		if (len == 2) {
			if (compare(arr[start], arr[start + 1]) > 0) {
				T tmp = arr[start];
				arr[start] = arr[start + 1];
				arr[start + 1] = tmp;
			}
			return;
		}
		int split = len / 2, left_head = start, left_tail = start + split, right_head = start + split, right_tail = start + len;
		mergeSort(arr, start, split, working);
		mergeSort(arr, start + split, len - split, working);
		for (int i = start; i < start + len; i++) {
			if (left_head < left_tail && (right_head >= right_tail || compare(arr[left_head], arr[right_head]) < 0)) {
				working[i] = arr[left_head++];
			}
			else {
				working[i] = arr[right_head++];
			}
		}
		System.arraycopy(working, start, arr, start, len);
	}

	public static <T extends Comparable<E>, E> int compare(T o1, E o2) {
		if (o1 == o2) {
			return 0;
		}
		return o1 == null ? -1 : o2 == null ? 1 : o1.compareTo(o2);
	}
}
