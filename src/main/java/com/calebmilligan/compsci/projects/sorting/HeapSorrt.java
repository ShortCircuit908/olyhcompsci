package com.calebmilligan.compsci.projects.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * @author ShortCircuit908
 *         Created on 4/18/2016.
 */
public class HeapSorrt {
	public static void main(String... args) {
		Random random = new Random();
		Integer[] arr = new Integer[42];
		ArrayList<Integer> s = new ArrayList<>(arr.length);
		for (int i = 0; i < arr.length; i++) {
			//arr[i] = random.nextInt(arr.length);
			s.add(i);
		}
		Collections.shuffle(s);
		s.toArray(arr);
		System.out.println(Arrays.toString(arr));
		sort(arr);
	}


	public static <T extends Comparable<T>> void sort(T[] arr) {
		heapify(arr);
		int n = arr.length - 1;
		for (int i = n; i > 0; i--) {
			swap(arr, 0, i);
			maxheap(arr, 0, --n);
		}
	}

	public static <T extends Comparable<T>> void heapify(T[] arr) {
		int n = arr.length - 1;
		for (int i = n / 2; i >= 0; i--) {
			maxheap(arr, i, n);
		}
	}

	public static <T extends Comparable<T>> int maxheap(T[] arr, int i, int n) {
		int left = i * 2;
		int right = i * 2 + 1;
		int max = i;
		if (left <= n && compare(arr[left], arr[i]) > 0) {
			max = left;
		}
		if (right <= n && compare(arr[right], arr[max]) > 0) {
			max = right;
		}
		if (max != i) {
			swap(arr, i, max);
			maxheap(arr, max, n);
		}
		return n;
	}

	public static <T> void swap(T[] arr, int i, int j) {
		T tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
		System.out.println(Arrays.toString(arr));
	}

	public static <T extends Comparable<E>, E> int compare(T o1, E o2) {
		if (o1 == o2) {
			return 0;
		}
		return o1 == null ? -1 : o2 == null ? 1 : o1.compareTo(o2);
	}
}
