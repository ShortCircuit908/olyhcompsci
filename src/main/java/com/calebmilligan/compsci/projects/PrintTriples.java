package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: PrintTriples.java
 * Description: Print (on one line) the first 20 multiples of 3
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/2/2015
 */
@ProjectId(9)
public class PrintTriples {
	public static void main(String... args) {
		for (int i = 1; i <= 20; i++) {
			System.out.print((i * 3) + ", ");
		}
	}
}
