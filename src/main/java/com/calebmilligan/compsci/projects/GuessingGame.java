package com.calebmilligan.compsci.projects;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * @author ShortCircuit908
 *         Created on 5/5/2016.
 */
public class GuessingGame implements Runnable {
	private static final Scanner scanner = new Scanner(System.in);
	private int min = 1;
	private int max = 101;
	private boolean success = false;

	public GuessingGame() {
		new Thread(this).start();
	}

	public static void main(String... args) {
		new GuessingGame();
	}

	private static String getInput(Pattern matcher) {
		String input;
		while (!matcher.matcher(input = scanner.nextLine()).matches()) {
			System.out.println("Invalid input");
		}
		return input;
	}

	public void guess() {
		int guess = (max - min) / 2 + min;
		System.out.println("Is your number " + guess + "?");
		System.out.print("higher/lower/equal: ");
		String input = getInput(Pattern.compile("high(er)?|low(er)?|equal(s)?", Pattern.CASE_INSENSITIVE));
		if (input.matches("high(er)?")) {
			min = guess;
		}
		else if (input.matches("low(er)?")) {
			max = guess;
		}
		else {
			System.out.println("Got it!");
			success = true;
		}
	}

	@Override
	public void run() {
		System.out.println("Pick a number between 1-100 and remember it.");
		while (!success) {
			guess();
		}
	}
}
