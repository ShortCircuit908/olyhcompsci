package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: Lab5d.java
 * Description: Strings (convert user input into pig latin)
 * <p>
 * (I unknowingly included the functionality of this lab in Lab5b, so I just copied the whole thing)
 *
 * @author Caleb Milligan, Period 4
 *         Created on 11/2/2015
 */
@ProjectId(22)
public class Lab5d {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a phrase: ");
		String line = scanner.nextLine().replaceAll("[^a-zA-Z\\s']", "");
		for (String word : line.split(" ")) {
			word = word.toLowerCase();
			System.out.print(translate(word) + " ");
		}
	}

	private static String translate(String word) {
		if (startsWithVowel(word) || startsWithSilentLetter(word)) {
			return word + "yay";
		}
		else {
			char first_vowel = 'e';
			vowel_finder:
			for (char ch : word.toCharArray()) {
				switch (ch) {
					case 'a':
					case 'e':
					case 'i':
					case 'o':
					case 'u':
					case 'y':
						first_vowel = ch;
						break vowel_finder;
				}
			}
			String[] parts = word.split("[aeiouy]", 2);
			return first_vowel + parts[1] + parts[0] + "ay";
		}
	}

	private static boolean startsWithVowel(String str) {
		return str.toLowerCase().matches("[ia]|([aeiouy]\\w+)");
	}

	private static boolean startsWithSilentLetter(String str) {
		return str.toLowerCase().matches("(kn|wr|gn|ps|mn|wh)\\w+");
	}
}
