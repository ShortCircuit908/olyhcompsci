package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: Lab5c.java
 * Description: Strings (formatting input)
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/27/2015
 */
@ProjectId(21)
public class Lab5c {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a phone number to format: ");
		String input = scanner.nextLine().replaceAll("\\D", "");
		String formatted = "";
		int offset = 0;
		switch (input.length()) {
			case 11:
				formatted = input.substring(0, (offset += 1));
			case 10:
				formatted += "(" + input.substring(offset, (offset += 3)) + ")";
			case 7:
				formatted += input.substring(offset, (offset += 3)) + "-" + input.substring(offset);
				break;
			default:
				System.out.println("Please enter a valid phone number");
				return;
		}
		System.out.println(formatted);
	}
}
