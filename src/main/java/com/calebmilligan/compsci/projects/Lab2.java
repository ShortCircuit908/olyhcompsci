package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.ArrayList;

/**
 * Name: Lab2.java
 * Description: There Was an Old Lady poem. Creating methods. You need at least six different methods.
 *
 * @author Caleb Milligan, Period 4
 *         Created on 9/18/2015
 */
@ProjectId(5)
public class Lab2 {
	private static final ArrayList<String> chain = new ArrayList<>();

	public static void main(String... args) {
		swallowObject("fly");
		swallowObject("spider", "That wriggled and iggled and jiggled inside her.");
		swallowObject("bird", "How absurd to swallow a %1$s.");
		swallowObject("cat", "Imagine that to swallow a %1$s.");
		swallowObject("dog", "What a hog to swallow a %1$s.");
		swallowObject("horse", true);
	}

	/**
	 * Method wrapper to simplify calls
	 *
	 * @see #swallowObject(String, boolean, String...)
	 */
	private static void swallowObject(String object, String... addendum) {
		swallowObject(object, false, addendum);
	}

	/**
	 * @param object   The object to swallow
	 * @param dies     Whether or not the old lady dies after this call
	 * @param addendum Additional lines
	 */
	private static void swallowObject(String object, boolean dies, String... addendum) {
		// Print first line of stanza
		System.out.println("There was an old lady who swallowed " + (startsWithVowel(object) ? "an " : "a ") + object + ".");
		// Print additional lines
		for (String line : addendum) {
			if (line != null && !line.isEmpty()) {
				System.out.println(String.format(line, object));
			}
		}
		// Print the chain in reverse order
		if (chain.size() > 0 && !dies) {
			String last_prey = object;
			for (int i = chain.size() - 1; i >= 0; i--) {
				catchObject(last_prey, chain.get(i));
				last_prey = chain.get(i);
			}
		}
		// Add the current object to the chain for the next iteration
		chain.add(object);
		if (!dies) {
			question();
		}
		// Print the last line of the stanza
		die(dies);
	}

	/**
	 * Function to format and print elements of the chain
	 *
	 * @param predator The object that was swallowed
	 * @param prey     The object the predator was intended to catch
	 */
	private static void catchObject(String predator, String prey) {
		System.out.println("She swallowed the " + predator + " to catch the " + prey + ",");
	}

	/**
	 * Function to format and print the last line of each stanza
	 *
	 * @param certain Whether or not the old lady should die
	 */
	private static void die(boolean certain) {
		System.out.println((certain ? "She died of course." : "Perhaps she'll die.") + '\n');
	}

	/**
	 * Filler to increase method count
	 */
	private static void question() {
		System.out.println("I don't know why she swallowed that fly,");
	}

	/**
	 * Tests whether a string begins with a vowel (ignoring y)
	 *
	 * @param str String to check for a leading vowel
	 * @return <tt>true</tt> if the string starts with a vowel
	 */
	private static boolean startsWithVowel(String str) {
		if (str.length() == 0) {
			return false;
		}
		return str.matches("[aeiou]\\w+");
	}

}