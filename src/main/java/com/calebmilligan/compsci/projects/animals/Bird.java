package com.calebmilligan.compsci.projects.animals;

/**
 * @author Caleb Milligan
 *         Created on 2/26/2016
 */
public abstract class Bird extends Animal {
	private boolean can_fly;

	public Bird() {
		this(null, true);
	}

	public Bird(Boolean gender) {
		this(gender, true);
	}

	public Bird(Boolean gender, boolean can_fly) {
		super(gender);
		this.can_fly = can_fly;
	}

	public String speak() {
		return "Feathers for me";
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + getPronoun() + (can_fly ? " can" : " cannot") + " fly";
	}
}
