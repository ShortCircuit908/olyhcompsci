package com.calebmilligan.compsci.projects.animals;

/**
 * @author Caleb Milligan
 *         Created on 2/26/2016
 */
public class Dog extends Mammal {
	private String name;

	public Dog() {
		this(null, null, null);
	}

	public Dog(String name) {
		this(name, null, null);
	}

	public Dog(String name, String hair_type) {
		this(name, null, hair_type);
	}

	public Dog(String name, Boolean gender) {
		this(name, gender, null);
	}

	public Dog(String name, Boolean gender, String hair_type) {
		super(gender, hair_type);
		this.name = name;
	}

	@Override
	public String speak() {
		return "bark, bark";
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + getPossessivePronoun() + " name is " + (name == null ? "unknown" : name);
	}
}
