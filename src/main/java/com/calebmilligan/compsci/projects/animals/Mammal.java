package com.calebmilligan.compsci.projects.animals;

/**
 * @author Caleb Milligan
 *         Created on 2/26/2016
 */
public abstract class Mammal extends Animal {
	private String hair_type;

	public Mammal() {
		this(null, null);
	}

	public Mammal(Boolean gender) {
		this(gender, null);
	}

	public Mammal(String hair_type) {
		this(null, hair_type);
	}

	public Mammal(Boolean gender, String hair_type) {
		super(gender);
		this.hair_type = hair_type;
	}

	public String speak() {
		return "I'm hairy";
	}

	public String getHairType() {
		return hair_type == null ? "unknown" : hair_type;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + getPossessivePronoun() + " hair is " + getHairType();
	}
}
