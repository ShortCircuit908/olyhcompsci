package com.calebmilligan.compsci.projects.zooproject;

/**
 * Name: Plesiosaur.java
 * Description: Represents the attributes/functions of a plesiosaur
 *
 * @author Caleb Milligan
 *         Created on 12/2/2015
 */
public class Plesiosaur extends Animal {
	public Plesiosaur() {
		super();
	}

	public Plesiosaur(String name, int age, double weight, Boolean gender) {
		super(name, age, weight, gender);
	}

	@Override
	public String speak() {
		return "Mroooooouuu";
	}
}
