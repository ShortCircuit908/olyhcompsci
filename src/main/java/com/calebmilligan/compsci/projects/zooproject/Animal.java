package com.calebmilligan.compsci.projects.zooproject;

/**
 * Name: Animal.java
 * Description: Base class for zoo animals
 *
 * @author Caleb Milligan
 *         Created on 12/1/2015
 */
public abstract class Animal {
	protected String name; // Null is unknown
	protected int age; // Zero or less is unknown
	protected double weight; // Zero or less is unknown
	protected Boolean gender; // False is male, true is female, null is unknown

	public Animal() {
		name = null;
		age = -1;
		weight = -1;
		gender = null;
	}

	public Animal(String name, int age, double weight, Boolean gender) {
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public void printInfo() {
		System.out.println(
				String.format("This %1$s's name is %2$s.\n"
								+ "%2$s's age is %3$s.\n"
								+ "%2$s's weight is %4$s.\n"
								+ "%2$s's gender is %5$s.",
						getClass().getSimpleName(),
						(name == null ? "uknown" : name),
						(age <= 0 ? "unknown" : age),
						(weight <= 0 ? "unknown" : weight),
						(gender == null ? "unknown" : gender ? "female" : "male"))

		);
	}

	public abstract String speak();
}
