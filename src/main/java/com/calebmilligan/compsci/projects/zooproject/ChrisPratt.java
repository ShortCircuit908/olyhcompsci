package com.calebmilligan.compsci.projects.zooproject;

/**
 * Name: ChrisPratt.java
 * Description: Represents the attributes/functions of Chris Pratt
 *
 * @author Caleb Milligan
 *         Created on 12/2/2015
 */
public class ChrisPratt extends Animal {
	public ChrisPratt() {
		super();
	}

	public ChrisPratt(String name, int age, double weight, Boolean gender) {
		super(name, age, weight, gender);
	}

	@Override
	public String speak() {
		return "Clever girl";
	}
}
