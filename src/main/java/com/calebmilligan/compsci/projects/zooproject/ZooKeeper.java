package com.calebmilligan.compsci.projects.zooproject;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: ZooKeeper.java
 * Description: Keep track of zoo animals, contains main method
 *
 * @author Caleb Milligan
 *         Created on 12/1/2015
 */
@ProjectId(26)
public class ZooKeeper {
	public static void main(String... args) {
		System.out.println("Welcome... to Jurassic Park!\n");

		Velociraptor raptor = new Velociraptor("Timmy", 6, 300, true);
		raptor.printInfo();
		System.out.println(raptor.getName() + " says \"" + raptor.speak() + "\"\n");

		raptor = new Velociraptor();
		raptor.setName("Tommy");
		raptor.setAge(6);
		raptor.setWeight(280.5);
		raptor.setGender(false);
		raptor.printInfo();
		System.out.println(raptor.getName() + " says \"" + raptor.speak() + "\"\n");

		Pterodactyl pterodactyl = new Pterodactyl("Leo", 8, 600, false);
		pterodactyl.printInfo();
		System.out.println(pterodactyl.getName() + " says \"" + pterodactyl.speak() + "\"\n");

		pterodactyl = new Pterodactyl();
		pterodactyl.setName("Louis");
		pterodactyl.setAge(10);
		pterodactyl.setWeight(650);
		pterodactyl.setGender(true);
		pterodactyl.printInfo();
		System.out.println(pterodactyl.getName() + " says \"" + pterodactyl.speak() + "\"\n");

		Plesiosaur plessie = new Plesiosaur("Plessie", 11, 20000, true);
		plessie.printInfo();
		System.out.println(plessie.getName() + " says \"" + plessie.speak() + "\"\n");

		plessie = new Plesiosaur();
		plessie.setName("Bessie");
		plessie.setAge(Integer.MAX_VALUE);
		plessie.setWeight(20000);
		plessie.setGender(null);
		plessie.printInfo();
		System.out.println(plessie.getName() + " says \"" + plessie.speak() + "\"\n");

		ChrisPratt crisp_rat = new ChrisPratt("Chris Pratt", 36, 190, false);
		crisp_rat.printInfo();
		System.out.println(crisp_rat.getName() + " says \"" + crisp_rat.speak() + "\"\n");

		crisp_rat = new ChrisPratt();
		crisp_rat.setName("Chris Pratt's inner child");
		crisp_rat.setAge(8);
		crisp_rat.setWeight(65);
		crisp_rat.setGender(null);
		crisp_rat.printInfo();
		System.out.println(crisp_rat.getName() + " says \"" + crisp_rat.speak() + "\"\n");
	}
}
