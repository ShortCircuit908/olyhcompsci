package com.calebmilligan.compsci.projects.zooproject;

/**
 * Name: Pterodactyl.java
 * Description: Represents the attributes/functions of a pterodactyl
 *
 * @author ShortCircuit908
 *         Created on 12/2/2015
 */
public class Pterodactyl extends Animal {
	public Pterodactyl() {
		super();
	}

	public Pterodactyl(String name, int age, double weight, Boolean gender) {
		super(name, age, weight, gender);
	}

	@Override
	public String speak() {
		return "REEEEEEEE";
	}
}
