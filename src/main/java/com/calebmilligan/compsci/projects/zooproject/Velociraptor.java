package com.calebmilligan.compsci.projects.zooproject;

/**
 * Name: Velociraptor.java
 * Description: Represents the attributes/functions of a velociraptor
 *
 * @author Caleb Milligan
 *         Created on 12/2/2015
 */
public class Velociraptor extends Animal {
	public Velociraptor() {
		super();
	}

	public Velociraptor(String name, int age, double weight, Boolean gender) {
		super(name, age, weight, gender);
	}

	@Override
	public String speak() {
		return "Roaaaaaaaa";
	}
}
