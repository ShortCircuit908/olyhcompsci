package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.Scanner;

/**
 * Name: Lab4b.java
 * Description: User input
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/14/2015
 */
@ProjectId(17)
public class Lab4b {
	public static void main(String... args) {
		Scanner scanner = new Scanner(System.in);
		Double min = null;
		Double max = null;
		double num;
		while (true) {
			System.out.print("Enter a number (0 to quit): ");
			try {
				/* Avoid using "scanner.nextDouble()" here as it can result in an infinite loop if given
				 * a malformed number. "scanner.nextDouble()" CAN be used, but it requires an
				 * accompanying "scanner.nextLine()" in the catch block to flush the input if an
				 * invalid token is given
				 */
				num = Double.parseDouble(scanner.nextLine());
				if (num == 0) {
					break;
				}
				if (min == null || num < min) {
					min = num;
				}
				if (max == null || num > max) {
					max = num;
				}
			}
			catch (NumberFormatException e) {
				System.out.print("Invalid input. ");
			}
		}
		scanner.close();
		System.out.println("Smallest: " + (min == null ? 0 : min));
		System.out.println("Largest: " + (max == null ? 0 : max));
	}
}
