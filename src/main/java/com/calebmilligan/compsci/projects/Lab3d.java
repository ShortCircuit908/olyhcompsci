package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: Lab3d.java
 * Description: For loops
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/7/2015
 */
@ProjectId(13)
public class Lab3d {
	public static void main(String... args) {
		for (int line = -4; line <= 4; line++) {
			int char_count = 9 - Math.abs(line * 2);
			int padding = (9 - char_count) / 2;
			repChar(' ', padding);
			repChar('*', char_count);
			repChar(' ', padding);
			System.out.println();
		}
	}

	private static void repChar(char ch, int len) {
		for (int i = 0; i < len; i++) {
			System.out.print(ch);
		}
	}
}
