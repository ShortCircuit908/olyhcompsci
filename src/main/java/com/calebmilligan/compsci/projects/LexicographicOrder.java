package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Caleb Milligan
 *         Created on 2/11/2016
 */
@ProjectId(34)
public class LexicographicOrder {
	public static void main(String... args) {
		ArrayList<String> teachers = new ArrayList<>(6);
		teachers.add("Nunn");
		teachers.add("Hart");
		teachers.add("Turnbow");
		teachers.add("Houge");
		teachers.add("Violette");
		teachers.add("Curley");
		System.out.println(teachers);
		ArrayList<String> sorted = copySort(teachers);
		System.out.println(sorted);
		//benchmark(10000);
	}

	/**
	 * Perform efficiency benchmarks comparing {@link #sort(List)} and {@link #oldSort(List)}
	 */
	public static void benchmark(long sample_size) {
		long total_elapsed_method_1 = 0;
		long total_elapsed_method_2 = 0;
		for (int i = 0; i < sample_size; i++) {
			ArrayList<Character> test_list_1 = createRandomCharacterList();
			ArrayList<Character> test_list_2 = new ArrayList<>(test_list_1);
			long now = System.nanoTime();
			sort(test_list_1);
			total_elapsed_method_1 += System.nanoTime() - now;
			now = System.nanoTime();
			oldSort(test_list_2);
			total_elapsed_method_2 += System.nanoTime() - now;
		}
		total_elapsed_method_1 /= sample_size;
		total_elapsed_method_2 /= sample_size;
		System.out.println("sort()    - " + total_elapsed_method_1 + " nanos average");
		System.out.println("oldSort() - " + total_elapsed_method_2 + " nanos average");
	}

	/**
	 * Create and shuffle an ArrayList of Characters from 'A' to 'z'
	 */
	private static ArrayList<Character> createRandomCharacterList() {
		ArrayList<Character> test = new ArrayList<>('z' - 'A');
		for (char i = 'A'; i <= 'z'; i++) {
			test.add(i);
		}
		Collections.shuffle(test);
		return test;
	}

	/**
	 * Sort a list by copying its contents into a new ArrayList and returning
	 * the sorted clone
	 */
	public static <T extends Comparable<T>> ArrayList<T> copySort(List<T> list) {
		ArrayList<T> copy = new ArrayList<>(list);
		sort(copy);
		return copy;
	}

	/**
	 * First attempt at a sorting method
	 * <p/>
	 * Simply iterates over the list, moving lower values to the front,
	 * repeating until all elements are in order
	 * <p/>
	 * This method may perform fewer total iterations, but must check
	 * the entire list each time, resulting in lower performance
	 */
	public static <T extends Comparable<T>> void oldSort(List<T> list) {
		while (true) {
			boolean found_mismatch = false;
			for (int i = 1; i < list.size(); i++) {
				if (list.get(i).compareTo(list.get(i - 1)) < 0) {
					T elem = list.remove(i);
					list.add(0, elem);
					found_mismatch = true;
				}
			}
			if (!found_mismatch) {
				break;
			}
		}
	}

	/**
	 * Second attempt at a more efficient sorting method
	 * <p/>
	 * This method typically has a higher number of total iterations,
	 * but optimizes by ensuring each operation moves the current
	 * element to its final location in the list (approx. 2.8x faster
	 * than the previous method)
	 */
	public static <T extends Comparable<T>> void sort(List<T> list) {
		int index = 0;
		while (true) {
			int lowest_index = index;
			boolean change_needed = false;
			for (int i = index + 1; i < list.size(); i++) {
				if (list.get(i).compareTo(list.get(lowest_index)) < 0) {
					lowest_index = i;
				}
				if (list.get(i).compareTo(list.get(i - 1)) < 0) {
					change_needed = true;
				}
			}
			if (!change_needed) {
				break;
			}
			list.add(index++, list.remove(lowest_index));
		}
	}
}
