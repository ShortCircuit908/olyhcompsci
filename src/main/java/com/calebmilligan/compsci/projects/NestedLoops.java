package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

/**
 * Name: NestedLoops.java
 * Description: Nested for loops
 *
 * @author Caleb Milligan, Period 4
 *         Created on 10/14/2015
 */
@ProjectId(14)
public class NestedLoops {
	public static void main(String... args) {
		for (int x = 2; x <= 5; x++) {
			for (int y = 1; y <= 5; y++) {
				System.out.print(x * y + " ");
			}
			System.out.println();
		}
	}
}
