package com.calebmilligan.compsci.projects.deck;

/**
 * @author ShortCircuit908
 *         Created on 2/9/2016
 */
public class Card {
	private Suit suit;
	private String rank;
	private int value;

	public Card(String rank, String suit, int value) {
		this.rank = rank;
		this.suit = Suit.valueOf(suit.toUpperCase());
		this.value = value;
	}

	public Suit suit() {
		return suit;
	}

	public String rank() {
		return rank;
	}

	public int pointValue() {
		return value;
	}

	public boolean matches(Card other) {
		return other.suit == this.suit && other.rank.equals(this.rank) && other.value == this.value;
	}

	@Override
	public String toString() {
		return (rank + " of " + suit + "pointvalue = " + value + ")");
	}

	@Override
	public boolean equals(Object o) {
		return o != null && o instanceof Card && matches((Card) o);
	}

	public enum Suit {
		HEARTS,
		DIAMONDS,
		SPADES,
		CLUBS
	}
}
