package com.calebmilligan.compsci.projects.deck;

import java.util.Collections;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 2/9/2016
 */
public class Deck {
	private final LinkedList<Card> cards;
	private final int initial_size;

	public Deck(String[] ranks, String[] suits, int[] values) {
		initial_size = ranks.length * suits.length * values.length;
		cards = new LinkedList<>();
		for (String rank : ranks) {
			for (String suit : suits) {
				for (int value : values) {
					cards.add(new Card(rank, suit, value));
				}
			}
		}
	}

	public boolean isEmpty() {
		return cards.isEmpty();
	}

	public int size() {
		return cards.size();
	}

	public void shuffle() {
		Collections.shuffle(cards);
	}

	public Card deal() {
		return cards.pop();
	}

	@Override
	public String toString() {
		String rtn = "size = " + initial_size + "\nUndealt cards: \n";

		for (int k = initial_size - 1; k >= 0; k--) {
			rtn = rtn + cards.get(k);
			if (k != 0) {
				rtn = rtn + ", ";
			}
			if ((initial_size - k) % 2 == 0) {
				// Insert carriage returns so entire deck is visible on console.
				rtn = rtn + "\n";
			}
		}

		rtn = rtn + "\nDealt cards: \n";
		for (int k = cards.size() - 1; k >= initial_size; k--) {
			rtn = rtn + cards.get(k);
			if (k != initial_size) {
				rtn = rtn + ", ";
			}
			if ((k - cards.size()) % 2 == 0) {
				// Insert carriage returns so entire deck is visible on console.
				rtn = rtn + "\n";
			}
		}

		rtn = rtn + "\n";
		return rtn;
	}
}
