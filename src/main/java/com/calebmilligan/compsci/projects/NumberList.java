package com.calebmilligan.compsci.projects;

import com.calebmilligan.compsci.ProjectId;

import java.lang.reflect.Array;

/**
 * Name: StringList.java
 * Description: Array practice
 *
 * @author Caleb Milligan
 *         Created on 12/11/2015
 */
@ProjectId(28)
public class NumberList {
	public static void main(String... args) {
		// Define new int array
		int[] number_list_1 = new int[5];
		// Populate the array with odd numbers
		for (int i = 0; i < number_list_1.length; i++) {
			number_list_1[i] = 1 + (i * 2);
		}
		printArrayParts(number_list_1);

		System.out.println();

		// Define new int array
		int[] number_list_2 = new int[6];
		// Populate the array with even numbers
		for (int i = 0; i < number_list_2.length; i++) {
			number_list_2[i] = (i + 1) * 2;
		}
		printArrayParts(number_list_2);
	}

	private static void printArrayParts(Object array) {
		int length;
		if (array == null) {
			throw new IllegalArgumentException("parameter may not be null");
		}
		if (!array.getClass().isArray() || (length = Array.getLength(array)) == 0) {
			throw new IllegalArgumentException("parameter is not an array, or is empty");
		}
		System.out.println(Array.get(array, 0));
		System.out.println(Array.get(array, (length - 1) / 2));
		System.out.println(Array.get(array, length - 1));
	}
}
