package com.calebmilligan.compsci.image;

import sun.awt.image.ToolkitImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 9/29/2015
 */
public class ImageUtils {
	/**
	 * Center an image on a new transparent canvas
	 *
	 * @param image  The image to center
	 * @param width  Width of the canvas
	 * @param height Height of the canvas
	 * @return The centered canvas
	 */
	public static BufferedImage centerOnCanvas(BufferedImage image, int width, int height) {
		BufferedImage canvas;
		if (image.getColorModel() instanceof IndexColorModel) {
			canvas = new BufferedImage(width, height, image.getType(), (IndexColorModel) image.getColorModel());
		}
		else {
			canvas = new BufferedImage(width, height, image.getType());
		}
		Graphics2D graphics = canvas.createGraphics();
		graphics.setBackground(new Color(0, true));
		graphics.clearRect(0, 0, width, height);
		graphics.drawImage(image, (width - image.getWidth(null)) / 2, (height - image.getHeight(null)) / 2, null);
		return canvas;
	}

	/**
	 * Scale an image while maintaining the original aspect ratio
	 *
	 * @param image       The image to scale
	 * @param width       The new width
	 * @param height      The new height
	 * @param scale_hints Scaling hints
	 * @return The scaled image
	 */
	public static ToolkitImage scaleImage(Image image, int width, int height, int scale_hints) {
		double scale_x = (double) width / (double) image.getWidth(null);
		double scale_y = (double) height / (double) image.getHeight(null);
		ToolkitImage scaled;
		if (scale_x > scale_y) {
			scaled = (ToolkitImage) image.getScaledInstance((int) (image.getWidth(null) * scale_y), (int) (image.getHeight(null) * scale_y), scale_hints);
		}
		else {
			scaled = (ToolkitImage) image.getScaledInstance((int) (image.getWidth(null) * scale_x), (int) (image.getHeight(null) * scale_x), scale_hints);
		}
		return scaled;
	}

	public static byte[] serializeImage(RenderedImage image, String format) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, format, out);
		byte[] data = out.toByteArray();
		out.close();
		return data;
	}

	public static BufferedImage deserializeImage(byte[] data) throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		BufferedImage image = ImageIO.read(in);
		in.close();
		return image;
	}
}
