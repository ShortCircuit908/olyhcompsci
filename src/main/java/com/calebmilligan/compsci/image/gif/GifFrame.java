package com.calebmilligan.compsci.image.gif;

import java.awt.image.BufferedImage;

/**
 * @author ShortCircuit908
 *         Created on 10/20/2015
 */
public class GifFrame {
	private final BufferedImage image;
	private final BufferedImage composite_image;
	private final int offset_x;
	private final int offset_y;
	private final int image_width;
	private final int image_height;
	private final boolean interlace;
	private final GifImage.ColorTable local_color_table;
	private final DisposalMethod disposal_method;
	private final boolean user_input;
	private final boolean transparent_color;
	private final int delay;
	private final short transparent_color_index;

	public GifFrame(BufferedImage image, BufferedImage composite_image, int offset_x, int offset_y, int image_width, int image_height, boolean interlace, GifImage.ColorTable local_color_table, DisposalMethod disposal_method, boolean user_input, boolean transparent_color, int delay, short transparent_color_index) {
		this.image = image;
		this.composite_image = composite_image;
		this.offset_x = offset_x;
		this.offset_y = offset_y;
		this.image_width = image_width;
		this.image_height = image_height;
		this.interlace = interlace;
		this.local_color_table = local_color_table;
		this.disposal_method = disposal_method;
		this.user_input = user_input;
		this.transparent_color = transparent_color;
		this.delay = delay;
		this.transparent_color_index = transparent_color_index;
	}

	public BufferedImage getImage() {
		return image;
	}

	public BufferedImage getCompositeImage() {
		return composite_image;
	}

	public int getOffsetX() {
		return offset_x;
	}

	public int getOffsetY() {
		return offset_y;
	}

	public boolean doInterlace() {
		return interlace;
	}

	public boolean requireUserInput() {
		return user_input;
	}

	public boolean hasTransparentColor() {
		return transparent_color;
	}

	public short getTransparentColorIndex() {
		return transparent_color_index;
	}

	public int getWidth() {
		return image_width;
	}

	public int getHeight() {
		return image_height;
	}

	public int getDelay() {
		return delay;
	}

	public DisposalMethod getDisposalMethod() {
		return disposal_method;
	}

	public GifImage.ColorTable getLocalColorTable() {
		return local_color_table;
	}

	public enum DisposalMethod {
		NONE("none"),
		DO_NOT_DISPOSE("doNotDispose"),
		RESTORE_TO_BACKGROUND_COLOR("restoreToBackgroundColor"),
		RESTORE_TO_PREVIOUS("restoreToPrevious"),
		UNDEFINED_DISPOSAL_METHOD_4("undefinedDisposalMethod4"),
		UNDEFINED_DISPOSAL_METHOD_5("undefinedDisposalMethod5"),
		UNDEFINED_DISPOSAL_METHOD_6("undefinedDisposalMethod6"),
		UNDEFINED_DISPOSAL_METHOD_7("undefinedDisposalMethod7");

		private final String disposal_method_name;

		DisposalMethod(String disposal_method_name) {
			this.disposal_method_name = disposal_method_name;
		}

		public String getDisposalMethodName() {
			return disposal_method_name;
		}

		public static DisposalMethod getDisposalMethod(String disposal_method_name) {
			for (DisposalMethod disposal_method : values()) {
				if (disposal_method.disposal_method_name.equals(disposal_method_name)) {
					return disposal_method;
				}
			}
			return DisposalMethod.NONE;
		}
	}
}
