package com.calebmilligan.compsci.image.gif;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * @author ShortCircuit908
 *         Created on 10/20/2015
 */
public class GifImage {
	private final ArrayList<GifFrame> frames = new ArrayList<>();
	private final String version;
	private final int width;
	private final int height;
	private final byte color_resolution; // 1-8
	private final short pixel_aspect_ratio; // 0-255
	private final ColorTable global_color_table;

	private GifImage(String version, int width, int height, byte color_resolution, short pixel_aspect_ratio, ColorTable global_color_table) {
		this.version = version;
		this.width = width;
		this.height = height;
		this.color_resolution = color_resolution;
		this.pixel_aspect_ratio = pixel_aspect_ratio;
		this.global_color_table = global_color_table;
	}

	public ArrayList<GifFrame> getFrames() {
		return frames;
	}

	public String getVersion() {
		return version;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public byte getColorResolution() {
		return color_resolution;
	}

	public short getPixelAspectRatio() {
		return pixel_aspect_ratio;
	}

	public ColorTable getGlobalColorTable() {
		return global_color_table;
	}

	public static GifImage explodeGif(InputStream in) throws IOException {
		final ImageReader reader = ImageIO.getImageReadersByFormatName("gif").next();
		reader.setInput(ImageIO.createImageInputStream(in));
		final IIOMetadata stream_metadata = reader.getStreamMetadata();
		final IIOMetadataNode root_node = (IIOMetadataNode) stream_metadata.getAsTree(stream_metadata.getNativeMetadataFormatName());
		// TODO: Global info
		final String version;
		final int width;
		final int height;
		final byte color_resolution; // 1-8
		final short pixel_aspect_ratio; // 0-255
		final ColorTable global_color_table;
		// TODO: Gather info
		IIOMetadataNode node;
		// TODO: Get version info
		node = getNodeByName(root_node, "Version");
		if (node != null) {
			version = node.getAttribute("value");
		}
		else {
			version = "unknown";
		}
		// TODO: Get screen descriptor info
		node = getNodeByName(root_node, "LogicalScreenDescriptor");
		assert node != null;
		width = Integer.parseInt(node.getAttribute("logicalScreenWidth"));
		height = Integer.parseInt(node.getAttribute("logicalScreenHeight"));
		color_resolution = Byte.parseByte(node.getAttribute("colorResolution"));
		pixel_aspect_ratio = Short.parseShort(node.getAttribute("pixelAspectRatio"));
		// TODO: Get global color table
		node = getNodeByName(root_node, "GlobalColorTable");
		assert node != null;
		global_color_table = getColorTable(node, color_resolution, false);
		GifImage gif_image = new GifImage(version, width, height, color_resolution, pixel_aspect_ratio, global_color_table);
		try {
			int index = 0;
			BufferedImage master_image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D master_graphics = master_image.createGraphics();
			master_graphics.setBackground(new Color(0, true));
			while (true) {
				final IIOMetadata image_metadata = reader.getImageMetadata(index);
				final IIOMetadataNode local_node = (IIOMetadataNode) image_metadata.getAsTree(image_metadata.getNativeMetadataFormatName());
				// TODO: Gather frame info
				final int offset_x;
				final int offset_y;
				final int image_width;
				final int image_height;
				final boolean interlace;
				final ColorTable local_color_table;
				final GifFrame.DisposalMethod disposal_method;
				final boolean user_input;
				final boolean transparent_color;
				final int delay;
				final short transparent_color_index;
				// TODO: Get image descriptor
				node = getNodeByName(local_node, "ImageDescriptor");
				assert node != null;
				offset_x =Integer.parseInt(node.getAttribute("imageLeftPosition"));
				offset_y = Integer.parseInt(node.getAttribute("imageTopPosition"));
				image_width = Integer.parseInt(node.getAttribute("imageWidth"));
				image_height =Integer.parseInt(node.getAttribute("imageHeight"));
				interlace = Boolean.parseBoolean(node.getAttribute("interlaceFlag"));
				// TODO: Get local color table
				node = getNodeByName(local_node, "LocalColorTable");
				if (node != null) {
					local_color_table = getColorTable(node, color_resolution, true);
				}
				else {
					local_color_table = null;
				}
				// TODO: Get additional information
				node = getNodeByName(local_node, "GraphicControlExtension");
				assert node != null;
				disposal_method = GifFrame.DisposalMethod.getDisposalMethod(node.getAttribute("disposalMethod"));
				user_input = Boolean.parseBoolean(node.getAttribute("userInputFlag"));
				transparent_color = Boolean.parseBoolean(node.getAttribute("transparentColorFlag"));
				delay = Integer.parseInt(node.getAttribute("delayTime"));
				transparent_color_index = Short.parseShort(node.getAttribute("transparentColorIndex"));
				// TODO: Create the frame
				// Read the raw image
				BufferedImage current_image = reader.read(index);
				// Create a new image for composition
				BufferedImage new_image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
				Graphics2D new_graphics = new_image.createGraphics();
				// Draw the master image as the first layer
				new_graphics.drawImage(master_image, 0, 0, null);
				// Set local background color
				new_graphics.setBackground(getApplicableBackgroundColor(global_color_table, local_color_table,
						transparent_color, transparent_color_index, false));
				// Draw the new image as the second layer
				new_graphics.drawImage(current_image, offset_x, offset_y, null);
				// Handle frame disposal
				// RESTORE_TO_PREVIOUS doesn't require any additional action, so don't bother handling it
				switch (disposal_method) {
					case DO_NOT_DISPOSE:
						master_image = new_image;
						master_graphics = new_graphics;
						break;
					case RESTORE_TO_BACKGROUND_COLOR:
						master_graphics.setBackground(getApplicableBackgroundColor(global_color_table, local_color_table,
								transparent_color, transparent_color_index, true));
						master_graphics.clearRect(0, 0, width, height);
						break;
				}
				// Create and add the new frame
				gif_image.frames.add(new GifFrame(
						current_image, new_image, offset_x, offset_y, image_width, image_height, interlace,
						local_color_table, disposal_method, user_input, transparent_color, delay, transparent_color_index
				));
				index++;
			}
		}
		catch (IndexOutOfBoundsException e) {
			// Do nothing
		}
		return gif_image;
	}

	private static Color getApplicableBackgroundColor(ColorTable global_color_table, ColorTable local_color_table,
													  boolean transparent_color, short transparent_color_index, boolean prefer_global) {
		if (prefer_global) {
			if (transparent_color && global_color_table.background_color_index == transparent_color_index) {
				return new Color(0, true);
			}
			return global_color_table.colors[global_color_table.background_color_index];
		}
		if (local_color_table == null) {
			if (transparent_color && global_color_table.background_color_index == transparent_color_index) {
				return new Color(0, true);
			}
			return global_color_table.colors[global_color_table.background_color_index];
		}
		if (transparent_color && local_color_table.background_color_index == transparent_color_index) {
			return new Color(0, true);
		}
		return local_color_table.colors[local_color_table.background_color_index];
	}

	private static IIOMetadataNode getNodeByName(Node parent, String name) {
		if (!parent.hasChildNodes()) {
			return null;
		}
		NodeList nodes = parent.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeName().equals(name)) {
				return (IIOMetadataNode) nodes.item(i);
			}
		}
		return null;
	}

	private static ColorTable getColorTable(IIOMetadataNode color_table_node, int color_resolution, boolean local) {
		short size = Short.parseShort(color_table_node.getAttribute("sizeOf" + (local ? "Local" : "Global") + "ColorTable"));
		short background_color_index = color_table_node.hasAttribute("backgroundColorIndex") ? Short.parseShort(color_table_node.getAttribute("backgroundColorIndex")) : 0;
		boolean sort = Boolean.parseBoolean(color_table_node.getAttribute("sortFlag"));
		Color[] colors = new Color[size];
		NodeList elements = color_table_node.getChildNodes();
		try {
			int i = 0;
			while (true) {
				IIOMetadataNode node = (IIOMetadataNode) elements.item(i);
				if (!node.getNodeName().equals("ColorTableEntry")) {
					continue;
				}
				short index = Short.parseShort(node.getAttribute("index"));
				int r = Integer.parseInt(node.getAttribute("red"));
				int g = Integer.parseInt(node.getAttribute("green"));
				int b = Integer.parseInt(node.getAttribute("blue"));
				colors[index] = new Color(r, g, b);
				i++;
			}
		}
		catch (NullPointerException e) {
			// Do nothing
		}
		return new ColorTable(colors, background_color_index, sort);
	}

	public static class ColorTable {
		public Color[] colors;
		public short background_color_index;
		public boolean sort;

		private ColorTable(Color[] colors, short background_color_index, boolean sort) {
			this.colors = colors;
			this.background_color_index = background_color_index;
			this.sort = sort;
		}
	}
}
