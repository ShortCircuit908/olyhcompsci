package com.calebmilligan.compsci.image.gif;

import java.awt.*;
import java.awt.image.ImageObserver;

/**
 * @author ShortCircuit908
 *         Created on 10/21/2015
 */
public class GifRenderer implements Runnable {
	private final GifImage image;
	private final Graphics2D graphics;
	private final int offset_x;
	private final int offset_y;
	private final ImageObserver observer;
	private final Color background_color;
	private int frame_index = 0;
	private double frame_rate_scale = 1.0;
	private boolean looping = false;
	private Thread render_thread = null;
	boolean paused = false;

	public GifRenderer(GifImage image, Graphics2D graphics, int offset_x, int offset_y, ImageObserver observer) {
		this(image, graphics, offset_x, offset_y, observer, new Color(0, true));
	}

	public GifRenderer(GifImage image, Graphics2D graphics, int offset_x, int offset_y, ImageObserver observer, Color background_color) {
		this.image = image;
		this.graphics = graphics;
		this.offset_x = offset_x;
		this.offset_y = offset_y;
		this.observer = observer;
		this.background_color = background_color;
		graphics.setBackground(background_color);
	}

	public double getFrameRateScale() {
		return frame_rate_scale;
	}

	public void setFrameRateScale(double frame_rate_scale) {
		this.frame_rate_scale = frame_rate_scale;
	}

	public int getFrameIndex() {
		return frame_index;
	}

	public void setFrameIndex(int frame_index) {
		boolean stopped = render_thread == null || paused;
		stop();
		if (frame_index >= image.getFrames().size()) {
			throw new IndexOutOfBoundsException();
		}
		this.frame_index = frame_index;
		if (!stopped) {
			start();
		}
	}

	public boolean isLooping() {
		return looping;
	}

	public void setLooping(boolean looping) {
		this.looping = looping;
	}

	@Override
	public void run() {
		while(true) {
			if (paused) {
				continue;
			}
			GifFrame frame = image.getFrames().get(frame_index++);
			graphics.clearRect(offset_x, offset_y, image.getWidth(), image.getHeight());
			graphics.drawImage(frame.getCompositeImage(), offset_x, offset_y, observer);
			if (observer != null) {
				observer.imageUpdate(frame.getCompositeImage(), ImageObserver.ALLBITS, offset_x, offset_y, image.getWidth(), image.getHeight());
			}
			try {
				double delay = ((frame.getDelay() == 0 ? 10 : frame.getDelay()) * 10.0) / frame_rate_scale;
				long millis = (long) delay;
				int nanos = (int) ((delay - millis) * 1000000.0);
				Thread.sleep(millis, nanos);
			}
			catch (InterruptedException e) {
				return;
			}
			if (frame_index >= image.getFrames().size()) {
				frame_index = 0;
				if (!looping) {
					stop();
					return;
				}
			}
		}
	}

	public void start() {
		paused = false;
		if (render_thread == null) {
			render_thread = new Thread(this);
			render_thread.start();
		}
		else {
			render_thread.resume();
		}
	}

	public void pause() {
		paused = true;
		render_thread.suspend();
	}

	public void restart() {
		stop();
		start();
	}

	public void stop() {
		if (render_thread != null) {
			render_thread.interrupt();
			render_thread = null;
		}
		frame_index = 0;
	}
}
