package com.calebmilligan.compsci.image;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.Serializable;

/**
 * Wrapper to hold an image for serialization
 *
 * @author ShortCircuit908
 *         Created on 9/30/2015
 */
public class ImageWrapper implements Serializable {
	private static final long serialVersionUID = -120394870993489L;
	private transient BufferedImage cached_image = null;
	private final String format;
	private final byte[] data;

	public ImageWrapper(RenderedImage image, String format) throws IOException {
		this.format = format;
		data = ImageUtils.serializeImage(image, format);
	}

	public BufferedImage getImage() throws IOException {
		if (cached_image == null) {
			cached_image = ImageUtils.deserializeImage(data);
		}
		return cached_image;
	}

	public String getFormat() {
		return format;
	}
}
